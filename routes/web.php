<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('reset_password/{token}', ['as' => 'password.reset', function($token)
{
    // implement your reset password route here!
}]);

Route::get('/','FrontendController@index')->name('homepage');
Route::get('/trackapps','FrontendController@track')->name('track');
Route::get('/artistapps','FrontendController@artist')->name('artist');
Route::get('/eventapps','FrontendController@event')->name('event');
Route::get('/cafeapps','FrontendController@cafe')->name('cafe');


Route::get('/berita', 'FrontendController@list');
Route::get('/berita/tag/{tag}', 'FrontendController@NewsByTag');
Route::get('/berita/cat/{cat}', 'FrontendController@NewsByCategory');
Route::get('/berita/view/{slug}', 'FrontendController@view');
Route::get('/podcasts1', 'FrontendController@listpodcast');
Route::get('/podcasts1/view/{slug}', 'FrontendController@viewpodcast');
Route::get('/contest', 'FrontendController@listcontest');

Route::get('/partnership', 'FrontendController@register');
Route::post('/store-register', 'FrontendController@storeRegister')->name('store-register');
Route::get('/success-register/{id}', 'FrontendController@viewSuccess')->name('succes-register');
Route::get('/verification-user/{id}', 'FrontendController@verifiedUser');

//Route::get('/constest/view/{slug}', 'FrontendController@viewpodcast');
Route::get('/about', 'FrontendController@about');
Route::get('/contact', 'FrontendController@contact');
Route::get('/home', 'HomeController@index')->name('home');
Auth::routes();
// $this->post('logout', 'Auth\LoginController@logout')->name('logout');


//user
Route::resource('user','Admin\UserController');

//business
// Route::get('//dashboard', 'DashboardController@index');

//admin
// Route::resource('business','Admin\BusinessController');
Route::resource('business-categories','Admin\BusinessCategoriesController');
Route::resource('partner','Admin\PartnerController');
Route::resource('list-business','Admin\ListBusinessController');


//news
Route::resource('news','Admin\NewsController');
Route::resource('news-categories','Admin\NewsCategoryController');


//musisi
// Route::get('musisi-platform/sync', 'Admin\MusisiPlatformController@sync')->name('musisi-platform.sync');
// Route::get('musisi-platform/syncTrack', 'Admin\MusisiPlatformController@syncTrack')->name('musisi-platform.syncTrack');
// Route::get('musisi-platform/syncBand', 'Admin\MusisiPlatformController@syncBand')->name('musisi-platform.syncBand');
// Route::resource('musisi-platform','Admin\MusisiPlatformController');

Route::prefix('business')->group(static function () {
    Route::get('/', 'Business\DashboardController@index')->name('business');

    Route::get('/employee', 'Business\EmployeeController@index')->name('business.employee');
    Route::get('/setting', 'Business\SettingController@index')->name('business.setting');

    // BusinessController
    Route::get('/list', 'Business\BusinessController@index')->name('business.list');
    Route::get('/create', 'Business\BusinessController@create')->name('business.create');
    Route::post('/store', 'Business\BusinessController@store')->name('business.store');
    Route::post('/edit', 'Business\BusinessController@update')->name('business.update');
    Route::post('/status', 'Business\BusinessController@updateStatus')->name('business.updateStatus');
    Route::get('/dashboard-business/{id}', 'Business\BusinessController@dashboardBusiness')->name('business.dashboard');
    Route::get('/detail/{id}', 'Business\BusinessController@detail')->name('business.detail');
    Route::get('/configuration/{id}', 'Business\BusinessController@configuration')->name('business.configuration');
    Route::get('/detail-order/{id}/{bId}', 'Business\BusinessController@detailOrder')->name('business.detailOrder');
    Route::get('/detail-track/{id}/{bId}', 'Business\BusinessController@detailMusic')->name('business.detailMusic');
    Route::get('/status/{id}/{bId}', 'Business\BusinessController@status')->name('business.status');
    Route::get('/edit/{id}', 'Business\BusinessController@edit')->name('business.edit');
    Route::get('/delete/{id}', 'Business\BusinessController@delete')->name('business.detele');
    Route::get('/report-business/{id}', 'Business\BusinessController@reportBusiness')->name('business.report');
    Route::get('/create-package/{id}', 'Business\BusinessController@createPackage')->name('business.createPackage');
    Route::get('/edit-package/{id}', 'Business\BusinessController@editPackage')->name('business.editPackage');
    Route::post('/store-package', 'Business\BusinessController@storePackage')->name('business.storePackage');
    Route::post('/edit-package', 'Business\BusinessController@updatePackage')->name('business.updatePackage');
    Route::post('/edit-notification', 'Business\BusinessController@updateNotification')->name('business.updateNotification');

    // Route::get('/track', 'Admin\MusisiPlatformController@track')->name('musisi-platform.track');
    // Route::get('/trackMusician', 'Admin\MusisiPlatformController@trackMusician')->name('musisi-platform.trackMusician');
    // Route::get('/band', 'Admin\MusisiPlatformController@band')->name('musisi-platform.band');
    // Route::get('/data', 'Admin\MusisiPlatformController@data')->name('musisi-platform.data');
    // Route::get('/dataBand', 'Admin\MusisiPlatformController@dataBand')->name('musisi-platform.dataBand');
    // Route::get('/dataTrack', 'Admin\MusisiPlatformController@dataTrack')->name('musisi-platform.dataTrack');
    // Route::get('/dataTrackMusician', 'Admin\MusisiPlatformController@dataTrackMusician')->name('musisi-platform.dataTrackMusician');
    // Route::get('musisi-platform/sync', 'Admin\MusisiPlatformController@sync')->name('musisi-platform.sync');
    // Route::get('musisi-platform/syncTrack', 'Admin\MusisiPlatformController@syncTrack')->name('musisi-platform.syncTrack');
    // Route::get('musisi-platform/syncTrackMusician', 'Admin\MusisiPlatformController@syncTrackMusician')->name('musisi-platform.syncTrackMusician');
    // Route::get('musisi-platform/syncBand', 'Admin\MusisiPlatformController@syncBand')->name('musisi-platform.syncBand');
});

Route::prefix('musisi-platform')->group(static function () {
    Route::get('/', 'Admin\MusisiPlatformController@index')->name('musisi-platform');
    Route::get('/track', 'Admin\MusisiPlatformController@track')->name('musisi-platform.track');
    Route::get('/trackMusician', 'Admin\MusisiPlatformController@trackMusician')->name('musisi-platform.trackMusician');
    Route::get('/band', 'Admin\MusisiPlatformController@band')->name('musisi-platform.band');
    Route::get('/data', 'Admin\MusisiPlatformController@data')->name('musisi-platform.data');
    Route::get('/dataBand', 'Admin\MusisiPlatformController@dataBand')->name('musisi-platform.dataBand');
    Route::get('/dataTrack', 'Admin\MusisiPlatformController@dataTrack')->name('musisi-platform.dataTrack');
    Route::get('/dataTrackMusician', 'Admin\MusisiPlatformController@dataTrackMusician')->name('musisi-platform.dataTrackMusician');
    Route::get('musisi-platform/sync', 'Admin\MusisiPlatformController@sync')->name('musisi-platform.sync');
    Route::get('musisi-platform/syncTrack', 'Admin\MusisiPlatformController@syncTrack')->name('musisi-platform.syncTrack');
    Route::get('musisi-platform/syncTrackMusician', 'Admin\MusisiPlatformController@syncTrackMusician')->name('musisi-platform.syncTrackMusician');
    Route::get('musisi-platform/syncBand', 'Admin\MusisiPlatformController@syncBand')->name('musisi-platform.syncBand');

    // Route::get('/data', 'Admin\BannerController@data')->name('banner.data');
    // Route::get('/create', 'Admin\BannerController@create')->name('banner.create');
    // Route::post('/store', 'Admin\BannerController@store')->name('banner.store');
    // Route::get('/edit', 'Admin\BannerController@edit')->name('banner.edit');
    // Route::post('/update', 'Admin\BannerController@update')->name('banner.update');
    // Route::get('/delete', 'Admin\BannerController@delete')->name('banner.delete');
    // Route::post('/destroy', 'Admin\BannerController@destroy')->name('banner.destroy');
    // Route::get('/show', 'Admin\BannerController@show')->name('banner.show');
});

Route::get('/home', 'HomeController@index')->name('home');

//Banner
Route::prefix('banner')->group(static function () {
    Route::get('/', 'Admin\BannerController@index')->name('banner');
    Route::get('/data', 'Admin\BannerController@data')->name('banner.data');
    Route::get('/create', 'Admin\BannerController@create')->name('banner.create');
    Route::post('/store', 'Admin\BannerController@store')->name('banner.store');
    Route::get('/edit', 'Admin\BannerController@edit')->name('banner.edit');
    Route::post('/update', 'Admin\BannerController@update')->name('banner.update');
    Route::get('/delete', 'Admin\BannerController@delete')->name('banner.delete');
    Route::post('/destroy', 'Admin\BannerController@destroy')->name('banner.destroy');
    Route::get('/show', 'Admin\BannerController@show')->name('banner.show');
});

//genre
Route::prefix('genre')->group(static function () {
    Route::get('/', 'Admin\GenreController@index')->name('genre');
    Route::get('/data', 'Admin\GenreController@data')->name('genre.data');
    Route::get('/create', 'Admin\GenreController@create')->name('genre.create');
    Route::post('/store', 'Admin\GenreController@store')->name('genre.store');
    Route::get('/edit', 'Admin\GenreController@edit')->name('genre.edit');
    Route::post('/update', 'Admin\GenreController@update')->name('genre.update');
    Route::get('/delete', 'Admin\GenreController@delete')->name('genre.delete');
    Route::post('/destroy', 'Admin\GenreController@destroy')->name('genre.destroy');
    Route::get('/show', 'Admin\GenreController@show')->name('genre.show');
});

//event
Route::prefix('event')->group(static function () {
    Route::get('/', 'Admin\EventController@index')->name('event');
    Route::get('/data', 'Admin\EventController@data')->name('event.data');
    Route::get('/create', 'Admin\EventController@create')->name('event.create');
    Route::post('/store', 'Admin\EventController@store')->name('event.store');
    Route::get('/edit', 'Admin\EventController@edit')->name('event.edit');
    Route::post('/update', 'Admin\EventController@update')->name('event.update');
    Route::get('/delete', 'Admin\EventController@delete')->name('event.delete');
    Route::post('/destroy', 'Admin\EventController@destroy')->name('event.destroy');
    Route::get('/show', 'Admin\EventController@show')->name('event.show');
});

//compilation
Route::prefix('compilation')->group(static function () {
    Route::get('/', 'Admin\CompilationController@index')->name('compilation');
    Route::get('/data', 'Admin\CompilationController@data')->name('compilation.data');
    Route::get('/create', 'Admin\CompilationController@create')->name('compilation.create');
    Route::post('/store', 'Admin\CompilationController@store')->name('compilation.store');
    Route::get('/edit', 'Admin\CompilationController@edit')->name('compilation.edit');
    Route::post('/update', 'Admin\CompilationController@update')->name('compilation.update');
    Route::get('/delete', 'Admin\CompilationController@delete')->name('compilation.delete');
    Route::post('/destroy', 'Admin\CompilationController@destroy')->name('compilation.destroy');
    Route::get('/show', 'Admin\CompilationController@show')->name('compilation.show');
});

//podcast

//podcast

Route::prefix('podcast')->group(static function () {
    Route::get('/', 'Admin\PodcastController@index')->name('podcast');
    Route::get('/data', 'Admin\PodcastController@data')->name('podcast.data');
    Route::get('/create', 'Admin\PodcastController@create')->name('podcast.create');
    Route::post('/store', 'Admin\PodcastController@store')->name('podcast.store');
    Route::get('/edit', 'Admin\PodcastController@edit')->name('podcast.edit');
    Route::post('/update', 'Admin\PodcastController@update')->name('podcast.update');
    Route::get('/delete', 'Admin\PodcastController@delete')->name('podcast.delete');
    Route::post('/destroy', 'Admin\PodcastController@destroy')->name('podcast.destroy');
    Route::get('/show', 'Admin\PodcastController@show')->name('podcast.show');
});

Route::resource('podcast-categories','Admin\PodcastCategoryController');
// Route::resource('banner','Admin\BannerController');

//Caffe
Route::prefix('cafe')->group(static function () {
    Route::get('/', 'Admin\CafeController@index')->name('cafe');
    Route::get('/data', 'Admin\CafeController@data')->name('cafe.data');
    Route::get('/create', 'Admin\CafeController@create')->name('cafe.create');
    Route::post('/store', 'Admin\CafeController@store')->name('cafe.store');
    Route::get('/edit', 'Admin\CafeController@edit')->name('cafe.edit');
    Route::post('/update', 'Admin\CafeController@update')->name('cafe.update');
    Route::get('/delete', 'Admin\CafeController@delete')->name('cafe.delete');
    Route::post('/destroy', 'Admin\CafeController@destroy')->name('cafe.destroy');
    Route::get('/show', 'Admin\CafeController@show')->name('cafe.show');
    Route::get('/search-user', 'Admin\CafeController@search_user')->name('cafe.search.user');
    Route::get('/map', 'Admin\CafeController@map')->name('cafe.map');
});

//Caffe Menu
Route::prefix('cafe-menu')->group(static function () {
    Route::get('/', 'Admin\CafeMenuController@index')->name('cafe.menu');
    Route::get('/data', 'Admin\CafeMenuController@data')->name('cafe.menu.data');
    Route::get('/create', 'Admin\CafeMenuController@create')->name('cafe.menu.create');
    Route::post('/store', 'Admin\CafeMenuController@store')->name('cafe.menu.store');
    Route::get('/edit', 'Admin\CafeMenuController@edit')->name('cafe.menu.edit');
    Route::post('/update', 'Admin\CafeMenuController@update')->name('cafe.menu.update');
    Route::get('/delete', 'Admin\CafeMenuController@delete')->name('cafe.menu.delete');
    Route::post('/destroy', 'Admin\CafeMenuController@destroy')->name('cafe.menu.destroy');
    Route::get('/show', 'Admin\CafeMenuController@show')->name('cafe.menu.show');
});

//Caffe Image
Route::prefix('cafe-image-data')->group(static function () {
    Route::get('', 'Admin\CafeImageController@index')->name('cafe.image');
    Route::get('/data', 'Admin\CafeImageController@data')->name('cafe.image.data');
    Route::get('/create', 'Admin\CafeImageController@create')->name('cafe.image.create');
    Route::post('/store', 'Admin\CafeImageController@store')->name('cafe.image.store');
    Route::get('/edit', 'Admin\CafeImageController@edit')->name('cafe.image.edit');
    Route::post('/update', 'Admin\CafeImageController@update')->name('cafe.image.update');
    Route::get('/delete', 'Admin\CafeImageController@delete')->name('cafe.image.delete');
    Route::post('/destroy', 'Admin\CafeImageController@destroy')->name('cafe.image.destroy');
});

//Caffe Schedule
Route::prefix('cafe-schedule')->group(static function () {
    Route::get('', 'Admin\CafeScheduleController@index')->name('cafe.schedule');
    Route::get('/data', 'Admin\CafeScheduleController@data')->name('cafe.schedule.data');
    Route::get('/create', 'Admin\CafeScheduleController@create')->name('cafe.schedule.create');
    Route::post('/store', 'Admin\CafeScheduleController@store')->name('cafe.schedule.store');
    Route::get('/edit', 'Admin\CafeScheduleController@edit')->name('cafe.schedule.edit');
    Route::post('/update', 'Admin\CafeScheduleController@update')->name('cafe.schedule.update');
    Route::get('/delete', 'Admin\CafeScheduleController@delete')->name('cafe.schedule.delete');
    Route::post('/destroy', 'Admin\CafeScheduleController@destroy')->name('cafe.schedule.destroy');
});
