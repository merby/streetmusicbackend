<?php

use Dingo\Api\Routing\Router;

/** @var Router $api */
$api = app(Router::class);


$api->version('v1', function (Router $api) {
    $api->group(['prefix' => 'auth'], function(Router $api) {
        $api->post('signup', 'App\\Api\\V1\\Controllers\\SignUpController@signUp');
        $api->post('login', 'App\\Api\\V1\\Controllers\\LoginController@login');

        $api->post('recovery', 'App\\Api\\V1\\Controllers\\ForgotPasswordController@sendResetEmail');
        $api->post('reset', 'App\\Api\\V1\\Controllers\\ResetPasswordController@resetPassword');

        $api->post('logout', 'App\\Api\\V1\\Controllers\\LogoutController@logout');
        $api->post('refresh', 'App\\Api\\V1\\Controllers\\RefreshController@refresh');
        $api->get('me', 'App\\Api\\V1\\Controllers\\UserController@me');

        //user
        $api->post('create-user-apps', 'App\\Api\\V1\\Controllers\\ArtistController@createUserApps');
        $api->post('create-band-apps', 'App\\Api\\V1\\Controllers\\ArtistController@createBandApps');
        $api->post('create-track-apps', 'App\\Api\\V1\\Controllers\\ArtistController@createTrackApps');





        //news

        $api->get('list-news', 'App\\Api\\V1\\Controllers\\NewsController@ListNews');
    	  $api->get('get-news', 'App\\Api\\V1\\Controllers\\NewsController@GetNews');
        $api->get('list-category', 'App\\Api\\V1\\Controllers\\NewsController@ListCategory');
        $api->post('news-by-category', 'App\\Api\\V1\\Controllers\\NewsController@NewsByCategory');
        $api->post('news-by-tags', 'App\\Api\\V1\\Controllers\\NewsController@NewsByTag');

        //banner
        $api->get('get-list-banner-by-category', 'App\\Api\\V1\\Controllers\\BannerController@getBannerByCategory');
        $api->get('get-banner-by-id', 'App\\Api\\V1\\Controllers\\BannerController@getBannerById');
        
        //Cafe
        $api->post('get-list-cafe', 'App\\Api\\V1\\Controllers\\CafeController@getListCafe');
        $api->get('get-cafe', 'App\\Api\\V1\\Controllers\\CafeController@getCafe');

        //event
        $api->post('get-list-event', 'App\\Api\\V1\\Controllers\\EventController@getListEvent');
        $api->get('get-event', 'App\\Api\\V1\\Controllers\\EventController@getEvent');
        

        //genre
        $api->get('get-list-genre', 'App\\Api\\V1\\Controllers\\RecommendationController@getListGenre');
        $api->get('get-genre', 'App\\Api\\V1\\Controllers\\RecommendationController@getGenreById');
        $api->get('get-trending', 'App\\Api\\V1\\Controllers\\RecommendationController@getTrending');
        $api->get('get-latest-artist', 'App\\Api\\V1\\Controllers\\RecommendationController@getLatestArtistDashboard');
        $api->get('get-latest-song', 'App\\Api\\V1\\Controllers\\RecommendationController@getLatestSongDashboard');
        $api->get('get-latest-podcast', 'App\\Api\\V1\\Controllers\\RecommendationController@getLatestPodcast');
        $api->get('get-popular-podcast', 'App\\Api\\V1\\Controllers\\RecommendationController@getPopularPodcast');
        $api->get('get-podcast-category', 'App\\Api\\V1\\Controllers\\RecommendationController@getPodcastCategory');
    	$api->get('get-podcast-by-category', 'App\\Api\\V1\\Controllers\\RecommendationController@getPodcastByCategory');

        $api->get('get-popular-song-genre', 'App\\Api\\V1\\Controllers\\RecommendationController@getPopularSongByGenre');
        $api->get('get-latest-song-genre', 'App\\Api\\V1\\Controllers\\RecommendationController@getLatestSongByGenre');
        $api->get('get-latest-compilation', 'App\\Api\\V1\\Controllers\\RecommendationController@getLatestCompilation');
        $api->get('get-popular-compilation', 'App\\Api\\V1\\Controllers\\RecommendationController@getPopularCompilation');
        $api->get('get-compilation-track', 'App\\Api\\V1\\Controllers\\RecommendationController@getCompilationTrack');

        //playlist
        $api->post('create-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@createPlaylist');
        $api->get('my-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@getMyPlaylist');
        $api->get('my-tracklist', 'App\\Api\\V1\\Controllers\\PlaylistController@getMyTracklist');
        $api->get('delete-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@deletePlaylist');
        $api->get('set-default-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@setDefault');
        $api->post('add-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@addToPlaylist');
        $api->post('add-playlist-bulk', 'App\\Api\\V1\\Controllers\\PlaylistController@addToPlaylistBulk');
        $api->post('remove-playlist', 'App\\Api\\V1\\Controllers\\PlaylistController@removeFromPlaylist');
        $api->post('remove-playlist-bulk', 'App\\Api\\V1\\Controllers\\PlaylistController@removeFromPlaylistBulk');

        //artist
        $api->get('get-artist', 'App\\Api\\V1\\Controllers\\ArtistController@getArtistById');
        $api->get('get-artist-schedule', 'App\\Api\\V1\\Controllers\\ArtistController@GetArtistSchedule');
        $api->get('search-by-text', 'App\\Api\\V1\\Controllers\\SearchController@searchByText');
        $api->post('search-by-musician', 'App\\Api\\V1\\Controllers\\SearchController@searchByMusician');
        $api->post('search-by-band', 'App\\Api\\V1\\Controllers\\SearchController@searchByBand');
        $api->get('get-track', 'App\\Api\\V1\\Controllers\\ArtistController@getTrackById');

        //bussiness
        $api->post('post_request', 'App\\Api\\V1\\Controllers\\AggregatorApiController@requestPost');
        $api->get('get_list_aggregator', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListAggregator');
        $api->get('get_list_request', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListRequest');
        $api->get('get_list_package', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListPackage');
        $api->get('update_payment', 'App\\Api\\V1\\Controllers\\AggregatorApiController@updatePayment');
        $api->post('update_request', 'App\\Api\\V1\\Controllers\\AggregatorApiController@requestUpdate');
        $api->get('get_list_bahasa_rilis', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListBahasaRilis');
        $api->get('get_list_bahasa_lirik', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListBahasaLirik');
        $api->get('get_list_genre_musik', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getListGenreMusik');
        $api->get('get_bahasa_by_id', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getBahasaById');
        $api->get('get_genre_by_id', 'App\\Api\\V1\\Controllers\\AggregatorApiController@getGereMusikById');
    });

    $api->group(['middleware' => 'jwt.auth'], function(Router $api) {
        $api->get('protected', function() {
            return response()->json([
                'message' => 'Access to protected resources granted! You are seeing this text as you provided the token correctly.'
            ]);
        });

        $api->get('refresh', [
            'middleware' => 'jwt.refresh',
            function() {
                return response()->json([
                    'message' => 'By accessing this endpoint, you can refresh your access token at each request. Check out this response headers!'
                ]);
            }
        ]);
    });

    $api->get('hello', function() {
        return response()->json([
            'message' => 'This is a simple example of item returned by your APIs. Everyone can see it.'
        ]);
    });
});
