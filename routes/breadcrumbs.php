<?php

// Home
Breadcrumbs::for('home', function ($trail) {
    $trail->push('Home', route('home'));
});

// Home > Profile

Breadcrumbs::for('user', function ($trail) {
    $trail->push('Home', route('home'));
});
Breadcrumbs::for('user.show', function ($trail,$id) {
    $trail->parent('user');
    $trail->push('Profile', route('user.show',$id));
});

Breadcrumbs::for('user.edit', function ($trail,$id) {
    $trail->parent('user');
    $trail->push('Profile', route('user.show',$id));
});
// Home > Users
Breadcrumbs::for('users', function ($trail) {

    $trail->push('User Management', route('users'));
});

Breadcrumbs::for('users.show', function ($trail) {
    $trail->parent('users');
    $trail->push('View User', route('users'));
});
Breadcrumbs::for('users.create', function ($trail) {
    $trail->parent('users');
    $trail->push('Create User', route('users'));
});

Breadcrumbs::for('users.edit', function ($trail) {
    $trail->parent('users');
    $trail->push('Edit User', route('users'));
});

// Home > business

// Breadcrumbs::for('business', function ($trail) {

//     $trail->push('Business', route('business.index'));
// });

// Breadcrumbs::for('business.create', function ($trail) {
//     $trail->parent('business');
//     $trail->push('Create New', route('business.create'));
// });

Breadcrumbs::for('business-categories', function ($trail) {
    $trail->push('Categories', route('business-categories.index'));
});

Breadcrumbs::for('business-categories.index', function ($trail) {
    $trail->parent('business-categories');
    $trail->push('List', route('business-categories.index'));
});
Breadcrumbs::for('business-categories.show', function ($trail) {
    $trail->parent('business-categories');
    $trail->push('View Detail', route('business-categories.index'));
});

Breadcrumbs::for('business-categories.edit', function ($trail) {
    $trail->parent('business-categories');
    $trail->push('Edit', route('business-categories.index'));
});

Breadcrumbs::for('business-categories.create', function ($trail) {
    $trail->parent('business-categories');
    $trail->push('Create', route('business-categories.create'));
});

//Partner
Breadcrumbs::for('partner', function ($trail) {
    $trail->push('partner', route('partner.index'));
});

Breadcrumbs::for('partner.index', function ($trail) {
    $trail->parent('partner');
    $trail->push('List', route('partner.index'));
});

Breadcrumbs::for('partner.show', function ($trail) {
    $trail->parent('partner');
    $trail->push('View Detail', route('partner.index'));
});

Breadcrumbs::for('partner.edit', function ($trail) {
    $trail->parent('partner');
    $trail->push('Edit', route('partner.index'));
});

Breadcrumbs::for('partner.create', function ($trail) {
    $trail->parent('partner');
    $trail->push('Create', route('partner.create'));
});

//list-business
Breadcrumbs::for('list-business', function ($trail) {
    $trail->push('list-business', route('list-business.index'));
});

Breadcrumbs::for('list-business.index', function ($trail) {
    $trail->parent('list-business');
    $trail->push('List', route('list-business.index'));
});
// Home > news
Breadcrumbs::for('News', function ($trail) {

    $trail->push('News', route('news.index'));
});
Breadcrumbs::for('news.index', function ($trail) {
    $trail->parent('News');
    $trail->push('List News', route('news.index'));
});
Breadcrumbs::for('news.create', function ($trail) {
    $trail->parent('News');
    $trail->push('Create News', route('news.create'));
});
Breadcrumbs::for('news.edit', function ($trail) {
    $trail->parent('News');
    $trail->push('Edit News', route('news.index'));
});
Breadcrumbs::for('news.show', function ($trail) {
    $trail->parent('News');
    $trail->push('Preview News', route('news.index'));
});

Breadcrumbs::for('news-categories', function ($trail) {

    $trail->push('news-categories', route('news-categories.index'));
});
Breadcrumbs::for('news-categories.index', function ($trail) {
    $trail->parent('news-categories');
    $trail->push('List', route('news-categories.index'));
});

Breadcrumbs::for('news-categories.show', function ($trail) {
    $trail->parent('news-categories');
    $trail->push('View Detail', route('news-categories.index'));
});

Breadcrumbs::for('news-categories.edit', function ($trail) {
    $trail->parent('news-categories');
    $trail->push('Edit', route('news-categories.index'));
});

Breadcrumbs::for('news-categories.create', function ($trail) {
    $trail->parent('news-categories');
    $trail->push('Create', route('news-categories.create'));
});


// Breadcrumbs::for('users.create', function ($trail) {
//     $trail->parent('users');
//     $trail->push('Create User', route('users'));
// });

// Breadcrumbs::for('users.edit', function ($trail) {
//     $trail->parent('users');
//     $trail->push('Edit User', route('users'));
// });


// Home > Roles
Breadcrumbs::for('roles', function ($trail) {

    $trail->push('Roles', url('roles'));
});

Breadcrumbs::for('laravelroles::roles.index', function ($trail) {
    $trail->parent('roles');
    $trail->push('Roles & Permissions', url('roles'));
});

Breadcrumbs::for('laravelroles::roles.show', function ($trail) {
    $trail->parent('roles');
    $trail->push('View Role', url('roles'));
});

Breadcrumbs::for('laravelroles::roles.edit', function ($trail) {
    $trail->parent('roles');
    $trail->push('Edit Role', url('roles'));
});

Breadcrumbs::for('laravelroles::roles.create', function ($trail) {
    $trail->parent('roles');
    $trail->push('Create Role', url('roles'));
});

Breadcrumbs::for('laravelroles::permissions.create', function ($trail) {
    $trail->parent('roles');
    $trail->push('Create Permission', url('roles'));
});

Breadcrumbs::for('laravelroles::permissions.edit', function ($trail) {
    $trail->parent('roles');
    $trail->push('Edit Permission', url('roles'));
});

Breadcrumbs::for('laravelroles::permissions.show', function ($trail) {
    $trail->parent('roles');
    $trail->push('View Permission', url('roles'));
});

// Banner
Breadcrumbs::for('banner', function ($trail) {
    $trail->push('Banner', route('banner'));
});

Breadcrumbs::for('banner.create', function ($trail) {
    $trail->parent('banner');
    $trail->push('Create', route('banner.create'));
});

Breadcrumbs::for('banner.edit', function ($trail) {
    $trail->parent('banner');
    $trail->push('Edit', route('banner.edit'));
});

Breadcrumbs::for('banner.show', function ($trail) {
    $trail->parent('banner');
    $trail->push('Detail Banner', route('banner.show'));
});

// Genre
Breadcrumbs::for('genre', function ($trail) {
    $trail->push('Genre', route('genre'));
});

Breadcrumbs::for('genre.create', function ($trail) {
    $trail->parent('genre');
    $trail->push('Create', route('genre.create'));
});

Breadcrumbs::for('genre.edit', function ($trail) {
    $trail->parent('genre');
    $trail->push('Edit', route('genre.edit'));
});

Breadcrumbs::for('genre.show', function ($trail) {
    $trail->parent('genre');
    $trail->push('Detail genre', route('genre.show'));
});


// compilations
Breadcrumbs::for('compilation', function ($trail) {
    $trail->push('Compilation', route('compilation'));
});

Breadcrumbs::for('compilation.create', function ($trail) {
    $trail->parent('compilation');
    $trail->push('Create', route('compilation.create'));
});

Breadcrumbs::for('compilation.edit', function ($trail) {
    $trail->parent('compilation');
    $trail->push('Edit', route('compilation.edit'));
});

Breadcrumbs::for('compilation.show', function ($trail) {
    $trail->parent('compilation');
    $trail->push('Detail compilation', route('compilation.show'));
});

// podcast
Breadcrumbs::for('podcast', function ($trail) {
    $trail->push('Podcast', route('podcast'));
});

Breadcrumbs::for('podcast.create', function ($trail) {
    $trail->parent('podcast');
    $trail->push('Create', route('podcast.create'));
});

Breadcrumbs::for('podcast.edit', function ($trail) {
    $trail->parent('podcast');
    $trail->push('Edit', route('podcast.edit'));
});

Breadcrumbs::for('podcast.show', function ($trail) {
    $trail->parent('podcast');
    $trail->push('Detail podcast', route('podcast.show'));
});

Breadcrumbs::for('podcast-categories', function ($trail) {

    $trail->push('podcast-categories', route('podcast-categories.index'));
});
Breadcrumbs::for('podcast-categories.index', function ($trail) {
    $trail->parent('podcast-categories');
    $trail->push('List', route('podcast-categories.index'));
});

Breadcrumbs::for('podcast-categories.show', function ($trail) {
    $trail->parent('podcast-categories');
    $trail->push('View Detail', route('podcast-categories.index'));
});

Breadcrumbs::for('podcast-categories.edit', function ($trail) {
    $trail->parent('podcast-categories');
    $trail->push('Edit', route('podcast-categories.index'));
});

Breadcrumbs::for('podcast-categories.create', function ($trail) {
    $trail->parent('podcast-categories');
    $trail->push('Create', route('podcast-categories.create'));
});


// event
Breadcrumbs::for('event', function ($trail) {
    $trail->push('Event', route('event'));
});

Breadcrumbs::for('event.create', function ($trail) {
    $trail->parent('event');
    $trail->push('Create', route('event.create'));
});

Breadcrumbs::for('event.edit', function ($trail) {
    $trail->parent('event');
    $trail->push('Edit', route('event.edit'));
});

Breadcrumbs::for('event.show', function ($trail) {
    $trail->parent('event');
    $trail->push('Detail event', route('event.show'));
});

// Caffe
Breadcrumbs::for('cafe', function ($trail) {
    $trail->push('Caffe', route('cafe'));
});

Breadcrumbs::for('cafe.create', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Create', route('cafe.create'));
});

Breadcrumbs::for('cafe.edit', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Edit', route('cafe.edit'));
});

Breadcrumbs::for('cafe.show', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Detail Cafe', route('cafe.show'));
});

Breadcrumbs::for('cafe.menu', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Menu Cafe', route('cafe.menu') . '?cafe=' . request()->input('cafe'));
});

Breadcrumbs::for('cafe.menu.create', function ($trail) {
    $trail->parent('cafe.menu');
    $trail->push('Create Menu Cafe', route('cafe.menu.create'));
});

Breadcrumbs::for('cafe.menu.edit', function ($trail) {
    $trail->parent('cafe.menu');
    $trail->push('Edit Menu Cafe', route('cafe.menu.edit'));
});

Breadcrumbs::for('cafe.image', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Image Cafe', route('cafe.image') . '?cafe=' . request()->input('cafe'));
});

Breadcrumbs::for('cafe.schedule', function ($trail) {
    $trail->parent('cafe');
    $trail->push('Schedule Cafe', route('cafe.schedule') . '?cafe=' . request()->input('cafe'));
});

Breadcrumbs::for('cafe.schedule.create', function ($trail) {
    $trail->parent('cafe.schedule');
    $trail->push('Create Schedule Cafe', route('cafe.schedule.create'));
});

Breadcrumbs::for('cafe.schedule.edit', function ($trail) {
    $trail->parent('cafe.schedule');
    $trail->push('Edit Schedule Cafe', route('cafe.schedule.edit'));
});

// Home > business

Breadcrumbs::for('musisi-platform', function ($trail) {

    $trail->push('List Musisi', route('musisi-platform'));
});

Breadcrumbs::for('musisi-platform.sync', function ($trail) {
    $trail->parent('musisi-platform');
    $trail->push('Syncronize', route('musisi-platform.sync'));
});
Breadcrumbs::for('musisi-platform.band', function ($trail) {
    $trail->parent('musisi-platform');
    $trail->push('Band', route('musisi-platform.band'));
});
Breadcrumbs::for('musisi-platform.track', function ($trail) {
    $trail->parent('musisi-platform');
    $trail->push('Track', route('musisi-platform.track'));
});
Breadcrumbs::for('musisi-platform.trackMusician', function ($trail) {
    $trail->parent('musisi-platform');
    $trail->push('Track Musician', route('musisi-platform.trackMusician'));
});
