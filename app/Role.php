<?php

namespace App;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
      protected $table = 'roles';
      
      public function User(){
        return $this->belongsToMany(User::class, 'user_id', 'id');
      }
}
