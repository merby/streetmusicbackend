<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tags extends Model
{
    //
    protected $table = 'tags';
    protected $fillable = ['news_id','tag','slug'];
    protected $hidden = ['pivot'];
    public function news()
    {
        return $this->belongsToMany('App\News');
    }
}
