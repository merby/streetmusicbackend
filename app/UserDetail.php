<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDetail extends Model
{
  protected $table = 'user_details';
  protected $fillable = ['nik','npwp','foto_usaha','foto_selfie','user_id'];

  public function User()
   {
    return $this->belongsTo('App\User');
   }

}
