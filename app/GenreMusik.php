<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregatorOrderDetailTrack;

class GenreMusik extends Model
{
    protected $table = 'genre_musik';

    protected $fillable = [
        'name',
    ];

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    public function genreMusik()
    {
        return $this->hasMany(BusinessAggregatorOrderDetailTrack::class, 'genre_id', 'id');
    }
}
