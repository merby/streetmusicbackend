<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    //
    protected $table = 'news';
    protected $fillable = ['user_id', 'category_id', 'title', 'slug', 'content', 'file', 'banner_image', 'published_at', 'is_published'];
    protected $hidden = ['pivot'];
    public function category()
    {
        return $this->belongsTo('\App\NewsCategory')->withDefault();
    }

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function tags()
    {
        return $this->belongsToMany('App\Tags');
    }

}
