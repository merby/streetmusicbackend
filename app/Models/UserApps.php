<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserApps extends Model
{    
    public $primaryKey = 'uid';
    protected $table = 'user_apps';
    protected $fillable = [
        'uid',
        'fullname',
        'displayName',
        'photoURL',
        'email',
        'religion',
        'phone',
        'gender',
        'bio',
        'birthdate',
        'job',
        'address',
        'available',
        'additional',
        'position',
        'genre',
        'gears',
        'purposed',
    ];
    protected $casts = ['uid' => 'string'];

    public function bands()
    {
        return $this->hasMany(BandApps::class, 'owner_id','uid');
    }
    public function tracks()
    {
        return $this->belongsTo(TrackMusicianApps::class,"owner_id","uid");
        // return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
    }
}
