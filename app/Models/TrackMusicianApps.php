<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackMusicianApps extends Model
{
    //
    protected $table = 'track_musician_apps';
    protected $fillable = [
        'id',
        'owner_id',
        'genre',
        'playlist',
        'star',
        'starCount',
        'playlistCount',
        'playlistAdded',        
        'album',
        'type',
        'title',        
        'createdAt',
        'url'
    ];
    protected $casts = ['id' => 'string'];
    protected $hidden = ['pivot'];

//    function tracks(){
// 		return $this->belongsTo(BandApps::class,"id","owner_id");
//     }
public function users()
{        
    return $this->belongsTo(UserApps::class,"owner_id","uid");
}

}
