<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PodcastCategory extends Model
{
    //
    protected $table = 'podcast_category';
    protected $fillable = ['name','slug','enabled'];
}
