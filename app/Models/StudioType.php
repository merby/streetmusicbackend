<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudioType extends Model
{
    protected $fillable = ['name', 'description'];
}
