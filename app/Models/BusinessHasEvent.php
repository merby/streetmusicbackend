<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessHasEvent extends Model
{
    protected $fillable = ['business_id', 'event_id'];
    public $timestamps = false;
}
