<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class StudioPriceHour extends Model
{
    protected $fillable = [
        'studio_id',
        'start_time',
        'end_time',
        'price',
        'red_day'
    ];
}
