<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BandApps extends Model
{
    //
    protected $table = 'band_apps';
    protected $fillable = [
        'id',
        'owner_id',
        'genres',
        'url',
        'name',
        'contact',
        'bio',
        'website',
        'location',
        'personil',
        'createdAt',
    ];
    protected $casts = ['id' => 'string'];

    public function users()
    {        
        return $this->belongsTo(UserApps::class, 'owner_id', 'uid');
    }

    public function tracks()
    {
        return $this->belongsTo(TrackApps::class,"id","owner_id");
        // return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
    }
}
