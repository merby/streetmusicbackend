<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Compilations extends Model
{
    //
    protected $table = 'compilations';
    protected $fillable = [
      'id',
      'title',
      'image',
      'description',
      'is_published',
      'createdBy',
      'createdAt'
    ];

    public function tracks()
    {
        return $this->belongsToMany(TrackApps::class,"compilation_tracks","compilation_id");
    }
}
