<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

class Cafe extends Model
{
    use SoftDeletes;

    protected $table = 'cafe';
    protected $fillable = [
      'user_id',
      'name',
      'phone',
      'address',
      'city',
      'description',
      'logo',
      'coordinate',
      'is_active',
    ];

    public function cafe_menu()
    {
      return $this->hasMany(CafeMenu::class, 'cafe_id', 'id');
    }

    public function owner()
    {
      return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function cafe_image()
    {
      return $this->hasMany(CafeImage::class, 'cafe_id', 'id');
    }
}
