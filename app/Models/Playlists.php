<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Playlists extends Model
{
    //
    protected $table = 'playlists';
    protected $fillable = [
      'id',
      'uid',
      'name',   
      'default' 
    ];

    public function tracks()
    {
        return $this->belongsToMany(TrackApps::class,"playlist_tracks","playlist_id");
    }
}
