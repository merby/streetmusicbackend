<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CafeHasBusiness extends Model
{
    protected $fillable = ['cafe_id', 'business_id'];
    public $timestamps = false;
}
