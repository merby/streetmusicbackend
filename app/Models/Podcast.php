<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\PodcastCategory;
use App\User;

class Podcast extends Model
{
    use SoftDeletes;
    protected $table = 'podcast';
    protected $fillable = [
      'user_id',
      'category_id',
      'title',
      'slug',
      'description',
      'image',
      'file',
      'is_published',
    ];

    public function category()
    {
      return $this->belongsTo(PodcastCategory::class, 'category_id', 'id');
    }

    public function author()
    {
      return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
