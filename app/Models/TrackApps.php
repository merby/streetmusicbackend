<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackApps extends Model
{
    //
    protected $table = 'track_apps';
    protected $fillable = [
        'id',
        'owner_id',
        'genre',
        'playlist',
        'star',
        'starCount',
        'playlistCount',
        'playlistAdded',        
        'album',
        'type',
        'title',        
        'createdAt',
        'url',
        'visibility'
    ];
    protected $casts = ['id' => 'string'];
    protected $hidden = ['pivot'];

//    function tracks(){
// 		return $this->belongsTo(BandApps::class,"id","owner_id");
//     }
    
    public function bands()
    {        
        return $this->belongsTo(BandApps::class,"owner_id","id","idx");
    }

    public function compilations()
    {
        return $this->belongsToMany(Compilations::class,"compilation_tracks","track_apps_id");
    }

    public function playlists()
    {
        return $this->belongsToMany(Compilations::class,"playlist_tracks","track_apps_id");
    }
}
