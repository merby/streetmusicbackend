<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessHasStore extends Model
{
    protected $fillable = ['business_id', 'store_id'];
    public $timestamps = false;
}
