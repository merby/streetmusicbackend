<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CafeSchedule extends Model
{
    protected $table = 'cafe_schedule';
    protected $fillable = [
      'cafe_id',
      'title',
      'description',
      'image',
      'start_date',
      'end_date',
      'is_active',
    ];
}
