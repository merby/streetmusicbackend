<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregator;
use App\Models\Business\BusinessAggregatorOrders;

class BusinessAggregatorPackage extends Model
{
    //
    protected $table = 'business_aggregator_package';

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    public function businessAggregator()
    {
        return $this->belongsTo(BusinessAggregator::class, 'business_aggreator_id', 'id');
    }

    public function businessOrder()
    {
        return $this->hasMany(BusinessAggregatorOrders::class, 'price_id', 'id');
    }

    protected $fillable = [
        'business_id',
        'package_name',
        'price',
        'description',
    ];
}
