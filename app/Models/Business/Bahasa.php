<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregatorOrderDetailTrack;

class Bahasa extends Model
{
    protected $table = 'bahasa';

    protected $fillable = [
        'name',
        'type',
    ];

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    public function bahasaLirik()
    {
        return $this->hasMany(BusinessAggregatorOrderDetailTrack::class, 'bahasa_lirik_id', 'id');
    }

    public function bahasaRilis()
    {
        return $this->hasMany(BusinessAggregatorOrderDetailTrack::class, 'bahasa_rilis_id', 'id');
    }
}
