<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Business;

class BusinessCategories extends Model
{
    //
    protected $table = 'business_category';
    protected $fillable = ['name','description','image'];

    public function business()
    {
        return $this->hasMany(Business::class, 'business_category_id', 'id');
    }
}
