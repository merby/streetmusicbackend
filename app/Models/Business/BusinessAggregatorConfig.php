<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregator;

class BusinessAggregatorConfig extends Model
{
    //
    protected $table = 'business_aggregator_config';

    protected $fillable = [
        'id',
        'email_notification',
        'web_notification',
        'business_id',
    ];

    public function businessAggregator()
    {
        return $this->belongsTo(BusinessAggregator::class, 'business_aggreator_id', 'id');
    }

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;
}
