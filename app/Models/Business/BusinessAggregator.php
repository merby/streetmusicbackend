<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\Business;
use App\Models\Business\BusinessAggregatorConfig;
use App\Models\Business\BusinessAggregatorPackage;
use App\Models\Business\BusinessAggregatorOrders;

class BusinessAggregator extends Model
{
    //
    protected $table = 'business_aggregator';

    public function business()
    {
        return $this->belongsTo(Business::class, 'business_id', 'id');
    }

    public function businessAggregatorConfig()
    {
        return $this->hasMany(BusinessAggregatorConfig::class, 'business_aggreator_id', 'id');
    }

    public function businessAggregatorPackage()
    {
        return $this->hasMany(BusinessAggregatorConfig::class, 'business_aggreator_id', 'id');
    }

    public function businessAggregatorOrder()
    {
        return $this->hasMany(BusinessAggregatorOrders::class, 'business_aggreator_id', 'id');
    }

    protected $fillable = [
        'id',
        'name',
        'business_id',
        'description',
    ];

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;
}
