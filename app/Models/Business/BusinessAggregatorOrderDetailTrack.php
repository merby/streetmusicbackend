<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregatorOrdersDetails;
use App\Models\Business\Bahasa;
use App\Models\Business\GenreMusik;

class BusinessAggregatorOrderDetailTrack extends Model
{
    //
    protected $table = 'business_aggregator_order_details_track';

    public function ordersDetails()
    {
        return $this->belongsTo(BusinessAggregatorOrdersDetails::class, 'orders_details_id', 'id');
    }

    public function bahasaLirik()
    {
        return $this->belongsTo(Bahasa::class, 'bahasa_lirik_id', 'id');
    }

    public function bahasaRilis()
    {
        return $this->belongsTo(Bahasa::class, 'bahasa_rilis_id', 'id');
    }

    public function genreMusik()
    {
        return $this->belongsTo(GenreMusik::class, 'genre_id', 'id');
    }

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    protected $fillable = [
        'orders_details_id',
        'judul_rilis',
        'cover_rilis',
        'nama_artis',
        'link_spotify',
        'foto_artis',
        'nama_artis_tamu',
        'link_spotify_tamu',
        'genre_id',
        'genre_sekunder',
        'bahasa_rilis_id',
        'nama_hak_cipta',
        'tahun_rilis',
        'nama_master_rekaman',
        'tahun_master_rekaman',
        'informasi_rilis',
        'tanggal_rilis',
        'file_music',
        'set_start',
        'nama_pencipta',
        'nama_penulis',
        'nama_produser',
        'bahasa_lirik_id',
        'explicit',
        'lirik_lagu',
    ];
}
