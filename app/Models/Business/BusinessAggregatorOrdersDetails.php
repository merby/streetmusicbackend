<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregatorOrders;
use App\Models\Business\BusinessAggregatorOrderDetailTrack;

class BusinessAggregatorOrdersDetails extends Model
{
    //
    protected $table = 'business_aggregator_orders_details';

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    public function order()
    {
        return $this->belongsTo(BusinessAggregatorOrders::class, 'orders_id', 'id');
    }

    public function orderDetailTrack()
    {
        return $this->hasMany(BusinessAggregatorOrderDetailTrack::class, 'orders_details_id', 'id');
    }

    protected $fillable = [
        'orders_id',
        'email',
        'nama_lengkap',
        'nik',
        'phone_satu',
        'phone_dua',
        'jenis_rilis',
        'jumlah_lagu',
        'jumlah_artis',
        'channel_yt',
        'akun_ig',
        'akun_fb',
    ];
}
