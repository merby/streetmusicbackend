<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregator;
use App\Models\Business\BusinessAggregatorOrdersDetails;
use App\Models\Business\BusinessAggregatorPackage;

class BusinessAggregatorOrders extends Model
{
    //
    protected $table = 'business_aggregator_orders';

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;

    public function businessAggregator()
    {
        return $this->belongsTo(BusinessAggregator::class, 'business_aggreator_id', 'id');
    }

    public function businessPackage()
    {
        return $this->belongsTo(BusinessAggregatorPackage::class, 'price_id', 'id');
    }

    public function orderDetail()
    {
        return $this->hasMany(BusinessAggregatorOrdersDetails::class, 'orders_id', 'id');
    }

    protected $fillable = [
        'business_aggreator_id',
        'status',
        'request_name',
        'tanggal',
        'account_id',
        'keterangan',
        'price_id',
    ];
}
