<?php

namespace App\Models\Business;

use Illuminate\Database\Eloquent\Model;
use App\Models\Business\BusinessAggregator;
use App\Models\Business\BusinessCategories;

class Business extends Model
{
    protected $table = 'business';

    public function businessAggregator()
    {
        return $this->hasMany(BusinessAggregator::class, 'business_id', 'id');
    }

    public function businessCategories()
    {
        return $this->belongsTo(BusinessCategories::class, 'business_category_id', 'id');
    }

    protected $fillable = [
        'id',
        'name',
        'business_category_id',
        'description',
        'contact',
        'address',
        'image',
    ];

    /**
     * The attributes that disable timestamps on laravel.
     *
     * @var string
     */
    public $timestamps = false;
}
