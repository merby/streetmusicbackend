<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CafeScheduleBand extends Model
{
    protected $table = 'cafe_schedule_band';
    protected $fillable = [
      'cafe_schedule_id',
      'band_id',
      'pic_band',
      'pic_band_phone',
      'band_name',
      'song',
    ];
}
