<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CafeMenu extends Model
{
    protected $table = 'cafe_menu';
    protected $fillable = [
      'cafe_id',
      'name',
      'price',
      'description',
      'image',
      'is_active',
    ];

    public function caffe()
    {
        return $this->belongsTo(Caffe::class, 'cafe_id', 'id');
    }
}
