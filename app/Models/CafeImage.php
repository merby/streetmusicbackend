<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CafeImage extends Model
{
    protected $table = 'cafe_image';
    protected $fillable = [
      'cafe_id',
      'image',
    ];
}
