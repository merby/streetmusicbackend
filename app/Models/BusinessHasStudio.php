<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BusinessHasStudio extends Model
{
    protected $fillable = ['business_id', 'studio_id'];
    public $timestamps = false;
}
