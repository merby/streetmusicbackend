<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Studio extends Model
{
    protected $fillable = [
        'name',
        'studio_type_id',
        'description',
        'open_time',
        'close_time',
        'studioscol',        
        'location'
    ];
}
