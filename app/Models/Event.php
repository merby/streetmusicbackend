<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $table = 'events';
    protected $fillable = [
        'name',
        'start_date',
        'start_time',
        'end_date',
        'end_time',
        'photo',
        'description',
        'location',
        'coordinate',
        'city',
        'price',
        'ticket_distribution',
        'open_band',
        'type',
        'status',
        'slug',
        'is_published'
    ];

      
    public function participants()
    {
        return $this->belongsTo(EventParticipant::class, 'id', 'event_id');
    }
}
