<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventParticipant  extends Model
{
    //
    protected $table = 'event_participant';
    protected $fillable = [
        'event_id',
        'uid',
        'band_id',
        'add_info',        
    ];
   
    public function users()
    {
        return $this->belongsTo(UserApps::class, 'uid', 'uid');
    }

    public function events()
    {
        return $this->belongsTo(Event::class, 'event_id', 'id');
    }
}
