<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\NewsCategory;
use App\User;

class Banner extends Model
{
    use SoftDeletes;
    protected $table = 'banners';
    protected $fillable = [
      'user_id',
      'category_id',
      'title',
      'slug',
      'description',
      'image',
      'is_published',
    ];

    public function category()
    {
      return $this->belongsTo(NewsCategory::class, 'category_id', 'id');
    }

    public function author()
    {
      return $this->belongsTo(User::class, 'user_id', 'id');
    }
}
