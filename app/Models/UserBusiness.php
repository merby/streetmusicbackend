<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserBusiness extends Model
{
    protected $fillable = ['user_id', 'business_id'];
    public $timestamps = false;
}
