<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Banner;

class BannerController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
   public function getBannerByCategory(Request $request)
   {
       $req = $request->all();
       $banner = Banner::where('category_id', $req['category_id'])
          ->where('is_published', 1)
          ->orderByDesc('created_at')
          ->get();
       $data = $banner->take($req['limit']);

       return response()->json($data);
   }

   public function getBannerById(Request $request)
   {
      $data = Banner::find($request->input('id'));

      return response()->json($data);
   }
}
