<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\BandApps;
use App\Models\TrackApps;
use App\Models\UserApps;



class ArtistController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function GetArtistById(Request $request)
    {
        $req = $request->all();
        $banner = BandApps::find($req['id']);     
        $tracks = BandApps::select('track_apps.track_id as trackId','band_apps.id as ownerId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')
        ->join('track_apps','band_apps.id','=','track_apps.owner_id')
        ->where('band_apps.id','=',$req['id'])
        ->get();  

        
        $arr = Array("profile"=>$banner,"tracks"=>$tracks);
        return response()->json($arr);
    }

    public function GetTrackById(Request $request)
    {
        $req = $request->all();
        $tracks = TrackApps::select('track_apps.track_id as trackId','band_apps.id as ownerId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('track_apps.id','=',$req['id'])
        ->get();  

        
        return response()->json($tracks);
    }

    public function GetArtistSchedule(Request $request)
    {
        //riebase
        $arr = Array("status"=>"cooming_soon");
        return response()->json($arr);
    }


    public function createUserApps(Request $request)
    {
        $req = $request->all();
        $shopOwner = UserApps::updateOrCreate(
            ['uid' => $req['uid']],
            $request->all()
         );

    }

    public function createBandApps(Request $request)
    {
        $req = $request->all();
        $shopOwner = BandApps::updateOrCreate(
            ['id' => $req['id']],
            $request->all()
         );

    }

    public function createTrackApps(Request $request)
    {
        $req = $request->all();
        $shopOwner = TrackApps::updateOrCreate(
            ['id' => $req['id']],
            $request->all()
         );

    }
}
