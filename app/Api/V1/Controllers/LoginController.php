<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use Tymon\JWTAuth\JWTAuth;
use App\Http\Controllers\Controller;
use App\Api\V1\Requests\LoginRequest;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Auth;

use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

class LoginController extends Controller
{
    /**
     * Log the user in
     *
     * @param LoginRequest $request
     * @param JWTAuth $JWTAuth
     * @return \Illuminate\Http\JsonResponse
     */
    public function __construct()
    {
        $serviceAccount = ServiceAccount::fromJsonFile(base_path()."/resources/streetmusic-id-33e3f-firebase-adminsdk-3tdzj-7571c64cfd.json");
        $this->firebase = (new Factory) -> withServiceAccount($serviceAccount) -> create();

    }    

    public function login(LoginRequest $request, JWTAuth $JWTAuth)
    {
        
        $credentials = $request->only(['email', 'password']);
  
        try {
            $token = Auth::guard('api')->attempt($credentials);
            
            if(!$token) {
                // throw new AccessDeniedHttpException();
                return response()->json(['status'=>false, 'error' => 'invalid_credentials'], 401);
            }
            $user = Auth::guard('api')->user();
            // dd($user->id);
            $customToken = $this->firebase->getAuth()->createCustomToken($user->email);
            $customTokenString = (string) $customToken;

        } catch (JWTException $e) {
            throw new HttpException(500);
        }

        return response()
            ->json([
                'status' => 'ok',
                'token' => $token,
                'token_firebase'=>$customTokenString,
                'expires_in' => Auth::guard('api')->factory()->getTTL() * 60
            ]);
    }
}
