<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\BandApps;
use App\Models\UserApps;
use App\Models\TrackApps;
use DB;
class SearchController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
      public function searchByText(Request $request)
    {
        $req = $request->all();

        $artist = BandApps::where('name','LIKE','%'.$req['name'].'%')->get();           
        //$lagu = TrackApps::with('bands')->where('title','LIKE','%'.$req['name'].'%')->get();             
        $lagu = DB::table('track_apps')
->select('track_apps.owner_id as ownerId','track_apps.track_id as trackId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('title','LIKE','%'.$req['name'].'%')
        ->get();   
        $arr = Array(Array("artist"=>$artist,"tracks"=>$lagu));
        return response()->json($arr);
    }

    public function searchByMusician(Request $request)
    {
        $req = $request->all();
    	
        $sort = 'created_at';
        $order = 'desc';

		$name = $req['name'];
        $add = var_export($req['additional'],1);
        if($add=='false')
        {
            $add = '';
        }


        $rec = var_export($req['available'],1);
        if($rec=='false')
        {
            $rec = '';
        }

        

        $artist = UserApps::where(function ($query) use ($name) {
    	$query->where('fullname','LIKE','%'.$name.'%')->orWhere('displayName','LIKE','%'.$name.'%');
		})->where('address','LIKE','%'.$req['location'].'%')->where('position','LIKE','%'.$req['position'].'%')
        ->where('genre','LIKE','%'.$req['genre'].'%')        
        ->where(function ($query) use ($rec,$add) {

            if(($rec=='') && ($add==''))
            {
                
            }
            else
            {
                $query->where('available','=',$rec)->orWhere('additional','=',$add);
            }
            
        })
        // ->where('available','LIKE','%'.var_export($req['available'],1).'%')
        // ->where('additional','LIKE','%'.var_export($req['additional'],1).'%')
        ->orderBy($sort, $order)
        ->paginate($req['limit']);           
        //$lagu = TrackApps::with('bands')->where('title','LIKE','%'.$req['name'].'%')->get();             
        // $lagu = DB::table('track_apps')
        // ->select('track_apps.*','band_apps.name as artistName','band_apps.url as artistImage')
        // ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        // ->where('title','LIKE','%'.$req['keyword'].'%')
        // ->get();   
        // $arr = Array("artist"=>$artist);
        return response()->json($artist);
    }

    public function searchByBand(Request $request)
    {
        $req = $request->all();  
        $sort = 'created_at';
        $order = 'desc';
        $artist = BandApps::where('name','LIKE','%'.$req['name'].'%')->where('genres','LIKE','%'.$req['genre'].'%')->where('location','LIKE','%'.$req['location'].'%')->orderBy($sort, $order)->paginate($req['limit']);    
        // $arr = Array("page"=>$artist);
        return response()->json($artist);
    }

}
