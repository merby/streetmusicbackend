<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\News;
use App\NewsCategory;
use App\Tags;

class NewsController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
   public function GetNews(Request $request)
     {
         $req = $request->all();
         $data = News::where('slug','=',$req['slug'])->where('is_published',true)->with('user')->with('tags:tag')->get();
         return response()->json($data);
     }

    public function ListNews(Request $request)
    {       
        $req = $request->all();
 
        $sort = 'id';
        $order = 'desc';
        $data = News::where('is_published',true)->with('user')->with('tags:tag')->orderBy($sort, $order)->paginate($req['limit']);
        return response()->json($data);
    } 

    public function ListCategory()
    {
        $data = NewsCategory::all();
        return response()->json($data);
    }

    public function NewsByCategory(Request $request)
    {
        $req = $request->all();
        $limit = 5;
        $sort = 'id';
        $order = 'desc';
        // dd($req['tag']);
        $data = News::where('category_id','=',$req['category_id'])->with('tags:tag')->orderBy($sort, $order)->paginate($limit);
        return response()->json($data);      
    }

    public function NewsByTag(Request $request)
    {
        $req = $request->all();
        // dd($req['tag']);
        $data = Tags::where('tag','=',$req['tag'])->with('news')->get()->pluck('news')->flatten()->pluck('id');
        $limit = 5;
        $sort = 'id';
        $order = 'desc';
        //$data = News::where('category_id','=',$req['category_id'])->with('tags:tag')->get();
        $data = News::whereIn('id', $data)->with('tags:tag')->orderBy($sort, $order)->paginate($limit);
        return response()->json($data);    
    }
    
}
