<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Event;
use DB;

class EventController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListEvent(Request $request)
    {
        $req = $request->all();   
        

        if($req['latitude']!='' && $req['longitude']!='')
        {
            $cafes = DB::select("SELECT *,get_distance_in_miles_between_geo_locations('".$req['latitude']."','".$req['longitude']."',SUBSTRING_INDEX(SUBSTRING_INDEX(coordinate, ',', 1), ',', -1), SUBSTRING_INDEX(SUBSTRING_INDEX(coordinate, ',', 2), ',', -1))* 1.609344 AS distance FROM `events` order by distance limit ".$req['start'].",".$req['limit']);        

         }
        else if($req['city']!='')
        {
            $cafes = Event::select('*',DB::raw('0 as distance'))
            ->where('name','LIKE','%'.$req['name'].'%')
            ->where('city','=',$req['city'])
            ->where('is_published',true)

            ->offset($req['start'])
            ->limit($req['limit'])
            ->get();  
        }
        else
        {
            $cafes = Event::select('*',DB::raw('0 as distance'))
            ->where('name','LIKE','%'.$req['name'].'%')            
            ->where('is_published',true)
            ->offset($req['start'])
            ->limit($req['limit'])
            ->get();  
        }


        return response()->json($cafes);
    }

    public function getEvent(Request $request)
    {        
        $data = Event::find($request->input('id'));        
        $arr = Array("event_information"=>$data);
        return response()->json($data);
    }


}
