<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Business\Business;
use App\Models\Business\BusinessAggregatorOrdersDetails;
use App\Models\Business\BusinessAggregatorOrderDetailTrack;
use App\Models\Business\BusinessAggregatorOrders;
use App\Models\Business\BusinessAggregatorPackage;
use App\Models\Business\Bahasa;
use App\Models\Business\GenreMusik;
use DB;

class AggregatorApiController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    public function requestPost(Request $request)
    {
        $data = $request->all();

        try {
            DB::connection('mysql')->beginTransaction();
            $orders = BusinessAggregatorOrders::create([
              'business_aggreator_id' => $data['business_aggreator_id'],
              'status' => $data['status'],
              'request_name' => $data['request_name'],
              'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
              'account_id' => $data['account_id'],
              'keterangan' => $data['keterangan'],
            ]);

            $detail = BusinessAggregatorOrdersDetails::create([
              'orders_id' => $orders->id,
              'email' => $data['email'],
              'nama_lengkap' => $data['nama_lengkap'],
              'nik' => $data['nik'],
              'phone_satu' => $data['phone_satu'],
              'price_id' => $data['price_id'],
              'phone_dua' => $data['phone_dua'],
              'jenis_rilis' => $data['jenis_rilis'],
              'jumlah_lagu' => $data['jumlah_lagu'],
              'jumlah_artis' => $data['jumlah_artis'],
              'channel_yt' => $data['channel_yt'],
              'akun_ig' => $data['akun_ig'],
              'akun_fb' => $data['akun_fb'],
            ]);

            foreach ($data['track'] as $track) {
                if (isset($track['bahasa_lirik_baru'])) {
                  $bahasaLirik = Bahasa::create([
                    'name' => $track['bahasa_lirik_baru'],
                    'type' => 'Lirik',
                  ]);

                  $track['bahasa_lirik_id'] = $bahasaLirik->id;
                }

                if (isset($track['bahasa_rilis_baru'])) {
                  $bahasaRilis = Bahasa::create([
                    'name' => $track['bahasa_rilis_baru'],
                    'type' => 'Rilis',
                  ]);

                  $track['bahasa_rilis_id'] = $bahasaRilis->id;
                }

                BusinessAggregatorOrderDetailTrack::create([
                  'orders_details_id' => $detail->id,
                  'judul_rilis' => $track['judul_rilis'],
                  'cover_rilis' => $track['cover_rilis'],
                  'nama_artis' => $track['nama_artis'],
                  'link_spotify' => $track['link_spotify'],
                  'foto_artis' => $track['foto_artis'],
                  'nama_artis_tamu' => $track['nama_artis_tamu'],
                  'link_spotify_tamu' => $track['link_spotify_tamu'],
                  'genre_id' => $track['genre_id'],
                  'genre_sekunder' => $track['genre_sekunder'],
                  'bahasa_rilis_id' => $track['bahasa_rilis_id'],
                  'nama_hak_cipta' => $track['nama_hak_cipta'],
                  'tahun_rilis' => $track['tahun_rilis'],
                  'nama_master_rekaman' => $track['nama_master_rekaman'],
                  'tahun_master_rekaman' => $track['tahun_master_rekaman'],
                  'informasi_rilis' => $track['informasi_rilis'],
                  'tanggal_rilis' => $track['tanggal_rilis'],
                  'file_music' => $track['file_music'],
                  'set_start' => $track['set_start'],
                  'nama_pencipta' => $track['nama_pencipta'],
                  'nama_penulis' => $track['nama_penulis'],
                  'nama_produser' => $track['nama_produser'],
                  'bahasa_lirik_id' => $track['bahasa_lirik_id'],
                  'explicit' => $track['explicit'],
                  'lirik_lagu' => $track['lirik_lagu'],
                ]);
            }
            DB::connection('mysql')->commit();
        } catch (\Throwable $exp) {
            DB::connection('mysql')->rollback();
            return response()->json(['status' => false, 'error' => $exp->getMessage()], 500);
        }

        return response()->json([
          'status' => 'ok'
          ], 201);
    }

    public function getListAggregator()
    {
        $list = Business::where('business_category_id', 1)->get();

        return response()->json($list);
    }

    public function getListRequest(Request $request)
    {
        $data = $request->all();
        $list = BusinessAggregatorOrders::where('account_id', $data['account_id'])->get();
        $detail = [];
        foreach ($list as $key) {
            $track = [];

            foreach ($key->orderDetail[0]->orderDetailTrack as $value) {
                array_push($track, array('track_id' => $value->id,
                                         'judul_rilis' => $value->judul_rilis,
                                         'cover_rilis' => $value->cover_rilis,
                                         'nama_artis' => $value->nama_artis,
                                         'link_spotify' => $value->link_spotify,
                                         'foto_artis' => $value->foto_artis,
                                         'nama_artis_tamu' => $value->nama_artis_tamu,
                                         'link_spotify_tamu' => $value->link_spotify_tamu,
                                         'genre_id' => $value->genre_id,
                                         'genre_id' => $value->genre_sekunder,
                                         'bahasa_rilis_id' => $value->bahasa_rilis_id,
                                         'nama_hak_cipta' => $value->nama_hak_cipta,
                                         'tahun_rilis' => $value->tahun_rilis,
                                         'nama_master_rekaman' => $value->nama_master_rekaman,
                                         'tahun_master_rekaman' => $value->tahun_master_rekaman,
                                         'informasi_rilis' => $value->informasi_rilis,
                                         'tanggal_rilis' => $value->tanggal_rilis,
                                         'file_music' => $value->file_music,
                                         'set_start' => $value->set_start,
                                         'nama_pencipta' => $value->nama_pencipta,
                                         'nama_penulis' => $value->nama_penulis,
                                         'nama_produser' => $value->nama_produser,
                                         'bahasa_lirik_id' => $value->bahasa_lirik_id,
                                         'explicit' => $value->explicit,
                                         'lirik_lagu' => $value->lirik_lagu));
            }

            array_push($detail, array(
            'status' => $key->status,
            'request_name' => $key->request_name,
            'tanggal' => $key->tanggal,
            'price_id' => $key->price_id,
            'keterangan' => $key->keterangan,
            'order_id' => $key->id,
            'detail' => ['email' => $key->orderDetail[0]->email,
                         'nama_lengkap' => $key->orderDetail[0]->nama_lengkap,
                         'nik' => $key->orderDetail[0]->nik,
                         'phone_satu' => $key->orderDetail[0]->phone_satu,
                         'phone_dua' => $key->orderDetail[0]->phone_dua,
                         'jenis_rilis' => $key->orderDetail[0]->jenis_rilis,
                         'jumlah_lagu' => $key->orderDetail[0]->jumlah_lagu,
                         'jumlah_artis' => $key->orderDetail[0]->jumlah_artis,
                         'channel_yt' => $key->orderDetail[0]->channel_yt,
                         'akun_ig' => $key->orderDetail[0]->akun_ig,
                         'akun_fb' => $key->orderDetail[0]->akun_fb,
                         'track' => $track]));

        }


        return response()->json($detail);
    }

    public function getListPackage(Request $request)
    {
        $data = $request->all();
        $list = BusinessAggregatorPackage::where('business_aggreator_id', $data['id'])->first();

        return response()->json($list);
    }

    public function getListBahasaLirik()
    {
        $list = Bahasa::where('type', 'Lirik')->get();

        return response()->json($list);
    }

    public function getListBahasaRilis()
    {
        $list = Bahasa::where('type', 'Rilis')->get();

        return response()->json($list);
    }

    public function getListGenreMusik()
    {
        $list = GenreMusik::all();

        return response()->json($list);
    }

    public function getBahasaById(Request $request)
    {
        $data = $request->all();
        $list = Bahasa::where('id', $data['id'])->first();

        return response()->json($list);
    }

    public function getGereMusikById(Request $request)
    {
        $data = $request->all();
        $list = GenreMusik::where('id', $data['id'])->first();

        return response()->json($list);
    }

    public function updatePayment(Request $request)
    {
        $data = $request->all();

        try {
            BusinessAggregatorOrders::where('id', $data['order_id'])->update([
                'status' => 2,
            ]);
        } catch (\Throwable $exp) {
            return response()->json(['status' => false, 'error' => $exp->getMessage()], 500);
        }

        return response()->json([
          'status' => 'ok'
          ], 201);
    }

    public function requestUpdate(Request $request)
    {
        $data = $request->all();

        try {
            DB::connection('mysql')->beginTransaction();
            $orders = BusinessAggregatorOrders::where('id', $data['order_id'])->update([
              'status' => $data['status'],
              'request_name' => $data['request_name'],
              'tanggal' => date('Y-m-d', strtotime($data['tanggal'])),
              'price_id' => $data['price_id'],
              'account_id' => $data['account_id'],
              'keterangan' => $data['keterangan'],
            ]);

            $detail = BusinessAggregatorOrdersDetails::where('id', $data['order_id'])->update([
              'email' => $data['email'],
              'nama_lengkap' => $data['nama_lengkap'],
              'nik' => $data['nik'],
              'phone_satu' => $data['phone_satu'],
              'phone_dua' => $data['phone_dua'],
              'jenis_rilis' => $data['jenis_rilis'],
              'jumlah_lagu' => $data['jumlah_lagu'],
              'jumlah_artis' => $data['jumlah_artis'],
              'channel_yt' => $data['channel_yt'],
              'akun_ig' => $data['akun_ig'],
              'akun_fb' => $data['akun_fb'],
            ]);

            foreach ($data['track'] as $track) {
                if (isset($track['bahasa_lirik_baru'])) {
                  $bahasaLirik = Bahasa::create([
                    'name' => $track['bahasa_lirik_baru'],
                    'type' => 'Lirik',
                  ]);

                  $track['bahasa_lirik_id'] = $bahasaLirik->id;
                }

                if (isset($track['bahasa_rilis_baru'])) {
                  $bahasaRilis = Bahasa::create([
                    'name' => $track['bahasa_rilis_baru'],
                    'type' => 'Rilis',
                  ]);

                  $track['bahasa_rilis_id'] = $bahasaRilis->id;
                }

                BusinessAggregatorOrderDetailTrack::where('id', (int)$track['id'])->update([
                  'judul_rilis' => $track['judul_rilis'],
                  'cover_rilis' => $track['cover_rilis'],
                  'nama_artis' => $track['nama_artis'],
                  'link_spotify' => $track['link_spotify'],
                  'foto_artis' => $track['foto_artis'],
                  'nama_artis_tamu' => $track['nama_artis_tamu'],
                  'link_spotify_tamu' => $track['link_spotify_tamu'],
                  'genre_id' => $track['genre_id'],
                  'genre_sekunder' => $track['genre_sekunder'],
                  'bahasa_rilis_id' => $track['bahasa_rilis_id'],
                  'nama_hak_cipta' => $track['nama_hak_cipta'],
                  'tahun_rilis' => $track['tahun_rilis'],
                  'nama_master_rekaman' => $track['nama_master_rekaman'],
                  'tahun_master_rekaman' => $track['tahun_master_rekaman'],
                  'informasi_rilis' => $track['informasi_rilis'],
                  'tanggal_rilis' => $track['tanggal_rilis'],
                  'file_music' => $track['file_music'],
                  'set_start' => $track['set_start'],
                  'nama_pencipta' => $track['nama_pencipta'],
                  'nama_penulis' => $track['nama_penulis'],
                  'nama_produser' => $track['nama_produser'],
                  'bahasa_lirik_id' => $track['bahasa_lirik_id'],
                  'explicit' => $track['explicit'],
                  'lirik_lagu' => $track['lirik_lagu'],
                ]);
            }
            DB::connection('mysql')->commit();
        } catch (\Throwable $exp) {
            DB::connection('mysql')->rollback();
            return response()->json(['status' => false, 'error' => $exp->getMessage()], 500);
        }

        return response()->json([
          'status' => 'ok'
          ], 201);
    }
}
