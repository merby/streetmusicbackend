<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Cafe;
use DB;

class CafeController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getListCafe(Request $request)
    {
        $req = $request->all();   
        

        if($req['latitude']!='' && $req['longitude']!='')
        {
            $cafes = DB::select("SELECT *,round(get_distance_in_miles_between_geo_locations('".$req['latitude']."','".$req['longitude']."',SUBSTRING_INDEX(SUBSTRING_INDEX(coordinate, ',', 1), ',', -1), SUBSTRING_INDEX(SUBSTRING_INDEX(coordinate, ',', 2), ',', -1))* 1.609344,2) AS distance FROM cafe order by distance limit ".$req['start'].",".$req['limit']);        

         }
        else if($req['city']!='')
        {
            $cafes = Cafe::select('*',DB::raw('0 as distance'))
            ->where('name','LIKE','%'.$req['name'].'%')
            ->where('city','=',$req['city'])
            ->offset($req['start'])
            ->limit($req['limit'])
            ->get();  
        }
        else
        {
            $cafes = Cafe::select('*',DB::raw('0 as distance'))
            ->where('name','LIKE','%'.$req['name'].'%')            
            ->offset($req['start'])
            ->limit($req['limit'])
            ->get();  
        }


        return response()->json($cafes);
    }

    public function getCafe(Request $request)
    {        
        $data = Cafe::find($request->input('id'));        
        $arr = Array("cafe_information"=>$data,"cafe_menus"=>$data->cafe_menu,"cafe_image"=>$data->cafe_image);
        return response()->json($arr);
    }


}
