<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Playlists;
use DB;
class PlaylistController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function createPlaylist(Request $request)
    {
        $user = new Playlists($request->all());
        if(!$user->save()) {
            throw new HttpException(500);
        }
        return response()->json([
            'status' => 'ok'
        ], 201);
    }

    public function getMyPlaylist(Request $request)
    {
        $req = $request->all();
        $data = Playlists::where('uid', $req['uid'])
           ->get();        
 
        return response()->json($data);
    }

    public function getMyTracklist(Request $request)
    {        
        $data = Playlists::find($request->input('id'));
        $data2 = DB::table('playlists')
        ->select('track_apps.owner_id as ownerId','track_apps.track_id as trackId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')
        ->join('playlist_tracks','playlists.id','=','playlist_tracks.playlist_id')
        ->join('track_apps','track_apps.id','=','playlist_tracks.track_apps_id')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('playlists.id','=',$data->id)
        ->get();                
        $arr = Array("playlist_information"=>$data,"track_list"=>$data2);
        return response()->json($arr);
    }

    

    public function deletePlaylist(Request $request)
    {
       $req = $request->all();

       $data = Playlists::where('id', $req['id'])->delete();
       return response()->json([
        'status' => 'ok'
        ], 201);
    }

  public function setDefault(Request $request)
    {
       	$req = $request->all();
  		DB::statement("update playlists set `default`='0' where uid='".$req['uid']."'");
  
       	$flight = Playlists::find($req['id']);
		$flight->default = 1;
		$flight->save();
  
  		

       // $data = Playlists::where('id', $req['id'])->delete();
       return response()->json([
        'status' => 'ok'
        ], 201);
    }

    public function addToPlaylist(Request $request)
    {

        $req = $request->all();
        	
     	$dd = json_decode($req['tracks_id'], false);
    // dd($dd);    	
    	$dx = $req['tracks_id'];
    	$dx = str_replace("[","",$dx);
    	$dx = str_replace("]","",$dx);
    	$dx = explode(',', $dx);    	
    	$dx = array_map('trim', $dx);
        $data = Playlists::find($req['id']);                  
        $data->tracks()->sync($dx,false);
        return response()->json([
            'status' => 'ok'
            ], 201);
    }

    public function addToPlaylistBulk(Request $request)
    {
        $req = $request->all();
        $data = Playlists::find($req['id']);            

        $data->tracks()->sync($req['tracks_id'],false);
        return response()->json([
            'status' => 'ok'
            ], 201);

    }

    public function RemoveFromPlaylist(Request $request)
    {
        $req = $request->all();
        $data = Playlists::find($req['id']);            

        $data->tracks()->detach($req['tracks_id']);
        return response()->json([
            'status' => 'ok'
            ], 201);
    }

    public function RemoveFromPlaylistBulk(Request $request)
    {
        $req = $request->all();
        $data = Playlists::find($req['id']);            

        $data->tracks()->detach($req['tracks_id']);
        return response()->json([
            'status' => 'ok'
            ], 201);
    }
}
