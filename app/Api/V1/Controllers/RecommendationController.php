<?php

namespace App\Api\V1\Controllers;

use Symfony\Component\HttpKernel\Exception\HttpException;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Illuminate\Http\Request;
use App\Models\Genre;
use App\Models\TrackApps;
use App\Models\BandApps;
use App\Models\Podcast;
use App\Models\Compilations;
use DB;


class RecommendationController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('jwt.auth', []);
    }

    /**
     * Get the authenticated User
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getTrending(Request $request)
    {
        $data = DB::table('track_apps')
        ->select('track_apps.owner_id as ownerId','track_apps.track_id as trackId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->orderBy("track_apps.starCount","desc")
        ->orderBy("track_apps.playlistAdded","desc")
        ->limit(10)
        ->get();
        return response()->json($data);
    }

  	public function getPodcastCategory(Request $request)
    {
        $data = DB::table('podcast_category')     
        ->get();   
        return response()->json($data);
    }

	public function getPodcastByCategory(Request $request)
    {
            $req = $request->all();

       $banner = Podcast::select('podcast.*','podcast_category.name as category_name')->leftJoin('podcast_category', 'podcast.category_id', '=', 'podcast_category.id')->where('podcast.is_published', 1)
       	->where('podcast.category_id','=', $req['id'])
           ->orderByDesc('podcast.created_at')
           ->limit($req['limit'])
           ->offset($req['start'])
           ->get(); 
    
        return response()->json($banner);
    }

    public function getLatestPodcast(Request $request)
    {
        $req = $request->all();
        $banner = Podcast::select('podcast.*','podcast_category.name as category_name')->leftJoin('podcast_category', 'podcast.category_id', '=', 'podcast_category.id')->where('podcast.is_published', 1)
           ->orderByDesc('podcast.created_at')
           ->get();
        $data = $banner->take($req['limit']);
 
        return response()->json($data);
    }

    public function getPopularPodcast(Request $request)
    {
        $req = $request->all();
        $banner = Podcast::where('is_published', 1)
           ->orderByDesc('created_at')
           ->get();
        $data = $banner->take($req['limit']);
 
        return response()->json($data);
    }

    public function getLatestCompilation(Request $request)
    {
        $req = $request->all();
        $banner = Compilations::where('is_published', 1)
           ->orderByDesc('id')
           ->limit($req['limit'])
           ->offset($req['start'])
           ->get();
        //$data = $banner->skip($req['start'])->take($req['limit']);
 		if(count($banner)>0)
        {
        	return response()->json($banner);
        }
    	else
        {	
        	return response()->json(null);
        }
        
    }

    public function getPopularCompilation(Request $request)
    {
        $req = $request->all();
        $banner = Compilations::where('is_published', 1)
           ->orderByDesc('created_at')
           ->get();
        $data = $banner->take($req['limit']);
 
        return response()->json($data);
    }

    public function getCompilationTrack(Request $request)
    {
        $req = $request->all();
        $banner = Compilations::find($req['id']);    

        $data = DB::table('compilation_tracks')
        ->select('track_apps.owner_id as ownerId','track_apps.track_id as trackId','track_apps.track_id as mediaStoreId','track_apps.id as idx','track_apps.duration as duration','track_apps.title','track_apps.url as path','track_apps.album','band_apps.name as artist','band_apps.url as coverArt')
        ->join('track_apps','compilation_tracks.track_apps_id','=','track_apps.id')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('compilation_tracks.compilation_id','=',$req['id'])
        ->orderBy("compilation_tracks.id","asc")        
        ->get();
              
        $arr = Array("compilation_info"=>$banner,"track_list"=> $data);

        return response()->json($arr);
    }


    public function getListGenre()
    {        
        $data = Genre::all();        
        return response()->json($data);
    }

    public function getGenreById(Request $request)
    {        
        $data = Genre::find($request->input('id'));        
        return response()->json($data);
    }

    public function getLatestSongByGenre(Request $request)
    {
        $data = DB::table('track_apps')
        ->select('track_apps.*','band_apps.name as artistName','band_apps.url as artistImage')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('genre','LIKE','%"'.$request->genre.'"%')
        ->orderBy("track_apps.createdAt","desc")        
        ->limit(10)
        ->get();   
        return response()->json($data);
    }

    public function getPopularSongByGenre(Request $request)
    {
        $data = DB::table('track_apps')
        ->select('track_apps.*','band_apps.name as artistName','band_apps.url as artistImage')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('genre','LIKE','%"'.$request->genre.'"%')
        ->orderBy("track_apps.starCount","desc")
        ->orderBy("track_apps.playlistAdded","desc")
        ->limit(10)
        ->get();   
        return response()->json($data);

    }

    public function getLatestNewsGenre(Request $request)
    {

    }

    public function getLatestArtistDashboard(Request $request)
    {
        $data = BandApps::orderBy("createdAt","desc")->limit(10)->get();                
        return response()->json($data);
    }

    public function getLatestSongDashboard(Request $request)
    {
        $data = DB::table('track_apps')
        ->select('track_apps.*','band_apps.name as artistName','band_apps.url as artistImage')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->orderBy("track_apps.createdAt","desc")        
        ->limit(10)
        ->get();   
        return response()->json($data);
    }



}
