<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Business\BusinessCategories;
class BusinessCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $businessCategories = BusinessCategories::latest()->paginate(5);

        return view('admin.business-categories.index',compact('businessCategories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.business-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'name' => 'required',
            'description' => 'required',
             'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $imageName = time().'.'.$request->image->extension();
        $request->image->move(public_path('images'), $imageName);
        
          BusinessCategories::create([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'image' => 'images/' . $imageName,
          ]);


        return redirect()->route('business-categories.index')
                        ->with('success','category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(BusinessCategories $businessCategory)
    {
        return view('admin.business-categories.show',compact('businessCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(BusinessCategories $businessCategory)
    {
        //
        return view('admin.business-categories.edit',compact('businessCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, BusinessCategories $businessCategory)
    {
        //
        $request->validate([
            'name' => 'required',
            'description' => 'required',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
        ]);

        if (!is_null($request->file('image'))) {
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);
            $image = 'images/' . $imageName;
        }

        if (is_null($request->file('image'))) {
            $image = $request->input('oldImage');
        }

        $businessCategory->where('id',$request->input('id'))->update([
          'image' => $image,
          'name' => $request->input('name'),
          'description' => $request->input('description'),

        ]);


        return redirect()->route('business-categories.index')
                        ->with('success','category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(BusinessCategories $businessCategory)
    {
        //
        $businessCategory->delete();

        return redirect()->route('business-categories.index')
                        ->with('success','category deleted successfully');
    }
}
