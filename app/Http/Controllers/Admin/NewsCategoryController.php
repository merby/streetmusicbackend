<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\NewsCategory;
use App\Services\Slug;
class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $NewsCategories = NewsCategory::orderBy('id','asc')->paginate(5);
  
        return view('admin.news-categories.index',compact('NewsCategories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.news-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        
        $data = $request->all();
        $request->validate([
            'name' => 'required',          
        ]);
        $slug = new Slug();
        $data['slug'] = $slug->createSlug($request->name);
        $data['enabled'] = true;
        

        NewsCategory::create($data);
   
        return redirect()->route('news-categories.index')
                        ->with('success','category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(NewsCategory $newsCategory)
    {
        //
        return view('admin.news-categories.show',compact('newsCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsCategory $newsCategory)
    {
        //        
        return view('admin.news-categories.edit',compact('newsCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, NewsCategory $newsCategory)
    {
        //
         //
         $data = $request->all();
         $request->validate([
            'name' => 'required',           
        ]);
        $slug = new Slug();
        $data['slug'] = $slug->createSlug($request->name);
        $data['enabled'] = true;
        

        $newsCategory->update($data);
   
  
        return redirect()->route('news-categories.index')
                        ->with('success','category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsCategory $newsCategory)
    {
        //
        $newsCategory->delete();
  
        return redirect()->route('news-categories.index')
                        ->with('success','category deleted successfully');
    }
}
