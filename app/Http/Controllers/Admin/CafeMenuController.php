<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Cafe;
use App\Models\CafeMenu;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Str;

class CafeMenuController extends Controller
{
    public function index(Request $request)
    {
        $cafe = Cafe::find($request->input('cafe'));

        return view('admin.cafe-menu.index', compact('cafe'));
    }

    public function data(Request $request)
    {
        $data = CafeMenu::where('cafe_id', $request->input('cafe_id'))->get();

        return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);
    }

    public function create(Request $request)
    {
        return view('admin.cafe-menu.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'price' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();

            if (isset($data['isActive'])){
                $data['is_active'] = 1;
            }
            else{
                $data['is_active'] = 0;
            }

            if ($request->hasFile('image')) {
              $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
              $request->file('image')->move(public_path('cafe-menu-image'), $imageName);
              $image = url("/cafe-menu-image/" .  $imageName);
            }

            CafeMenu::create([
              'cafe_id' => $data['cafe_id'],
              'name' => $data['name'],
              'price' => $data['price'],
              'description' => $data['description'],
              'is_active' => $data['is_active'],
              'image' => $image,
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect(route('cafe.menu'). '?cafe='.$data['cafe_id'])->with('success','Menu cafe created successfully.');
    }

    public function edit(Request $request)
    {
        $data = CafeMenu::find($request->input('data'));

        return view('admin.cafe-menu.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $request->validate([
          'name' => 'required',
          'price' => 'required',
        ]);

        try {
            $data = $request->all();
            $update = CafeMenu::find($data['id']);
            $image = $update->image;

            if (isset($data['isActive'])){
                $data['is_active'] = 1;
            }
            else{
                $data['is_active'] = 0;
            }

            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('cafe-menu-image'), $imageName);
                $image = url("/cafe-menu-image/" .  $imageName);

                if(\File::exists(public_path('cafe-menu-image/' . $path['basename']))) \File::delete(public_path('cafe-menu-image/' . $path['basename']));
            }

            $update->name = $data['name'];
            $update->price = $data['price'];
            $update->description = $data['description'];
            $update->is_active = $data['is_active'];
            $update->image = $image;
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data']);
        }

        return redirect(route('cafe.menu'). '?cafe='.$data['cafe_id'])->with('success','Menu cafe updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = CafeMenu::find($request->input('id'));

        return response()->json(view('admin.cafe-menu.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $delete = CafeMenu::find($request->input('id'));
            $cafe_id = $delete->cafe_id;
            $path = pathinfo($delete->image);
            if(\File::exists(public_path('cafe-menu-image/' . $path['basename']))) \File::delete(public_path('cafe-menu-image/' . $path['basename']));
            $delete->delete();
        } catch (\Exception $e) {
            return redirect()->route('cafe-menu')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect(route('cafe.menu'). '?cafe='.$cafe_id)->with('success','Menu cafe deleted successfully.');
    }
}
