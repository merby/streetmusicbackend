<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Compilations;
use App\NewsCategory;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use Storage;
use Illuminate\Support\Str;
use Carbon\Carbon;

class CompilationController extends Controller
{
    public function index()
    {
        return view('admin.compilation.index');
    }

    public function data()
    {
        $data = Compilations::all();
        return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);
    }

    public function create(Request $request)
    {        
        return view('admin.compilation.create');
    }

    public function store(Request $request)
    {
        $request->validate([            
            'title' => 'required|max:255',
            'description' => 'required|max:255',         
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'tracks'=> 'required'
        ]);

        try {
            $data = $request->all();

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }


            if ($request->hasFile('image')) {
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('banners'), $imageName);
                $image = url("/banners/" .  $imageName);
              }

          
            $tracks = json_decode($data['tracks']);    
            $data = Compilations::create([                            
              'title' => $data['title'],
              'description' => $data['description'],
              'is_published' => $data['is_published'],
              'image' => $image,
              'createdBy'=>auth()->user()->id,
              'createdAt'=> Carbon::now()
            ]);        
            
            $tagIds = [];
            foreach($tracks as $trc)
            {;
                $tagIds[] = $trc->id;
            }
            $data->tracks()->sync($tagIds);
                


        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data'. $e]);
        }

        return redirect()->route('compilation')->with('success','compilation created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Compilations::find($request->input('data'));
        $data2 = DB::table('compilations')
        ->select('track_apps.id','track_apps.title','band_apps.name as owner','track_apps.album','track_apps.genre')
        ->join('compilation_tracks','compilations.id','=','compilation_tracks.compilation_id')
        ->join('track_apps','track_apps.id','=','compilation_tracks.track_apps_id')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('compilations.id','=',$data->id)
        ->get();        
        $tracks = json_encode($data2);
        return view('admin.compilation.edit', compact('data','tracks'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'title' => 'required|max:255',
            'description' => 'required|max:255',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Compilations::find($data['id']);
            $image = $update->image;

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }
            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = $data['icon_name'].'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('banners'), $imageName);
                $image = url("/banners/" .  $imageName);

                if(\File::exists(public_path('banners/' . $path['basename']))) \File::delete(public_path('banners/' . $path['basename']));
            }            
            $update->title = $data['title'];            
            $update->description = $data['description'];
            $update->is_published = $data['is_published'];
            $update->image = $image;
            $update->save();
            $tracks = json_decode($data['tracks']);
            $tagIds = [];
            foreach($tracks as $trc)
            {;
                $tagIds[] = $trc->id;
            }
            $update->tracks()->sync($tagIds);
                
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data'.$e]);
        }

        return redirect()->route('compilation')->with('success','Genre updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Compilations::find($request->input('id'));
     
        return response()->json(view('admin.compilation.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $banner = Compilations::find($request->input('id'));
            $banner->tracks()->detach();
            $banner->delete();
        } catch (\Exception $e) {
            return redirect()->route('compilation')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('compilation')->with('success','Compilations deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Genre::find($request->input('data'));

        return view('admin.compilation.show', compact('data'));
    }
}
