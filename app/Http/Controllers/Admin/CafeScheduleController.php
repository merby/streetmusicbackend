<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Cafe;
use App\Models\CafeSchedule;
use Yajra\Datatables\Datatables;

class CafeScheduleController extends Controller
{
    public function index(Request $request)
    {
        $cafe = Cafe::find($request->input('cafe'));

        return view('admin.cafe-schedule.index', compact('cafe'));
    }

    public function data(Request $request)
    {
        $data = CafeSchedule::where('cafe_id', $request->input('cafe_id'))->get();

        return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);
    }

    public function create(Request $request)
    {
        return view('admin.cafe-schedule.create');
    }
}
