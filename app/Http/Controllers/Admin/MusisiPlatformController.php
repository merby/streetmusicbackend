<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\UserApps;
use App\Models\BandApps;
use App\Models\TrackApps;
use App\Models\TrackMusicianApps;
use App\NewsCategory;
use App\Services\SlugNews;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;

class MusisiPlatformController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('admin.musisi-platform.index');
    }

    public function track()
    {
        //
        return view('admin.musisi-platform.track');
    }

    public function trackMusician()
    {
        //
        return view('admin.musisi-platform.track-musician');
    }

    public function band()
    {
        //
        return view('admin.musisi-platform.band');
    }

    public function data()
    {
        $data = UserApps::all();
        // dd($data);

        return Datatables::of($data)
          ->addIndexColumn()
        //   ->addColumn('category', function($data){
        //     return $data->category->name;
        //   })
        //   ->addColumn('positions', function($data){
        //     $data = "";
        //     foreach($data as $dat){
        //         $data = $data.",".$dat;
        //     }
        //     return $data;
        //   })
          ->make(true);
    }

    public function dataBand()
    {
        //$data = BandApps::all();
        // dd($data->pivot->tracko);
        // dd($price = $model->tracks()->findOrFail($tracks->band_apps_id, )->pivot->tracko);
        $data = BandApps::all();



        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('owner', function($data){
            return $data->users->displayName;
          })
          ->addColumn('phone', function($data){
            return $data->users->phone;
          })
          ->make(true);
    }

    public function dataTrack()
    {
        $data = TrackApps::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('owner', function($data){
            return $data->bands->name;
          })
          ->make(true);
    }

    public function dataTrackMusician()
    {
        $data = TrackMusicianApps::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('owner', function($data){
            return $data->users->displayName;
          })
          ->make(true);
    }

    public function sync()
    {
        // return redirect('musisi-platform')->with('success','syncronize succesfull.');
        // return redirect()->route('musisi-platform.index');
        
       $json = json_decode(file_get_contents('https://us-central1-streetmusic-id.cloudfunctions.net/getUsers'), true);
        // $path = storage_path() . "/users.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        // $json = json_decode(file_get_contents($path), true); 
    
        try{
            DB::beginTransaction();
            //bas
 

            foreach($json as $value)
            {
                //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
                $fullname;
                $phone="";
                $gender="";
                $bio="";
                $birthdate="1901-01-01";
                $job = "";
                $address ="";
                $religion = "";
                //adv
                $available =0;
                $additional=0;
                $position="";
                $genre="";
                $gears="";
                //standar
                $uid="";            
                $displayName="";
                $photoURL="";
                $email="";
                   //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
                if(array_key_exists('uid', $value)) 
                {
                    $uid=$value['uid'];
                }
                if(array_key_exists('displayName', $value)) 
                {
                    $displayName=$value['displayName'];
                }
                if(array_key_exists('photoURL', $value)) 
                {
                    $photoURL=$value['photoURL'];
                }
                if(array_key_exists('email', $value)) 
                {
                    $email=$value['email'];
                }

                if(array_key_exists('basic_info', $value)) {
                    //key exists, do stuff
                    $fullname=$value['basic_info']['fullname'];
                    $religion=$value['basic_info']['religion'];
                    $phone=$value['basic_info']['phone'];
                    $gender=$value['basic_info']['gender'];
                    $bio=$value['basic_info']['bio'];
                    $birthdate=$value['basic_info']['birthdate'];
                    $job=$value['basic_info']['job'];
                    $address=$value['basic_info']['address'];
                }
                
               
                if(array_key_exists('advance_info', $value)) 
                {
                    if($value['advance_info']['available']==null)
                    {
                        $available = 0;
                    }
                    else
                    {
                        $available = $value['advance_info']['available'];
                    }

                    if($value['advance_info']['additional']==null)
                    {
                        $additional = 0;
                    }
                    else
                    {
                        $additional = $value['advance_info']['additional'];
                    }

                   
                  
                                    
                    if(array_key_exists('gears',$value['advance_info'])) 
                    {         
                        
                        $gears=$value['advance_info']['gears'];                         
                    }
                    if(array_key_exists('genre', $value['advance_info'])) 
                    {                        
                        $genre=$value['advance_info']['genre'];   
                    }
                    if(array_key_exists('position', $value['advance_info'])) 
                    {                        
                        $position=$value['advance_info']['position'];
                    }
                    
                   
                }
         

                $data=array(
                    "displayName"=>$displayName,
                    "photoURL"=>$photoURL,
                    "email"=>$email,
                    "religion"=>$religion,                 
                    "fullname"=>$fullname,        
                    "phone"=> $phone,
                    "gender"=>$gender,
                    "bio"=>$bio,
                    "birthdate"=> $birthdate,
                    "job"=> $job,
                    "address"=> $address,                                
                    "available"=>$available,
                    "additional"=>$additional,
                    "gears"=>json_encode($gears),
                    "position"=>json_encode($position),
                    "genre"=>json_encode($genre)
                );
                if($uid!="")
                {
                    UserApps::updateOrCreate(["uid"=>$uid],$data);
                }
                
            }
            DB::commit();
        }catch(\Exception $e){            
            dd($e,$uid);
            DB::rollback();
        }






        
        return redirect()->route('musisi-platform')
        ->with('success','syncronize.');
    }


    public function syncBand()
    {
        // return redirect('musisi-platform')->with('success','syncronize succesfull.');
        // return redirect()->route('musisi-platform.index');
        
         $json = json_decode(file_get_contents('https://us-central1-streetmusic-id.cloudfunctions.net/getBands'), true);
         //$path = storage_path() . "/bands.json"; // ie: /var/www/laravel/app/storage/json/filename.json

         //$json = json_decode(file_get_contents($path), true); 
     
        try{
            DB::beginTransaction();
            //bas
 

            foreach($json as $value)
            {
                //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
                if(array_key_exists('id',$value) && array_key_exists('uid',$value)) 
                {
                $url="";
                $id="";
                $name="";
                $contact="";
                $bio="";
                $website = "";
                $location ="";
                $genres = "";          
                $uid="";            
                $personil="";         
                $createdAt="";
               
                   //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
               
                if(array_key_exists('url',$value)) 
                {         
                    $url=$value['url'];                    
                }
                //key exists, do stuff
               
                $id=$value['id'];
                $name=$value['name'];
                $contact=$value['contact'];
                $bio=$value['bio'];
                $website=$value['website'];
                $location=$value['location'];
                $uid=$value['uid'];                               
                $genres=$value['genres'];
                $personil=$value['personil'];
                $createdAt=date("Y-m-d H:i:s", strtotime($value['createdAt'])); //returns an integer epoch time: 1401339270
            
         
                $data=array(
                    "url"=>$url,
                    "name"=>$name,
                    "contact"=>$contact,
                    "bio"=>$bio,                 
                    "website"=>$website,        
                    "createdAt"=>$createdAt,        
                    "location"=> $location,
                    "owner_id"=>$uid,
                    "genres"=>json_encode($genres),
                    "personil"=>json_encode($personil)
                );        
                $band = BandApps::updateOrCreate(["id"=>$id],$data);          
                // $user = UserApps::find($uid);          
                // $users = DB::table('user_has_band')
                //      ->select(DB::raw('*'))
                //      ->where('user_apps_id', "=", $uid)
                //      ->where('band_apps_id', "=", $id)
                //      ->get()->count();
        
                
                // if($users==0)
                // {
                //     $user->bands()->attach($id,array("user_apps_id"=>$uid));
                // }
            }
                
               
              
                
            }
            DB::commit();
        }catch(\Exception $e){            
            dd($e,$json);
            DB::rollback();
        }
        return redirect()->route('musisi-platform')
        ->with('success','syncronize.');
    }


    public function syncTrack()
    {
        // return redirect('musisi-platform')->with('success','syncronize succesfull.');
        // return redirect()->route('musisi-platform.index');
        
        $json = json_decode(file_get_contents('https://us-central1-streetmusic-id.cloudfunctions.net/getTracks'), true);
        // $path = storage_path() . "/tracks.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        //  $json = json_decode(file_get_contents($path), true); 
     
        try{
            DB::beginTransaction();
            //bas
            foreach($json as $value)
            {
                //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
           
                $id="";
                $genre="";
                $playlist="";
                $star="";
                $starCount = 0;
                $playlistCount =0;
                $playlistAdded = 0;          
                $album="";            
                $type="";         
                $title="";
                $createdAt="";         
                $url="";
               
                   //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
               

                //key exists, do stuff
                
                $id=$value['id'];
                $author=$value['author'];
                $genre=$value['genre'];
                $album=$value['album'];            
                $type=$value['type'];         
                $title=$value['title'];
                $createdAt=date("Y-m-d H:i:s", strtotime($value['createdAt'])); //returns an integer epoch time: 1401339270
                $url=$value['url'];

                if(array_key_exists('playlist',$value)) 
                {   
                    $playlist=$value['playlist'];
                }
                if(array_key_exists('star',$value)) 
                {   
                    $star=$value['star'];
                }
                if(array_key_exists('starCount',$value)) 
                { 
                    $starCount = $value['starCount'];
                }
                if(array_key_exists('playlistCount',$value)) 
                { 
                    $playlistCount =$value['playlistCount'];
                }
                if(array_key_exists('playlistAdded',$value)) 
                { 
                    $playlistAdded = $value['playlistAdded'];          
                }
                
               
         
                $data=array(
                    "genre"=>json_encode($genre),
                    "playlist"=>json_encode($playlist),
                    "star"=>json_encode($star),
                    "starCount"=>$starCount,
                    "playlistCount"=>$playlistCount,
                    "playlistAdded"=>$playlistAdded,        
                    "album"=>$album,
                    "owner_id"=>$author,
                    "type"=>$type,
                    "title"=>$title,        
                    "createdAt"=>$createdAt,
                    "url"=>$url
                );        
                $track = TrackApps::updateOrCreate(["id"=>$id],$data);          
                // $band = BandApps::find($author);          

                // if($band)
                // {
                //     $check = DB::table('band_has_tracks')
                //         ->select(DB::raw('*'))
                //         ->where('track_apps_id', "=", $id)
                //         ->where('band_apps_id', "=", $author)
                //         ->get()->count();
                //     if($check==0)
                //     {
                //         $band->tracks()->attach($id,array("band_apps_id"=>$author));
                //     }
                // }
                // else
                // {
                //     dd($track);
                // }
                
               
              
                
            }
            DB::commit();
        }catch(\Exception $e){            
            dd($e,$data);
            DB::rollback();
        }
        return redirect()->route('musisi-platform')
        ->with('success','syncronize.');
    }


    public function syncTrackMusician()
    {
        // return redirect('musisi-platform')->with('success','syncronize succesfull.');
        // return redirect()->route('musisi-platform.index');
        
        $json = json_decode(file_get_contents('https://us-central1-streetmusic-id.cloudfunctions.net/getAllTracks'), true);
        // $path = storage_path() . "/tracks.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        //  $json = json_decode(file_get_contents($path), true); 
     
        try{
            DB::beginTransaction();
            //bas
            foreach($json as $value)
            {
                //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
           
                $id="";
                $genre="";
                $playlist="";
                $star="";
                $starCount = 0;
                $playlistCount =0;
                $playlistAdded = 0;          
                $album="";            
                $type="";         
                $title="";
                $createdAt="1991-03-04";         
                $url="";
               
                   //uid, fullname, displayName, photoURL, email, religion, phone, gender, bio, birhdate,
                //job, address, available, additional, position, genre, deleted_at, created_at, updated_at
               

                //key exists, do stuff
                
          

                if(array_key_exists('title',$value)) 
                {   
                    $title=$value['title'];
                }
                if(array_key_exists('url',$value)) 
                {   
                    $url=$value['url'];
                }

                if(array_key_exists('album',$value)) 
                {   
                    $album=$value['album'];
                }

                if(array_key_exists('type',$value)) 
                {   
                    $type=$value['type'];
                }

                if(array_key_exists('genre',$value)) 
                {   
                    $genre=$value['genre'];
                }
                if(array_key_exists('author',$value)) 
                {   
                    $author=$value['author'];
                }
                if(array_key_exists('id',$value)) 
                {   
                    $id=$value['id'];
                }
                if(array_key_exists('createdAt',$value)) 
                {   
                    $createdAt=date("Y-m-d H:i:s", strtotime($value['createdAt'])); ;
                }
                if(array_key_exists('playlist',$value)) 
                {   
                    $playlist=$value['playlist'];
                }
                if(array_key_exists('star',$value)) 
                {   
                    $star=$value['star'];
                }
                if(array_key_exists('starCount',$value)) 
                { 
                    $starCount = $value['starCount'];
                }
                if(array_key_exists('playlistCount',$value)) 
                { 
                    $playlistCount =$value['playlistCount'];
                }
                if(array_key_exists('playlistAdded',$value)) 
                { 
                    $playlistAdded = $value['playlistAdded'];          
                }
                $data=array(
                    "genre"=>json_encode($genre),
                    "playlist"=>json_encode($playlist),
                    "star"=>json_encode($star),
                    "starCount"=>$starCount,
                    "playlistCount"=>$playlistCount,
                    "playlistAdded"=>$playlistAdded,        
                    "album"=>$album,
                    "owner_id"=>$author,
                    "type"=>$type,
                    "title"=>$title,        
                    "createdAt"=>$createdAt,
                    "url"=>$url,                    
                );        
                if(array_key_exists('isBand',$value)) 
                {   
                 
                }
                else
                {
                    if($id<>"")
                    {
                   
                    $track = TrackMusicianApps::updateOrCreate(["id"=>$id],$data);  
                    }
                }
         
                      
                // $band = BandApps::find($author);          

                // if($band)
                // {
                //     $check = DB::table('band_has_tracks')
                //         ->select(DB::raw('*'))
                //         ->where('track_apps_id', "=", $id)
                //         ->where('band_apps_id', "=", $author)
                //         ->get()->count();
                //     if($check==0)
                //     {
                //         $band->tracks()->attach($id,array("band_apps_id"=>$author));
                //     }
                // }
                // else
                // {
                //     dd($track);
                // }
                
               
              
                
            }
            DB::commit();
        }catch(\Exception $e){            
            dd($e,$data);
            DB::rollback();
        }
        return redirect()->route('musisi-platform')
        ->with('success','syncronize.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = NewsCategory::all();
        return view('news.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
       
        $data = $request->all();
        
    

        $request->validate([
            'title' => 'required',          
            'category_id' => 'required',
            'content' => 'required',
        ]);
        $slug = new SlugNews();
        $data['slug'] = $slug->createSlug($request->title);        
        if(isset($data['isPublished']))
        {
            $data['is_published'] = true;
        }
        else
        {
            $data['is_published'] = false;
        }
        $data['user_id'] = Auth::guard()->user()->id;
        $data['file'] = '';
        
        $data['published_at'] = \Carbon\Carbon::now();
        
        
        // $img = explode(',',$data['image'])[1];
        // $extension = explode('/', mime_content_type($data['image']))[1];
        // $fileStream = fopen(public_path() . "/news_img/" .  $data['slug'] .".".$extension, "wb"); 
        if ($request->hasFile('image')) {
        $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('news_img'), $imageName);        
        }
        $data['banner_image'] = url("/news_img/" .  $imageName);
        $data['file'] = '';

        // $data['banner_image'] = url("/news_img/" .  $data['slug'] .".".$extension);
        // $data['file'] = public_path() ."/news_img/" .  $data['slug'] .".".$extension;

        // fwrite($fileStream, base64_decode($img)); 
        // fclose($fileStream); 
        
        //tag
        $data['file'];

        try{
            DB::beginTransaction();
        
            $post = News::create($data);      
            if($post)
            {        
                $tagNames = explode(',',$data['tags']);
                      
                $tagIds = [];
                foreach($tagNames as $tagName)
                {
                    //$post->tags()->create(['name'=>$tagName]);
                    //Or to take care of avoiding duplication of Tag
                    //you could substitute the above line as
                    $tag = \App\Tags::firstOrCreate(['tag'=>$tagName,'slug'=>'']);
                    if($tag)
                    {
                    $tagIds[] = $tag->id;
                    }
    
                }
                $post->tags()->sync($tagIds);
            }
       
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
       
        return redirect()->route('news.index')
                        ->with('success','news created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
        return view('news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
        
        $tagx = $news->tags;
        $tags = array();
        
        foreach($tagx as $tag)
        {            
            array_push($tags,$tag->tag);
        }
        
        $news->tags = implode(',', $tags);

        //image
        // $b64image = "data:image/jpeg;base64,".base64_encode(file_get_contents($news->file));   
  
        //$news->banner_image =$news->file;

        $category = NewsCategory::all();

        return view('news.edit',compact('news','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
        $data = $request->all();
        
    

        $request->validate([
            'title' => 'required',          
            'category_id' => 'required',
            'content' => 'required',
        ]);
        $slug = new SlugNews();
        $data['slug'] = $slug->createSlug($request->title);        
        if(isset($data['isPublished']))
        {
            $data['is_published'] = true;
        }
        else
        {
            $data['is_published'] = false;
        }
        $data['user_id'] = Auth::guard()->user()->id;
        $data['file'] = '';
        
        $data['published_at'] = \Carbon\Carbon::now();
        
        
        // $img = explode(',',$data['image'])[1];
        // $extension = explode('/', mime_content_type($data['image']))[1];
        // $fileStream = fopen(public_path() . "/news_img/" .  $data['slug'] .".".$extension, "wb"); 
        if ($request->hasFile('image')) {
        $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('news_img'), $imageName);        
        $data['banner_image'] = url("/news_img/" .  $imageName);
        $data['file'] = '';
        }
        
        

        // $data['banner_image'] = url("/news_img/" .  $data['slug'] .".".$extension);
        // $data['file'] = public_path() ."/news_img/" .  $data['slug'] .".".$extension;

        // fwrite($fileStream, base64_decode($img)); 
        // fclose($fileStream); 
        
        //tag
        $data['file'];

        try{
            DB::beginTransaction();
        
            $post2 = $news->update($data);   
            $post = $news; 
               
            if($post2)
            {        
                $tagNames = explode(',',$data['tags']);
                      
                $tagIds = [];
                foreach($tagNames as $tagName)
                {
                    //$post->tags()->create(['name'=>$tagName]);
                    //Or to take care of avoiding duplication of Tag
                    //you could substitute the above line as
                    $tag = \App\Tags::firstOrCreate(['tag'=>$tagName,'slug'=>'']);
                    if($tag)
                    {
                    $tagIds[] = $tag->id;
                    }
    
                }                
                $post->tags()->sync($tagIds);
            }
       
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
       
        return redirect()->route('news.index')
                        ->with('success','news created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
        $news->delete();
  
        return redirect()->route('news.index')
                        ->with('success','news deleted successfully');
    }
}
