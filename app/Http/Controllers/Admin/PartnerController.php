<?php

namespace App\Http\Controllers\Admin;
use App\User;
use App\UserDetail;
use App\Role;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;
class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      //
      // if(auth()->user()->roles->pluck('name')[0] !== 'Admin'){
      //   $partner=User::select('users.*','role_user.role_id as role_id')->leftJoin('role_user', 'users.id', 'role_user.user_id')->where('role_id','!=',1)
      //     ->paginate(5);
      // }
      // if(auth()->user()->roles->pluck('name')[0] === 'Admin'){
      //       $partner = User::latest()->paginate(5);
      // }
      $partner = User::latest()->where('name','!=','Admin')->paginate(5);
      return view('admin.partner.index',compact('partner'))
          ->with('i', (request()->input('page', 1) - 1) * 5);

        return view('admin.partner.index');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.partner.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $request->validate([
          'name' => 'required',
          'email'=> 'required|email',
          'password' => 'required|alphaNum|min:6',
          'nik' => 'required|numeric',
          'npwp' => 'required|numeric',
           'foto_usaha' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'foto_selfie' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      try {
        DB::connection('mysql')->beginTransaction();
        $data = $request->all();
        $imageUsaha = time().'.'.$request->foto_usaha->extension();
        $imageSelfie = time().'.'.$request->foto_selfie->extension();
        $request->foto_usaha->move(public_path('images'), $imageUsaha);
        $request->foto_selfie->move(public_path('images'), $imageSelfie);

        $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => $data['password'],
          'isActivated' => 0,
          'emailVerification' => 0,
        ]);
        UserDetail::create([
          'user_id' => (int)$user->id,
          'nik' => $data['nik'],
          'npwp' => $data['npwp'],
          'foto_usaha' => 'images/' . $imageUsaha,
          'foto_selfie' =>'images/' . $imageSelfie,
        ]);


        DB::connection('mysql')->commit();
    } catch (\Exception $e) {
        dd($e);
        DB::connection('mysql')->rollback();
        return redirect()->back()->withInput($request->input())
          ->withErrors(['Something wrong, unable to save data']);
    }

    return redirect()->route('partner.index')
                    ->with('success','Partner created successfully.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show(User $partner)
     {
         return view('admin.partner.show',compact('partner'));
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $partner)
    {
        return view('admin.partner.edit',compact('partner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,User $partner)
    {
      $request->validate([
          'name' => 'required',
          'email' => 'required',
          'foto_usaha'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048'
      ]);

      //foto usaha
      if (!is_null($request->file('foto_usaha'))) {
          $pict_fotousaha = time().'.'.$request->foto_usaha->extension();
          $request->foto_usaha->move(public_path('images'), $pict_fotousaha);
          $foto_usaha = 'images/' . $pict_fotousaha;
      }

      if (is_null($request->file('foto_usaha'))) {
          $foto_usaha = $request->input('oldImage');
      }
      //foto selfie
      if (!is_null($request->file('foto_selfie'))) {
          $pict_fotoselfie = time().'.'.$request->foto_selfie->extension();
          $request->foto_selfie->move(public_path('images'), $pict_fotoselfie);
          $foto_selfie = 'images/' . $pict_fotoselfie;
      }

      if (is_null($request->file('foto_selfie'))) {
          $foto_selfie = $request->input('oldImage');
      }

      $partner->where('id',$request->input('id'))->update([

        'name' => $request->input('name'),
        'email' => $request->input('email'),
        'emailVerification' => $request->input('emailVerification'),
        'isActivated' => $request->input('isActivated'),

      ]);

      $partner->UserDetail()->update([
        'nik' => $request->input('nik'),
        'npwp' => $request->input('npwp'),
        'foto_usaha' => $foto_usaha,
        'foto_selfie' => $foto_selfie,
      ]);

      return redirect()->route('partner.index')
                      ->with('success','Partner updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $partner)
    {
      $partner->delete();

      return redirect()->route('partner.index')
                      ->with('success','Partner deleted successfully');
    }
}
