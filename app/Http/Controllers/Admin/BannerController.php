<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Banner;
use App\NewsCategory;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use Storage;
use Illuminate\Support\Str;

class BannerController extends Controller
{
    public function index()
    {
        return view('admin.banner.index');
    }

    public function data()
    {
        $data = Banner::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('category', function($data){
            return $data->category->name;
          })
          ->addColumn('author', function($data){
            return $data->author->name;
          })
          ->make(true);
    }

    public function create(Request $request)
    {
        $category = NewsCategory::where('enabled', 1)->get();

        return view('admin.banner.create', compact('category'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'title' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }

            if ($request->hasFile('image')) {
              $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
              $request->file('image')->move(public_path('banners'), $imageName);
              $image = url("/banners/" .  $imageName);
            }

            Banner::create([
              'user_id' => auth()->guard()->user()->id,
              'category_id' => $data['category'],
              'title' => $data['title'],
              'slug' => Str::slug($data['title'], '-'),
              'description' => $data['description'],
              'is_published' => $data['is_published'],
              'image' => $image,
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data $e']);
        }

        return redirect()->route('banner')->with('success','Banner created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Banner::find($request->input('data'));
        $category = NewsCategory::where('enabled', 1)->get();

        return view('admin.banner.edit', compact('data', 'category'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'title' => 'required|max:255',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Banner::find($data['id']);
            $image = $update->image;

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }

            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('banners'), $imageName);
                $image = url("/banners/" .  $imageName);

                if(\File::exists(public_path('banners/' . $path['basename']))) \File::delete(public_path('banners/' . $path['basename']));
            }

            $update->category_id = $data['category'];
            $update->title = $data['title'];
            $update->slug = Str::slug($data['title'], '-');
            $update->description = $data['description'];
            $update->is_published = $data['is_published'];
            $update->image = $image;
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data']);
        }

        return redirect()->route('banner')->with('success','Banner updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Banner::find($request->input('id'));

        return response()->json(view('banner.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $banner = Banner::find($request->input('id'));
            $banner->delete();
        } catch (\Exception $e) {
            return redirect()->route('banner')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('banner')->with('success','Banner deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Banner::find($request->input('data'));

        return view('admin.banner.show', compact('data'));
    }
}
