<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Podcast;
use App\Models\PodcastCategory;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use Storage;
use Illuminate\Support\Str;

class PodcastController extends Controller
{
    public function index()
    {
        return view('admin.podcast.index');
    }

    public function data()
    {
        $data = Podcast::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('category', function($data){
            return $data->category->name;
          })
          ->addColumn('author', function($data){
            return $data->author->name;
          })
          ->make(true);
    }

    public function create(Request $request)
    {
        $category = PodcastCategory::where('enabled', 1)->get();

        return view('admin.podcast.create', compact('category'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'title' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }

            if ($request->hasFile('image')) {
              $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
              $request->file('image')->move(public_path('podcasts'), $imageName);
              $image = url("/podcasts/" .  $imageName);
            }

            
            if ($request->file('song')) {         
                $imageName = time().'.'.$request->file('song')->getClientOriginalExtension();
                $request->file('song')->move(public_path('podcasts'), $imageName);
                $song = url("/podcasts/" .  $imageName);
              }

            Podcast::create([
              'user_id' => auth()->guard()->user()->id,
              'category_id' => $data['category'],
              'title' => $data['title'],
              'slug' => Str::slug($data['title'], '-'),
              'description' => $data['description'],
              'is_published' => $data['is_published'],
              'image' => $image,
              'file'=>$song
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data'.$e]);
        }

        return redirect()->route('podcast')->with('success','Podcast created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Podcast::find($request->input('data'));
        $category = PodcastCategory::where('enabled', 1)->get();

        return view('admin.podcast.edit', compact('data', 'category'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'category' => 'required',
            'title' => 'required|max:255',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Podcast::find($data['id']);
            $image = $update->image;

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }

            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('podcasts'), $imageName);
                $image = url("/podcasts/" .  $imageName);

                if(\File::exists(public_path('podcasts/' . $path['basename']))) \File::delete(public_path('podcasts/' . $path['basename']));
            }

            $update->category_id = $data['category'];
            $update->title = $data['title'];
            $update->slug = Str::slug($data['title'], '-');
            $update->description = $data['description'];
            $update->is_published = $data['is_published'];
            $update->image = $image;
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data']);
        }

        return redirect()->route('podcast')->with('success','Podcast updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Podcast::find($request->input('id'));

        return response()->json(view('podcast.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $podcast = Podcast::find($request->input('id'));
            $podcast->delete();
        } catch (\Exception $e) {
            return redirect()->route('podcast')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('podcast')->with('success','Podcast deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Podcast::find($request->input('data'));

        return view('admin.podcast.show', compact('data'));
    }
}
