<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\News;
use App\NewsCategory;
use App\Services\SlugNews;
use App\User;
use DB;
use Illuminate\Support\Facades\Auth;


class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $news = News::orderBy('id','asc')->paginate(5);
  
        return view('admin.news.index',compact('news'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $category = NewsCategory::all();
        return view('admin.news.create',compact('category'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
       
       
        $data = $request->all();
        
    

        $request->validate([
            'title' => 'required',          
            'category_id' => 'required',
            'content' => 'required',
        ]);
        $slug = new SlugNews();
        $data['slug'] = $slug->createSlug($request->title);        
        if(isset($data['isPublished']))
        {
            $data['is_published'] = true;
        }
        else
        {
            $data['is_published'] = false;
        }
        $data['user_id'] = Auth::guard()->user()->id;
        $data['file'] = '';
        
        $data['published_at'] = \Carbon\Carbon::now();
        $imageName = "";
        // $img = explode(',',$data['image'])[1];
        // $extension = explode('/', mime_content_type($data['image']))[1];
        // $fileStream = fopen(public_path() . "/news_img/" .  $data['slug'] .".".$extension, "wb"); 
        if ($request->hasFile('image')) {
        $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('news_img'), $imageName);        
        }
        $data['banner_image'] = url("/news_img/" .  $imageName);
        $data['file'] = '';

        // $data['banner_image'] = url("/news_img/" .  $data['slug'] .".".$extension);
        // $data['file'] = public_path() ."/news_img/" .  $data['slug'] .".".$extension;

        // fwrite($fileStream, base64_decode($img)); 
        // fclose($fileStream); 
        
        //tag
        $data['file'];

        try{
            DB::beginTransaction();
        
            $post = News::create($data);      
            if($post)
            {        
                $tagNames = explode(',',$data['tags']);
                      
                $tagIds = [];
                foreach($tagNames as $tagName)
                {
                    //$post->tags()->create(['name'=>$tagName]);
                    //Or to take care of avoiding duplication of Tag
                    //you could substitute the above line as
                    $tag = \App\Tags::firstOrCreate(['tag'=>$tagName,'slug'=>'']);
                    if($tag)
                    {
                    $tagIds[] = $tag->id;
                    }
    
                }
                $post->tags()->sync($tagIds);
            }
       
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
       
        return redirect()->route('news.index')
                        ->with('success','news created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
        return view('admin.news.show',compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        //
        
        $tagx = $news->tags;
        $tags = array();
        
        foreach($tagx as $tag)
        {            
            array_push($tags,$tag->tag);
        }
        
        $news->tags = implode(',', $tags);

        //image
        // $b64image = "data:image/jpeg;base64,".base64_encode(file_get_contents($news->file));   
  
        //$news->banner_image =$news->file;

        $category = NewsCategory::all();

        return view('admin.news.edit',compact('news','category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        //
        $data = $request->all();
        
    

        $request->validate([
            'title' => 'required',          
            'category_id' => 'required',
            'content' => 'required',
        ]);
        $slug = new SlugNews();
        $data['slug'] = $slug->createSlug($request->title);        
        if(isset($data['isPublished']))
        {
            $data['is_published'] = true;
        }
        else
        {
            $data['is_published'] = false;
        }
        $data['user_id'] = Auth::guard()->user()->id;
        $data['file'] = '';
        
        $data['published_at'] = \Carbon\Carbon::now();
        
        
        // $img = explode(',',$data['image'])[1];
        // $extension = explode('/', mime_content_type($data['image']))[1];
        // $fileStream = fopen(public_path() . "/news_img/" .  $data['slug'] .".".$extension, "wb"); 
        if ($request->hasFile('image')) {
        $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
        $request->file('image')->move(public_path('news_img'), $imageName);        
        $data['banner_image'] = url("/news_img/" .  $imageName);
        $data['file'] = '';
        }
        
        

        // $data['banner_image'] = url("/news_img/" .  $data['slug'] .".".$extension);
        // $data['file'] = public_path() ."/news_img/" .  $data['slug'] .".".$extension;

        // fwrite($fileStream, base64_decode($img)); 
        // fclose($fileStream); 
        
        //tag
        $data['file'];

        try{
            DB::beginTransaction();
        
            $post2 = $news->update($data);   
            $post = $news; 
               
            if($post2)
            {        
                $tagNames = explode(',',$data['tags']);
                      
                $tagIds = [];
                foreach($tagNames as $tagName)
                {
                    //$post->tags()->create(['name'=>$tagName]);
                    //Or to take care of avoiding duplication of Tag
                    //you could substitute the above line as
                    $tag = \App\Tags::firstOrCreate(['tag'=>$tagName,'slug'=>'']);
                    if($tag)
                    {
                    $tagIds[] = $tag->id;
                    }
    
                }                
                $post->tags()->sync($tagIds);
            }
       
        
            DB::commit();
        }catch(\Exception $e){
            DB::rollback();
        }
       
        return redirect()->route('news.index')
                        ->with('success','news created successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        //
        $news->delete();
  
        return redirect()->route('news.index')
                        ->with('success','news deleted successfully');
    }
}
