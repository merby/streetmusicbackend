<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Cafe;
use App\Models\CafeImage;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Str;

class CafeImageController extends Controller
{
    public function index(Request $request)
    {
        $cafe = Cafe::find($request->input('cafe'));

        return view('admin.cafe-image.index', compact('cafe'));
    }

    public function data(Request $request)
    {
        $data = CafeImage::where('cafe_id', $request->input('cafe_id'))->get();

        return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);
    }

    // public function create(Request $request)
    // {
    //     return view('cafe-menu.create');
    // }

    public function store(Request $request)
    {
        $request->validate([
            'images' => 'required',
        ]);

        try {
            $data = $request->all();
            foreach ($data['images'] as $key => $image) {
                $imageName = time().$key.'.'.$image->getClientOriginalExtension();
                $image->move(public_path('cafe-image'), $imageName);
                $image_url = url("/cafe-image/" .  $imageName);

                CafeImage::create([
                  'cafe_id' => $data['cafe_id'],
                  'image' => $image_url,
                ]);
            }

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect(route('cafe.image'). '?cafe='.$data['cafe_id'])->with('success','Menu Image uploaded successfully.');
    }

    public function delete(Request $request)
    {
        $data = CafeImage::find($request->input('id'));

        return response()->json(view('admin.cafe-image.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $delete = CafeImage::find($request->input('id'));
            $cafe_id = $delete->cafe_id;
            $path = pathinfo($delete->image);
            if(\File::exists(public_path('cafe-image/' . $path['basename']))) \File::delete(public_path('cafe-image/' . $path['basename']));
            $delete->delete();
        } catch (\Exception $e) {
            return redirect()->route('cafe-menu')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect(route('cafe.image'). '?cafe='.$cafe_id)->with('success','Image cafe deleted successfully.');
    }
}
