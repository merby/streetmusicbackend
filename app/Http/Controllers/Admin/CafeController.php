<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Cafe;
use Yajra\Datatables\Datatables;
use DB;
use Illuminate\Support\Str;
use App\User;

class CafeController extends Controller
{
    public function index()
    {
        return view('admin.cafe.index');
    }

    public function data()
    {
        $data = Cafe::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->addColumn('owner', function($data){
            return $data->owner->name;
          })
          ->addColumn('menu', function($data){
              if (!$data->cafe_menu->isEmpty()) {
                return 1;
              }

              return 0;
          })
          ->addColumn('image', function($data){
              if (!$data->cafe_image->isEmpty()) {
                return 1;
              }

              return 0;
          })
          ->make(true);
    }

    public function create(Request $request)
    {
        $path = public_path() . "/city.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $cities = json_decode(file_get_contents($path), true); 

        // dd($cities);

        return view('admin.cafe.create',compact('cities'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'owner' => 'required',
            'name' => 'required',
            'phone' => 'required',
            'city' => 'required',
            'address' => 'required',
            'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();

            if (isset($data['isActive'])){
                $data['is_active'] = 1;
            }
            else{
                $data['is_active'] = 0;
            }

            if ($request->hasFile('logo')) {
              $imageName = time().'.'.$request->file('logo')->getClientOriginalExtension();
              $request->file('logo')->move(public_path('cafe-logo'), $imageName);
              $image = url("/cafe-logo/" .  $imageName);
            }

            Cafe::create([
              'user_id' => $data['owner'],
              'name' => $data['name'],
              'phone' => Str::slug($data['phone'], '-'),
              'address' => $data['address'],
              'city' => $data['city'],
              'description' => $data['description'],
              'is_active' => $data['is_active'],
              'coordinate' => $data['coordinate'],
              'logo' => $image,
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data'.$e]);
        }

        return redirect()->route('cafe')->with('success','Cafe created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Cafe::find($request->input('data'));
        $path = public_path() . "/city.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $cities = json_decode(file_get_contents($path), true); 
        return view('admin.cafe.edit', compact('data','cities'));
    }

    public function update(Request $request)
    {
        $request->validate([
          'owner' => 'required',
          'name' => 'required',
          'city' => 'required',

          'phone' => 'required',
          'address' => 'required',
          // 'logo' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Cafe::find($data['id']);
            $image = $update->logo;

            if (isset($data['isActive'])){
                $data['is_active'] = 1;
            }
            else{
                $data['is_active'] = 0;
            }

            if ($request->hasFile('logo')) {
                $path = pathinfo($update->logo);
                $imageName = time().'.'.$request->file('logo')->getClientOriginalExtension();
                $request->file('logo')->move(public_path('cafe-logo'), $imageName);
                $image = url("/cafe-logo/" .  $imageName);

                if(\File::exists(public_path('cafe-logo/' . $path['basename']))) \File::delete(public_path('cafe-logo/' . $path['basename']));
            }

            $update->user_id = $data['owner'];
            $update->name = $data['name'];
            $update->phone = $data['phone'];
            $update->city = $data['city'];
            $update->address = $data['address'];
            $update->description = $data['description'];
            $update->coordinate = $data['coordinate'];
            $update->is_active = $data['is_active'];
            $update->logo = $image;
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data']);
        }

        return redirect()->route('cafe')->with('success','Cafe updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Cafe::find($request->input('id'));

        return response()->json(view('admin.cafe.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $delete = Cafe::find($request->input('id'));
            $delete->delete();
        } catch (\Exception $e) {
            return redirect()->route('cafe')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('cafe')->with('success','Cafe deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Cafe::find($request->input('data'));

        return view('admin.cafe.show', compact('data'));
    }

    public function search_user(Request $request)
    {
        $term = trim($request->q);

        $user = User::whereHas('roles', function ($q) {
            $q->where('roles.name', '=', 'User');
          })->where('name', 'like', '%' . $term . '%')->limit(5)->get();
        $data = [];

        foreach ($user as $value) {
            $data[] = ['id' => $value->id, 'text' => ucwords(strtolower($value->name))];
        }

        return response()->json($data);
    }
}
