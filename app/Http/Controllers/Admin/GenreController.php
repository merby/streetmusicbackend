<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Genre;
use App\NewsCategory;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use Storage;
use Illuminate\Support\Str;

class GenreController extends Controller
{
    public function index()
    {
        return view('admin.genre.index');
    }

    public function data()
    {
        $data = Genre::all();

        return Datatables::of($data)
          ->addIndexColumn()
          ->make(true);
    }

    public function create(Request $request)
    {        
        return view('admin.genre.create');
    }

    public function store(Request $request)
    {
        $request->validate([            
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'icon_name' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();

            if ($request->hasFile('image')) {
              $imageName = $data['icon_name'].'.'.$request->file('image')->getClientOriginalExtension();
              $request->file('image')->move(public_path('genres'), $imageName);
              $image = url("/genres/" .   $imageName);
            }

            //dd($data);

            Genre::create([                            
              'name' => $data['name'],
              'description' => $data['description'],
              'icon_name' => $data['icon_name'],              
              'image' => $image,
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data'. $e]);
        }

        return redirect()->route('genre')->with('success','Genre created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Genre::find($request->input('data'));

        return view('admin.genre.edit', compact('data'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'name' => 'required|max:255',
            'description' => 'required|max:255',
            'icon_name' => 'required|max:255'
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Genre::find($data['id']);
            $image = $update->image;


            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = $data['icon_name'].'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('banners'), $imageName);
                $image = url("/banners/" .  $imageName);

                if(\File::exists(public_path('banners/' . $path['basename']))) \File::delete(public_path('banners/' . $path['basename']));
            }            
            $update->name = $data['name'];            
            $update->description = $data['description'];
            $update->icon_name = $data['icon_name'];            
            $update->image = $image;
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data'.$e]);
        }

        return redirect()->route('genre')->with('success','Genre updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Genre::find($request->input('id'));

        return response()->json(view('admin.genre.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $banner = Genre::find($request->input('id'));
            $banner->delete();
        } catch (\Exception $e) {
            return redirect()->route('genre')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('genre')->with('success','Genre deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Genre::find($request->input('data'));

        return view('admin.genre.show', compact('data'));
    }
}
