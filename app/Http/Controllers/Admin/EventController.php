<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Event;
use App\NewsCategory;
use Yajra\Datatables\Datatables;
use DB;
use Image;
use Storage;
use Illuminate\Support\Str;

class EventController extends Controller
{
    public function index()
    {
        return view('admin.event.index');
    }

    public function data()
    {
        $data = Event::all();

        return Datatables::of($data)
          ->addIndexColumn()
        
          ->make(true);
    }

    public function create(Request $request)
    {
        $category = NewsCategory::where('enabled', 1)->get();
        $path = public_path() . "/city.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $cities = json_decode(file_get_contents($path), true); 
        return view('admin.event.create', compact('category','cities'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'name' => 'required|max:255',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
            'start_date' => 'required',
            'end_date' => 'required',
        ]);

        try {
            $data = $request->all();

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }
            if (isset($data['ticket_distribution'])){
                $data['ticket_distribution'] = 1;
            }
            else{
                $data['ticket_distribution'] = 0;
            }
            if (isset($data['open_band'])){
                $data['open_band'] = 1;
            }
            else{
                $data['open_band'] = 0;
            }

            if ($request->hasFile('image')) {
              $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
              $request->file('image')->move(public_path('banners'), $imageName);
              $image = url("/banners/" .  $imageName);
            }

            Event::create([
              'type' => $data['type'],
              'name' => $data['name'],
              'slug' => Str::slug($data['name'], '-'),
              'description' => $data['description'],
              'is_published' => $data['is_published'],
              'ticket_distribution' => $data['ticket_distribution'],
              'open_band' => $data['open_band'],
              'location' => $data['location'],              
              'coordinate' => $data['coordinate'],
              'city' => $data['city'],
              'photo' => $image,
              'start_time'=>"00:00:00",
              'end_time'=>"00:00:00",
              'start_date' => $data['start_date'],
              'end_date' => $data['end_date'],
            ]);

        } catch (\Exception $e) {
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data'.$e]);
        }

        return redirect()->route('event')->with('success','event created successfully.');
    }

    public function edit(Request $request)
    {
        $data = Event::find($request->input('data'));
        //$category = NewsCategory::where('enabled', 1)->get();
        $path = public_path() . "/city.json"; // ie: /var/www/laravel/app/storage/json/filename.json

        $cities = json_decode(file_get_contents($path), true); 
        return view('admin.event.edit', compact('data','cities'));
    }

    public function update(Request $request)
    {
        $request->validate([
            'type' => 'required',
            'name' => 'required|max:255',
            'start_date' => 'required',
            'end_date' => 'required',
            // 'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg',
        ]);

        try {
            $data = $request->all();
            $update = Event::find($data['id']);
            $image = $update->photo;

         

            if (isset($data['isPublished'])){
                $data['is_published'] = 1;
            }
            else{
                $data['is_published'] = 0;
            }
            if (isset($data['ticket_distribution'])){
                $data['ticket_distribution'] = 1;
            }
            else{
                $data['ticket_distribution'] = 0;
            }
            if (isset($data['open_band'])){
                $data['open_band'] = 1;
            }
            else{
                $data['open_band'] = 0;
            }

            if ($request->hasFile('image')) {
                $path = pathinfo($update->image);
                $imageName = time().'.'.$request->file('image')->getClientOriginalExtension();
                $request->file('image')->move(public_path('banners'), $imageName);
                $image = url("/banners/" .  $imageName);

                if(\File::exists(public_path('banners/' . $path['basename']))) \File::delete(public_path('banners/' . $path['basename']));
            }


            $update->type = $data['type'];
            $update->name = $data['name'];
            $update->slug = Str::slug($data['name'], '-');
            $update->description = $data['description'];
            $update->is_published = $data['is_published'];
            $update->ticket_distribution = $data['ticket_distribution'];
            $update->city = $data['city'];
            $update->open_band = $data['open_band'];
            $update->location =$data['location'];
            $update->coordinate =$data['coordinate'];
            $update->photo = $image;
            $update->start_date = $data['start_date'];
            $update->end_date = $data['end_date'];
            $update->save();
        } catch (\Exception $e) {
          return redirect()->back()->withInput($request->input())
            ->withErrors(['Something wrong, unable to update data'.$e]);
        }

        return redirect()->route('event')->with('success','event updated successfully.');
    }

    public function delete(Request $request)
    {
        $data = Event::find($request->input('id'));

        return response()->json(view('admin.event.delete', compact('data'))->render());
    }

    public function destroy(Request $request)
    {
        try {
            $banner = Event::find($request->input('id'));
            $banner->delete();
        } catch (\Exception $e) {
            return redirect()->route('event')->withErrors(['Something wrong, unable to delete data']);
        }

        return redirect()->route('event')->with('success','event deleted successfully.');
    }

    public function show(Request $request)
    {
        $data = Event::find($request->input('data'));

        return view('admin.event.show', compact('data'));
    }
}
