<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\PodcastCategory;
use App\Services\Slug;
class PodcastCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $NewsCategories = PodcastCategory::orderBy('id','asc')->paginate(5);
  
        return view('admin.podcast-categories.index',compact('NewsCategories'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.podcast-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        
        $data = $request->all();
        $request->validate([
            'name' => 'required',          
        ]);
        $slug = new Slug();
        $data['slug'] = $slug->createSlug($request->name);
        $data['enabled'] = true;
        

        PodcastCategory::create($data);
   
        return redirect()->route('podcast-categories.index')
                        ->with('success','category created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PodcastCategory $podcastCategory)
    {
        //
        
        return view('admin.podcast-categories.show',compact('podcastCategory'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PodcastCategory $podcastCategory)
    {
       
        //
        return view('admin.podcast-categories.edit',compact('podcastCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, PodcastCategory $podcastCategory)
    {
        //
         //
         $data = $request->all();
         $request->validate([
            'name' => 'required',           
        ]);
        $slug = new Slug();
        $data['slug'] = $slug->createSlug($request->name);
        $data['enabled'] = true;
        

        $podcastCategory->update($data);
   
  
        return redirect()->route('podcast-categories.index')
                        ->with('success','category updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PodcastCategory $podcastCategory)
    {
        //
        $podcastCategory->delete();
  
        return redirect()->route('podcast-categories.index')
                        ->with('success','category deleted successfully');
    }
}
