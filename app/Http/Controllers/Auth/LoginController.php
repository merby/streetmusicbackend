<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Input;
use Auth;
use Redirect;
use Illuminate\Support\Facades\Validator;
use IlluminateSupportFacadesValidator;
use IlluminateFoundationBusDispatchesJobs;
use IlluminateRoutingController as BaseController;
use IlluminateFoundationValidationValidatesRequests;
use IlluminateFoundationAuthAccessAuthorizesRequests;
use IlluminateFoundationAuthAccessAuthorizesResources;
use IlluminateHtmlHtmlServiceProvider;;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login / registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'logout']);
    }

    function login()
    {
    // Creating Rules for Email and Password
        $rules = array(
            'email' => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:6'
      // password has to be greater than 3 characters and can only be alphanumeric and);
      // checking all field
        );
      $validator = Validator::make(Input::all() , $rules);
      // if the validator fails, redirect back to the form
      if ($validator->fails())
        {
            dd("wewe");
        return Redirect::to('login')->withErrors($validator) // send back all errors to the login form
        ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        }
        else
        {
        // create our user data for the authentication
        $userdata = array(
          'email' => Input::get('email') ,
          'password' => Input::get('password'),
          
        );
        // attempt to do the login
        // dd(Auth::attempt($userdata));
        if (Auth::attempt($userdata))
          {

          // validation successful
          // do whatever you want on success
            //dd(Auth::user());
            if(Auth::user()->hasRole('admin'))
            {
                return Redirect::to('/home');
            }


            if(Auth::user()->hasRole('user'))
            {
              return Redirect::to('/business');
            }


          }
          else
          {
            // validation not successful, send back to form
            return Redirect::to('login');
          }
        }
      }

}
