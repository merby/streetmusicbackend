<?php

namespace App\Http\Controllers\Business;

use Illuminate\Http\Request;
use App\Models\Business\BusinessCategories;
use App\Models\Business\Business;
use App\Models\Business\BusinessAggregator;
use App\Models\Business\BusinessAggregatorPackage;
use App\Models\Business\BusinessAggregatorConfig;
use App\Models\Business\BusinessEvent;
use App\Models\Business\BusinessAggregatorOrders;
use App\Models\Business\BusinessAggregatorOrdersDetails;
use App\Models\Business\BusinessAggregatorOrderDetailTrack;
use App\Models\Business\GenreMusik;
use App\Models\Business\Bahasa;
use Illuminate\Support\Facades\Storage;
use DB;
use URL;

class BusinessController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $businessBanner = Business::orderBy('id', 'desc')->take(3)->get();
        $business = Business::all();

        return view('business.business.index', compact('business', 'businessBanner'));
    }

    public function create()
    {
        $category = BusinessCategories::all();

        return view('business.business.create', compact('category'));
    }

    public function createPackage(int $id)
    {
        $business = Business::where('id', $id)->first();

        return view('business.business.create-package', compact('business'));
    }

    public function updateNotification(Request $request)
    {
        try {
            DB::connection('mysql')->beginTransaction();
            $data = $request->all();

            if (BusinessAggregatorConfig::where('business_id', $data['bId'])->first() === null) {
                BusinessAggregatorConfig::create([
                  'web_notification' => $data['web'][0],
                  'email_notification' => $data['email'][0],
                  'business_id' => $data['bId'],
                ]);
            }

            if (BusinessAggregatorConfig::where('business_id', $data['bId'])->first() !== null) {
                BusinessAggregatorConfig::where('business_id', $data['bId'])->update([
                  'web_notification' => $data['web'][0],
                  'email_notification' => $data['email'][0],
                ]);
            }

            DB::connection('mysql')->commit();
        } catch (\Exception $e) {
            dd($e);
            DB::connection('mysql')->rollback();
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect(URL::to('/') . '/business/configuration/' . $data['bId'] . '?tag=2')->with('success','Notifikasi Berhasil di Update.');
    }

    public function editPackage(int $id)
    {
        $package = BusinessAggregatorPackage::where('id', $id)->first();
        $business = Business::where('id', $package->business_id)->first();

        return view('business.business.edit-package', compact('business', 'package'));
    }

    public function storePackage(Request $request)
    {
        try {
            DB::connection('mysql')->beginTransaction();
            $data = $request->all();

            BusinessAggregatorPackage::create([
              'package_name' => $data['name'],
              'business_id' => $data['bId'],
              'price' => $data['price'],
              'description' => $data['description'],
            ]);

            DB::connection('mysql')->commit();
        } catch (\Exception $e) {
            DB::connection('mysql')->rollback();
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect(URL::to('/') . '/business/configuration/' . $data['bId'] . '?tag=1')->with('success','Paket Baru Sudah di Tambah.');
    }

    public function updatePackage(Request $request)
    {
        try {
            DB::connection('mysql')->beginTransaction();
            $data = $request->all();

            BusinessAggregatorPackage::where('id', $data['packageId'])->update([
              'package_name' => $data['name'],
              'price' => $data['price'],
              'description' => $data['description'],
            ]);

            $package = BusinessAggregatorPackage::where('id', $data['packageId'])->first();
            $id = Business::where('id', $package->business_id)->first()->id;

            DB::connection('mysql')->commit();
        } catch (\Exception $e) {
            DB::connection('mysql')->rollback();
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect(URL::to('/') . '/business/configuration/' . $id . '?tag=1')->with('success','Paket Baru Sudah di Tambah.');
    }

    public function dashboardBusiness(int $id)
    {
        $business = Business::where('id', $id)->first();
        $id = BusinessAggregator::where('business_id', $id)->first()->id;
        $order = BusinessAggregatorOrders::where('business_aggreator_id', $id)->get();

        return view('business.business.dashboard-business', compact('business', 'order'));
    }

    public function reportBusiness(int $id)
    {
        $business = Business::where('id', $id)->first();
        $id = BusinessAggregator::where('business_id', $id)->first()->id;
        $order = BusinessAggregatorOrders::where('business_aggreator_id', $id)->get();

        return view('business.business.report-business', compact('business', 'order'));
    }

    public function detail(int $id)
    {
        $business = Business::where('id', $id)->first();
        $tag = request()->tag;

        return view('business.business.detail', compact('business', 'tag'));
    }

    public function configuration(int $id)
    {
        $business = Business::where('id', $id)->first();
        $tag = request()->tag;
        $package = BusinessAggregatorPackage::where('business_id', $id)->get();
        $notif = BusinessAggregatorConfig::where('business_id', $id)->first();

        return view('business.business.configuration', compact('business', 'tag', 'package', 'notif'));
    }

    public function detailOrder(int $id, int $bId)
    {
        $business = BusinessAggregator::where('business_id', $bId)->first();
        $detail = BusinessAggregatorOrdersDetails::where('orders_id', $id)->first();
        $genre = GenreMusik::all();

        return view('business.business.detail-order', compact('business', 'detail', 'genre'));
    }

    public function detailMusic(int $id, int $bId)
    {
        $business = BusinessAggregator::where('business_id', $bId)->first();
        $detail = BusinessAggregatorOrderDetailTrack::where('id', $id)->first();
        $genre = GenreMusik::all();
        $bahasa = Bahasa::all();

        return view('business.business.detail-track', compact('business', 'detail', 'genre', 'bahasa'));
    }

    public function status(int $id, int $bId)
    {
        $business = BusinessAggregator::where('business_id', $bId)->first();
        $order = BusinessAggregatorOrders::where('id', $id)->first();
        $detail = BusinessAggregatorOrdersDetails::where('orders_id', $id)->first();

        return view('business.business.detail-status', compact('business', 'detail', 'order'));
    }

    public function edit(int $id)
    {
        $business = Business::where('id', $id)->first();

        return view('business.business.edit', compact('business'));
    }

    public function delete(int $id)
    {
        BusinessAggregator::where('business_id', $id)->delete();
        Business::where('id', $id)->delete();

        return redirect()->route('business.list')->with('success','Bussiness was deleted.');
    }

    public function updateStatus(Request $request)
    {
        $order = BusinessAggregatorOrders::where('id', $request->input('id'))->update([
          'status' => $request->input('status'),
          'keterangan' => $request->input('keterangan'),
        ]);

        return redirect(URL::to('/') . '/business/detail/' . $request->input('bId') . '?tag=' . $request->input('status'))->with('success','Status sudah di update.');
    }

    public function store(Request $request)
    {
          if (is_null($request->file('image'))) {
              return redirect()->route('business.create')
                ->with('error', 'Gagal, Logo Wajib di Upload');
          }

          try {
            DB::connection('mysql')->beginTransaction();
            $data = $request->all();
            $imageName = time().'.'.$request->image->extension();
            $request->image->move(public_path('images'), $imageName);

            $business = Business::create([
              'name' => $data['name'],
              'business_category_id' => $data['business_category_id'],
              'contact' => $data['contact'],
              'address' => $data['address'],
              'description' => $data['description'],
              'image' => 'images/' . $imageName,
            ]);

            if ((int)$data['business_category_id'] === 1) {
                BusinessAggregator::create([
                  'business_id' => (int)$business->id,
                  'name' => $data['name'],
                  'description' => $data['description'],
                ]);
            }

            if ((int)$data['business_category_id'] === 2) {
                BusinessEvent::create([
                  'business_id' => (int)$business->id,
                  'name' => $data['name'],
                  'description' => $data['description'],
                ]);
            }

            DB::connection('mysql')->commit();
        } catch (\Exception $e) {
            DB::connection('mysql')->rollback();
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect()->route('business.list')->with('success','Bussiness created successfully.');
    }

    public function update(Request $request)
    {
        try {
            DB::connection('mysql')->beginTransaction();
            $data = $request->all();

            if (!is_null($request->file('image'))) {
                $imageName = time().'.'.$request->image->extension();
                $request->image->move(public_path('images'), $imageName);
                $image = 'images/' . $imageName;
            }

            if (is_null($request->file('image'))) {
                $image = $data['oldImage'];
            }

            $business = Business::find($data['id']);
            $business->update([
              'name' => $data['name'],
              'description' => $data['description'],
              'image' => $image,
              'contact' => $data['contact'],
              'address' => $data['address'],
            ]);

            if ($business->business_category_id === 1) {
                BusinessAggregator::where('business_id', $data['id'])->update([
                  'name' => $data['name'],
                  'description' => $data['description'],
                ]);
            }

            if ($business->business_category_id === 2) {
                BusinessEvent::where('business_id', $data['id'])->update([
                  'name' => $data['name'],
                  'description' => $data['description'],
                ]);
            }

            DB::connection('mysql')->commit();
        } catch (Exception $e) {
            DB::connection('mysql')->rollback();
            return redirect()->back()->withInput($request->input())
              ->withErrors(['Something wrong, unable to save data']);
        }

        return redirect()->route('business.list')->with('success','Bussiness created successfully.');
    }
}
