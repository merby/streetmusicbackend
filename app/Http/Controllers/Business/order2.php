@extends('layouts.app')

@section('content')
@if (session()->has('login.email'))
<div class="page-header">
	<h1>Form Pemesanan</h1>
</div><!-- /.page-header -->
<div id="user-profile-2" class="user-profile">
					<div class="tabbable">
						<ul class="nav nav-tabs padding-18">
                            <!--
							<li class="active">
								<a data-toggle="tab" href="#board" id="dash">
									<i class="red ace-icon fa fa-tachometer bigger-120"></i>
									Dashboard
								</a>
							</li>
                            -->
							<li class="active">
								<a data-toggle="tab" href="#newborn" id="nb">
									NB
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#sml" id="smla">
									SMLA
								</a>
							</li>
							<li>
								<a data-toggle="tab" href="#123x" id="123">
                                    123X
								</a>
							</li>							
                           
                            <li>                      
                                <a href="{{ url('/list') }}" class="btn btn-primary pull-right" id="tran">View Order</i></a>
							
							</li>
                            
						</ul>
            <div class="tab-content no-border">		
                <div id="newborn" class="tab-pane active">
                    <div class="table-container" style="width:100%">
                        <div class="table-container-header" >
                            <table class="table table-striped table-hover table-condensed table-bordered">
                                <colgroup>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width:auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>CODE</th>
                                        <th>DESC</th>
                                        <th>SIZE</th>
                                        <!-- wrn,sfp,slf -->
                                        <!-- <th align="center">WRN</th> -->
                                        <th align="center">PTH</th>
                                        <th align="center">NOP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">MTF</th>
                                        <!-- <th align="center">SFP</th> -->
                                        <!-- <th align="center">SLF</th> -->
                                        <th align="center">SNP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">ABU</th>
                                        <th align="center">KKI</th>
                                        <th align="center">PNK</th>
                                        <th align="center">BRU</th>
                                        <th align="center">HJU</th>
                                        <th align="center">KNG</th>
                                        <th align="center">PRICE</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="table-container-body">
                            <table class="table table-striped table-hover table-condensed table-bordered"  id="tblord">
                                <colgroup>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 90px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width:50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>                                    
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                </colgroup>
                                <tbody>
                                    @php
                                            $last="";
                                            @endphp

                                            @foreach($product as $prod)   
                                            @php
                                                if($prod->SIZE=='NB')
                                                {
                                            @endphp
                                    <tr>
                                        <td>{{$prod->CODE}}</td>
                                        <td>{{$prod->NAME}}</td>
                                        <td>{{$prod->SIZE}}</td>					
                                        @php
                                            $prodmast = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['PTH', 'NOP'])
                                        ->get();
                                            $prc1 = 0;
                                            foreach($prodmast as $prd):                                                
                                                $price =  $prd->PRICE;
                                                $color =  $prd->COLOR;                                                
                                                if($color=='WRN')
                                                {
                                                    if($price!=0)
                                                    {
                                                        $prc1 = $price;
                                                    }
                                                }
                                                if($prc1==0)
                                                {
                                                if($color=='PTH')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }
                                                }
                                                 if($prc1==0)
                                                {
                                                    if($color=='NOP')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }  
                                                }
                                            endforeach;

                                        $prodmast2 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['MTF', 'SNP'])
                                        ->get();
                                            $prc2 = 0;
                                            foreach($prodmast2 as $prd2):                                                
                                                $price2 =  $prd2->PRICE;
                                                $color2 =  $prd2->COLOR;
                                                if($color2=='MTF')
                                                {
                                                    if($price2!=0)
                                                    {
                                                        $prc2 = $price2;
                                                    }
                                                }
                                                if($prc2==0)
                                                {
                                                        if($color2=='SFP')
                                                        {
                                                            if($price2!=0)
                                                            {
                                                                $prc2 = $price2;
                                                            }
                                                        }
                                                }
                                                if($prc2==0)
                                                {
                                                    if($color2=='SLF')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }

                                                if($prc2==0)
                                                {
                                                    if($color2=='SNP')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }

                                            endforeach; 
                                            
                                            $prodmast3 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['ABU','KKI','PNK','BRU','HJU','KNG'])
                                        ->get();
                                            $prc3 = 0;
                                            foreach($prodmast3 as $prd3):                                                
                                                $price3 =  $prd3->PRICE;
                                                $color3 =  $prd3->COLOR;
                                                if($color3=='ABU')
                                                {
                                                    if($price3!=0)
                                                    {
                                                        $prc3 = $price3;
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KKI')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='PNK')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='BRU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='HJU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KNG')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }


                                            endforeach;  
                                        @endphp
                                            <!-- <td align="center">
                                                <input class="warna" name="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','WRN')">
                                            </td> -->
                                            <td align="center">
                                                <input class="putih" style="text-align:center;border:0px;"placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PTH'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PTH')">
                                                </td>
                                                <td align="center">
                                                    <input class="nops" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-NOP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','NOP')">
                                                    </td>
                                                    <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-1'}}">
                                                            @php
                                                                    if($prc1==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc1/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                            <td align="center">
                                                                <input class="motif" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-MTF'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','MTF')">
                                                            </td>
                                                            
                                                            <td align="center">
                                                                <input class="salur2" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-SNP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','SNP')">
                                                            </td>
                                                            <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                            @php
                                                                    if($prc2==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc2/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                            </td>
                                                            <td align="center">
                                                                <input class="abu" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-ABU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','ABU')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="khaki" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KKI'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KKI')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="pink" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PNK'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PNK')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="biru" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-BRU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','BRU')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="hijau" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-HJU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','HJU')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="kuning" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KNG'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KNG')">
                                                            </td>
                                                            <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                            @php
                                                                    if($prc3==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc3/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                            </tr>   
                                              @php
                                                       }

                                                    @endphp
                                                    @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
            

                <div id="sml" class="tab-pane">
                    <div class="table-container" style="width:100%">
                        <div class="table-container-header" >
                            <table class="table table-striped table-hover table-condensed table-bordered">
                                <colgroup>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width:auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>CODE</th>
                                        <th>DESC</th>
                                        <th>SIZE</th>
                                        <!-- wrn,sfp,slf -->
                                        <!-- <th align="center">WRN</th> -->
                                        <th align="center">PTH</th>
                                        <th align="center">NOP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">MTF</th>
                                        <!-- <th align="center">SFP</th> -->
                                        <!-- <th align="center">SLF</th> -->
                                        <th align="center">SNP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">ABU</th>
                                        <th align="center">KKI</th>
                                        <th align="center">PNK</th>
                                        <th align="center">BRU</th>
                                        <th align="center">HJU</th>
                                        <th align="center">KNG</th>
                                        <th align="center">PRICE</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="table-container-body">
                            <table class="table table-striped table-hover table-condensed table-bordered"  id="tblord">
                                <colgroup>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 90px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width:50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>                                    
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                </colgroup>
                                <tbody>
                                    @php
                                                $last="";
                                            @endphp

                                            @foreach($product as $prod)   
                                            @php
                                                if($prod->SIZE=='S' || $prod->SIZE=='M' || $prod->SIZE=='L' || $prod->SIZE=='A')
                                                {
                                            @endphp
                                    <tr>
                                        <td>  @php
                                            if($prod->CODE==$last)
                                            {
                                                echo "";
                                            }
                                            else
                                            {
                                                echo $prod->CODE;
                                            }

                                        @endphp</td>
                                        <td>{{$prod->NAME}}</td>
                                        <td>{{$prod->SIZE}}</td>					
                                         @php
                                            $prodmast = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['PTH', 'NOP'])
                                        ->get();
                                            $prc1 = 0;
                                            foreach($prodmast as $prd):                                                
                                                $price =  $prd->PRICE;
                                                $color =  $prd->COLOR;                                                
                                                if($color=='WRN')
                                                {
                                                    if($price!=0)
                                                    {
                                                        $prc1 = $price;
                                                    }
                                                }
                                                if($prc1==0)
                                                {
                                                if($color=='PTH')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }
                                                }
                                                 if($prc1==0)
                                                {
                                                    if($color=='NOP')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }  
                                                }
                                            endforeach;

                                             $prodmast2 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['MTF', 'SNP'])
                                        ->get();
                                            $prc2 = 0;
                                            foreach($prodmast2 as $prd2):                                                
                                                $price2 =  $prd2->PRICE;
                                                $color2 =  $prd2->COLOR;
                                                if($color2=='MTF')
                                                {
                                                    if($price2!=0)
                                                    {
                                                        $prc2 = $price2;
                                                    }
                                                }
                                                if($prc2==0)
                                                {
                                                        if($color2=='SFP')
                                                        {
                                                            if($price2!=0)
                                                            {
                                                                $prc2 = $price2;
                                                            }
                                                        }
                                                }
                                                if($prc2==0)
                                                {
                                                    if($color2=='SLF')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }
                                                if($prc2==0)
                                                {
                                                    if($color2=='SNP')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }
                                                

                                            endforeach;    
                                            
                                             $prodmast3 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['ABU','KKI','PNK','BRU','HJU','KNG'])
                                        ->get();
                                            $prc3 = 0;
                                            foreach($prodmast3 as $prd3):                                                
                                                $price3 =  $prd3->PRICE;
                                                $color3 =  $prd3->COLOR;
                                                if($color3=='ABU')
                                                {
                                                    if($price3!=0)
                                                    {
                                                        $prc3 = $price3;
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KKI')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='PNK')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='BRU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='HJU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KNG')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                               

                                            endforeach;  
                                        @endphp
                                        <!-- <td align="center">
                                            <input class="warna" name="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','WRN')">
                                            </td> -->
                                            <td align="center">
                                                <input class="putih" style="text-align:center;border:0px;"placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PTH'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PTH')">
                                                </td>
                                                <td align="center">
                                                    <input class="nops" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-NOP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','NOP')">
                                                    </td>
                                                    <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-1'}}">
                                                                 @php
                                                                    if($prc1==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc1/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                        <td align="center">
                                                            <input class="motif" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-MTF'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','MTF')">
                                                        </td>
                                                   
                                                        <td align="center">
                                                            <input class="salur2" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-SNP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','SNP')">
                                                        </td>
                                                       <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                            @php
                                                                    if($prc2==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc2/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                        
                                                                <td align="center">
                                                                    <input class="abu" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-ABU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','ABU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="khaki" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KKI'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KKI')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="pink" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PNK'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PNK')">
                                                                </td>
                                                                <td align="center">
                                                                <input class="biru" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-BRU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','BRU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="hijau" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-HJU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','HJU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="kuning" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KNG'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KNG')">
                                                                </td>
                                                                <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                            @php
                                                                    if($prc3==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc3/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                            </tr>   
                                              @php
                                                       }

                                                    @endphp
                                    
                                                     @php
                                                        $last=$prod->CODE
                                                    @endphp
                                                    @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>




<div id="123x" class="tab-pane">
                    <div class="table-container" style="width:100%">
                        <div class="table-container-header" >
                            <table class="table table-striped table-hover table-condensed table-bordered">
                                <colgroup>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width:auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                    <col style="width: auto;"></col>
                                </colgroup>
                                <thead>
                                    <tr>
                                        <th>CODE</th>
                                        <th>DESC</th>
                                        <th>SIZE</th>
                                        <!-- wrn,sfp,slf -->
                                        <!-- <th align="center">WRN</th> -->
                                        <th align="center">PTH</th>
                                        <th align="center">NOP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">MTF</th>
                                        <!-- <th align="center">SFP</th> -->
                                        <!-- <th align="center">SLF</th> -->
                                        <th align="center">SNP</th>
                                        <th align="center">PRICE</th>
                                        <th align="center">ABU</th>
                                        <th align="center">KKI</th>
                                        <th align="center">PNK</th>
                                        <th align="center">BRU</th>
                                        <th align="center">HJU</th>
                                        <th align="center">KNG</th>
                                        <th align="center">PRICE</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                        <div class="table-container-body">
                            <table class="table table-striped table-hover table-condensed table-bordered"  id="tblord">
                                <colgroup>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 90px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width:50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>                                    
                                    <col style="width: 50px;"></col>
                                    <col style="width: 50px;"></col>
                                </colgroup>
                                <tbody>
                                    @php
                                                $last="";
                                            @endphp

                                            @foreach($product as $prod)   
                                            @php
                                                if($prod->SIZE=='1' || $prod->SIZE=='2' || $prod->SIZE=='3' || $prod->SIZE=='X')
                                                {
                                            @endphp
                                    <tr>
                                        <td>  @php
                                            if($prod->CODE==$last)
                                            {
                                                echo "";
                                            }
                                            else
                                            {
                                                echo $prod->CODE;
                                            }

                                        @endphp</td>
                                        <td>{{$prod->NAME}}</td>
                                        <td>{{$prod->SIZE}}</td>					
                                         @php
                                            $prodmast = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['PTH', 'NOP'])
                                        ->get();
                                            $prc1 = 0;
                                            foreach($prodmast as $prd):                                                
                                                $price =  $prd->PRICE;
                                                $color =  $prd->COLOR;                                                
                                                if($color=='WRN')
                                                {
                                                    if($price!=0)
                                                    {
                                                        $prc1 = $price;
                                                    }
                                                }
                                                if($prc1==0)
                                                {
                                                if($color=='PTH')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }
                                                }
                                                 if($prc1==0)
                                                {
                                                    if($color=='NOP')
                                                    {
                                                        if($price!=0)
                                                        {
                                                            $prc1 = $price;
                                                        }
                                                    }  
                                                }
                                            endforeach;

                                             $prodmast2 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['MTF', 'SNP'])
                                        ->get();
                                            $prc2 = 0;
                                            foreach($prodmast2 as $prd2):                                                
                                                $price2 =  $prd2->PRICE;
                                                $color2 =  $prd2->COLOR;
                                                if($color2=='MTF')
                                                {
                                                    if($price2!=0)
                                                    {
                                                        $prc2 = $price2;
                                                    }
                                                }
                                                if($prc2==0)
                                                {
                                                        if($color2=='SFP')
                                                        {
                                                            if($price2!=0)
                                                            {
                                                                $prc2 = $price2;
                                                            }
                                                        }
                                                }
                                                if($prc2==0)
                                                {
                                                    if($color2=='SLF')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }
                                                if($prc2==0)
                                                {
                                                    if($color2=='SNP')
                                                    {
                                                        if($price2!=0)
                                                        {
                                                            $prc2 = $price2;
                                                        }
                                                    }
                                                }

                                            endforeach;     
                                             $prodmast3 = DB::table('posprodmast')->WHERE('CODE',$prod->CODE)->WHERE('SIZE',$prod->SIZE)
                                         ->whereIn('COLOR', ['ABU','KKI','PNK','BRU','HJU','KNG'])
                                        ->get();
                                            $prc3 = 0;
                                            foreach($prodmast3 as $prd3):                                                
                                                $price3 =  $prd3->PRICE;
                                                $color3 =  $prd3->COLOR;
                                                if($color3=='ABU')
                                                {
                                                    if($price3!=0)
                                                    {
                                                        $prc3 = $price3;
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KKI')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='PNK')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='BRU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='HJU')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }
                                                if($prc3==0)
                                                {
                                                    if($color3=='KNG')
                                                    {
                                                        if($price3!=0)
                                                        {
                                                            $prc3 = $price3;
                                                        }
                                                    }
                                                }

                                               

                                            endforeach;  
                                        @endphp
                                        <!-- <td align="center">
                                            <input class="warna" name="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-WRN'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','WRN')">
                                            </td> -->
                                                <td align="center">
                                                    <input class="putih" style="text-align:center;border:0px;"placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PTH'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PTH')">
                                                </td>
                                                <td align="center">
                                                    <input class="nops" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-NOP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','NOP')">
                                                </td>
                                                 <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-1'}}">
                                                                 @php
                                                                    if($prc1==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc1/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                    <td align="center">
                                                        <input class="motif" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-MTF'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','MTF')">
                                                        </td>
                                                            <!-- <td align="center">
                                                                <input class="salur" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-SFP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','SFP')">
                                                            </td>
                                                            <td align="center">
                                                                <input class="salur2" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-SLF'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','SLF')">
                                                            </td> -->
                                                            <td align="center">
                                                                <input class="salur2" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-SNP'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','SNP')">
                                                            </td>
                                                                <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                             @php
                                                                    if($prc2==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc2/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                                <td align="center">
                                                                    <input class="abu" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-ABU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','ABU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="khaki" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KKI'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KKI')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="pink" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-PNK'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','PNK')">                                                                    
                                                                </td>
                                                                <td align="center">
                                                                    <input class="biru" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-BRU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','BRU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="hijau" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-HJU'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','HJU')">
                                                                </td>
                                                                <td align="center">
                                                                    <input class="kuning" style="text-align:center;border:0px;" placeholder="0" size="3" id="{{$prod->CODE.'-'.$prod->SIZE.'-KNG'}}" onChange="process('{{$prod->CODE}}','{{$prod->SIZE}}','KNG')">
                                                                </td>
                                                                <td style="text-align:right" id="{{'PRC'.$prod->CODE.$prod->SIZE.'-2'}}">

                                                             @php
                                                                    if($prc3==0)
                                                                    {
                                                                        echo "Tidak tersedia";
                                                                    }
                                                                    else
                                                                    {
                                                                        echo ($prc3/1000)." rb"; 

                                                                    }
                                                                    
                                                            @endphp
                                                        </td>
                                                            </tr>   
                                              @php
                                                       }

                                                    @endphp
                                    
                                                     @php
                                                        $last=$prod->CODE
                                                    @endphp
                                                    @endforeach

                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>

					</div>
				</div>


      

  
<script>
  
    $.ajaxSetup({
      headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              }
    });
    
    $('.warna').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.warna').index(this) + 1;
         $('.warna').eq(index).focus();
     }
    });
    $('.putih').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.putih').index(this) + 1;
         $('.putih').eq(index).focus();
     }
    });
    
    $('.nops').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.nops').index(this) + 1;
         $('.nops').eq(index).focus();
     }
    });
    
    $('.motif').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.motif').index(this) + 1;
         $('.motif').eq(index).focus();
     }
    });
    $('.salur').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.salur').index(this) + 1;
         $('.salur').eq(index).focus();
     }
    });
    $('.salur2').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.salur2').index(this) + 1;
         $('.salur2').eq(index).focus();
     }
    });
    $('.biru').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.biru').index(this) + 1;
         $('.biru').eq(index).focus();
     }
    });
    $('.abu').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.abu').index(this) + 1;
         $('.abu').eq(index).focus();
     }
    });
    $('.khaki').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.khaki').index(this) + 1;
         $('.khaki').eq(index).focus();
     }
    });
    $('.pink').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.pink').index(this) + 1;
         $('.pink').eq(index).focus();
     }
    });
    
    $('.kuning').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.kuning').index(this) + 1;
         $('.kuning').eq(index).focus();
     }
    });
    $('.hijau').keydown(function (e) {
     if (e.which === 13) {
         var index = $('.hijau').index(this) + 1;
         $('.kuning').eq(index).focus();
     }
    });
    
    
    
    $.getJSON("list-data", function(data){
        $.each(data.data, function(i,item){
            //console.log(item[0].prdcd);
            $("#"+item.prdcd).val(item.qty);
            var prdcd = item.prdcd;
            var arr = prdcd.split("-");            
            recount(arr[0],arr[1],arr[2]);
        });
        
      });
   
    function recount(code,size,color)
    {         
        var wrn = $("#"+code+"-"+size+"-WRN").val();
        var pth = $("#"+code+"-"+size+"-PTH").val();
        var nop = $("#"+code+"-"+size+"-NOP").val();
        var mtf = $("#"+code+"-"+size+"-MTF").val();
        var sfp = $("#"+code+"-"+size+"-SFP").val();
        var slf = $("#"+code+"-"+size+"-SLF").val();
        var snp = $("#"+code+"-"+size+"-SNP").val();
        var abu = $("#"+code+"-"+size+"-ABU").val();
        var kki = $("#"+code+"-"+size+"-KKI").val();
        var pnk = $("#"+code+"-"+size+"-PNK").val();
        var bru = $("#"+code+"-"+size+"-BRU").val();
        var hju = $("#"+code+"-"+size+"-HJU").val();
        var kng = $("#"+code+"-"+size+"-KNG").val();


        
          if(wrn=="")
        {
            wrn=0;
        }
         if(pth=="")
        {
            pth=0;
        }
         if(nop=="")
        {
            nop=0;
        }
         if(mtf=="")
        {
            mtf=0;
        }
        if(sfp=="")
        {
            sfp=0;
        }
        if(slf=="")
        {
            slf=0;
        }
        if(snp=="")
        {
            snp=0;
        }
        if(abu=="")
        {
            abu=0;
        }
        if(kki=="")
        {
            kki=0;
        }
        if(pnk=="")
        {
            pnk=0;
        }
        if(bru=="")
        {
            bru=0;
        }
        if(hju=="")
        {
            hju=0;
        }
        if(kng=="")
        {
            kng=0;
        }
        var prc1 = $("#PRC"+code+size+"-1").html();
        var prc2 = $("#PRC"+code+size+"-2").html();
        
        var tot = ((parseFloat(pth)+parseFloat(wrn)+parseFloat(nop))*parseFloat(prc1))+((parseFloat(mtf)+parseFloat(sfp)+parseFloat(slf)+parseFloat(snp)+parseFloat(abu)+parseFloat(kki)+parseFloat(pnk))*parseFloat(prc2));
        $("#"+code+size).val(parseFloat(tot));
        
        var total= 0;
        $('#tblord > tbody  > tr').each(function (i, row){   
         var $row = $(row);    
         grand = $row.find('td#total input').val();
         total=total+parseFloat(grand);
            //console.log(family);
        });
        $("#grand").val(total);
    }
    
    function process(code,size,color)
    {
        //console.log(code+size+color);   
        var wrn = $("#"+code+"-"+size+"-WRN").val();
        var pth = $("#"+code+"-"+size+"-PTH").val();
        var nop = $("#"+code+"-"+size+"-NOP").val();
        var mtf = $("#"+code+"-"+size+"-MTF").val();
        var sfp = $("#"+code+"-"+size+"-SFP").val();
        var slf = $("#"+code+"-"+size+"-SLF").val();
        var snp = $("#"+code+"-"+size+"-SNP").val();
        var abu = $("#"+code+"-"+size+"-ABU").val();
        var kki = $("#"+code+"-"+size+"-KKI").val();
        var pnk = $("#"+code+"-"+size+"-PNK").val();
        var bru = $("#"+code+"-"+size+"-BRU").val();

        var hju = $("#"+code+"-"+size+"-HJU").val();

        var kng = $("#"+code+"-"+size+"-KNG").val();

        
        var prc1 = $("#PRC"+code+size+"-1").html();
        var prc2 = $("#PRC"+code+size+"-2").html();
         if(wrn=="")
        {
            wrn=0;
        }
         if(pth=="")
        {
            pth=0;
        }
         if(nop=="")
        {
            nop=0;
        }
         if(mtf=="")
        {
            mtf=0;
        }
        if(sfp=="")
        {
            sfp=0;
        }
        if(slf=="")
        {
            slf=0;
        }
        if(snp=="")
        {
            snp=0;
        }
        if(abu=="")
        {
            abu=0;
        }
        if(kki=="")
        {
            kki=0;
        }
        if(pnk=="")
        {
            pnk=0;
        }
        if(bru=="")
        {
            bru=0;
        }
        if(hju=="")
        {
            hju=0;
        }
        if(kng=="")
        {
            kng=0;
        }
        var tot = ((parseFloat(pth)+parseFloat(wrn)+parseFloat(nop))*parseFloat(prc1))+((parseFloat(mtf)+parseFloat(sfp)+parseFloat(slf)+parseFloat(snp)+parseFloat(abu)+parseFloat(kki)+parseFloat(pnk)+parseFloat(bru)+parseFloat(hju)+parseFloat(kng))*parseFloat(prc2));
        $("#"+code+size).val(parseFloat(tot));
        
        var prdcd = code+"-"+size+"-"+color;
        var qty = $("#"+code+"-"+size+"-"+color).val();
            $.get('tambah',{prdcd:prdcd,qty:qty},function(data) {
                console.log(data.length);
                if(data.length<8)
                    {
                        $("#"+code+"-"+size+"-"+color).val('');
                        alert('produk tidak tersedia');
                    }
                else
                    {
                        $.get("cart", function(data){
                          $('#myModal').modal('hide');
                          $('#cart').text("");
                          $('#cart').append('<i class="ace-icon fa fa-shopping-cart"></i> Cart <span class="badge badge-warning">'+ data+'</span>');
                        });        
                    }
                
            });
        
        
        var total= 0;
        $('#tblord > tbody  > tr').each(function (i, row){   
         var $row = $(row);    
         grand = $row.find('td#total input').val();
          console.log(grand);
         total=total+parseFloat(grand);
            //console.log(family);
        });
        console.log(total);
        $("#grand").val(total);
    }
   var $body = $(".table-container-body"),
    $header = $(".table-container-header"),
    $footer = $(".table-container-footer");

// Get ScrollBar width(From: http://bootstrap-table.wenzhixin.net.cn/)
var scrollBarWidth = (function () {
        var inner = $('<p/>').addClass('fixed-table-scroll-inner'),
            outer = $('<div/>').addClass('fixed-table-scroll-outer'),
            w1, w2;
        outer.append(inner);
        $('body').append(outer);
        w1 = inner[0].offsetWidth;
        outer.css('overflow', 'scroll');
        w2 = inner[0].offsetWidth;
        if (w1 === w2) {
            w2 = outer[0].clientWidth;
        }
        outer.remove();
        return w1 - w2;
})();

// Scroll horizontal
$body.on('scroll', function () {
    $header.scrollLeft($(this).scrollLeft());
    $footer.scrollLeft($(this).scrollLeft());
});

// Redraw Header/Footer
var redraw = function() {
    var tds = $body.find("> table > tbody > tr:first-child > td");
    tds.each(function (i) {
        var width = $(this).innerWidth(),
            lastPadding = (tds.length -1 == i ? scrollBarWidth : 0);
        lastHeader = $header.find("th:eq("+i+")").innerWidth(width + lastPadding);
        lastFooter = $footer.find("th:eq("+i+")").innerWidth(width + lastPadding);
    });
};

// Selection
$body.find("> table > tbody > tr > td").click(function(e) {
    $body.find("> table > tbody > tr").removeClass("info");
    $(e.target).parent().addClass('info');
});

// Listen to Resize Window
$(window).resize(redraw);
redraw();
    
        var hash = document.location.hash;
var prefix = "tab_";
if (hash) {
    $('.nav-tabs a[href="'+hash.replace(prefix,"")+'"]').tab('show');
} 

// Change hash for page-reload
$('.nav-tabs a').on('shown', function (e) {
    window.location.hash = e.target.hash.replace("#", "#" + prefix);
});
    
</script>
@else
	<script>
		alert('Harap Login Terlebih dahulu');
  		window.location.href='home';
  	</script>
@endif
@endsection   