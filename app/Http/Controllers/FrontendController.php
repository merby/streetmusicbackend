<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\News;
use App\Models\Podcast;
use App\Models\PodcastCategory;
use App\Models\TrackMusicianApps;
use App\NewsCategory;
use App\Tags;
use App\User;
use App\UserDetail;
use Illuminate\Support\Facades\DB;
use URL;
use App\Models\TrackApps;
use App\Models\Event;
use App\Models\BandApps;
use App\Models\Cafe;




class FrontendController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('front.index');
    }

    public function track(Request $request)
    {
        $req = $request->all();
        $data = DB::table('track_apps')
        ->select('track_apps.*','band_apps.name as artistName','band_apps.url as artistImage')
        ->join('band_apps','band_apps.id','=','track_apps.owner_id')
        ->where('track_apps.id','=',$req['id'])
        ->get();   

        // dd($data);
        // $arr = Array("cafe_information"=>$data,"cafe_menus"=>$data->cafe_menu,"cafe_image"=>$data->cafe_image);
        // return response()->json($arr);
                return view('front.track.index',compact('data'));
    }

    public function artist(Request $request)
    {
        $req = $request->all();
        $data = BandApps::find($req['id']);     

        // dd($data);
        // $arr = Array("cafe_information"=>$data,"cafe_menus"=>$data->cafe_menu,"cafe_image"=>$data->cafe_image);
        // return response()->json($arr);
                return view('front.track.artist',compact('data'));
    }


    public function cafe(Request $request)
    {
        $req = $request->all();
        $data = Cafe::find($req['id']);     
 

        // dd($data);
        // $arr = Array("cafe_information"=>$data,"cafe_menus"=>$data->cafe_menu,"cafe_image"=>$data->cafe_image);
        // return response()->json($arr);
                return view('front.track.cafe',compact('data'));
    }


    public function event(Request $request)
    {
        $req = $request->all();
        $data = Event::find($req['id']);     
        // dd($data);

        // dd($data);
        // $arr = Array("cafe_information"=>$data,"cafe_menus"=>$data->cafe_menu,"cafe_image"=>$data->cafe_image);
        // return response()->json($arr);
                return view('front.track.event',compact('data'));
    }

    public function register(){
        return view('front.register.create_register');
    }

    public function storeRegister(Request $request){
      $request->validate([
          'name' => 'required',
          'email'=> 'required|email',
          'password' => 'required|alphaNum|min:6',
          'nik' => 'required|numeric',
          'npwp' => 'required|numeric',
           'foto_usaha' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
           'foto_selfie' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
      ]);
      try {
        DB::connection('mysql')->beginTransaction();
        $data = $request->all();
        $imageUsaha = time().'.'.$request->foto_usaha->extension();
        $imageSelfie = time().'.'.$request->foto_selfie->extension();
        $request->foto_usaha->move(public_path('images'), $imageUsaha);
        $request->foto_selfie->move(public_path('images'), $imageSelfie);

        $user = User::create([
          'name' => $data['name'],
          'email' => $data['email'],
          'password' => $data['password'],
          'isActivated' => 0,
          'emailVerification' => 0,
        ]);
        UserDetail::create([
          'user_id' => (int)$user->id,
          'nik' => $data['nik'],
          'npwp' => $data['npwp'],
          'foto_usaha' => 'images/' . $imageUsaha,
          'foto_selfie' =>'images/' . $imageSelfie,
        ]);


        DB::connection('mysql')->commit();
    } catch (\Exception $e) {
        dd($e);
        DB::connection('mysql')->rollback();
        return redirect()->back()->withInput($request->input())
          ->withErrors(['Something wrong, unable to save data']);
    }

    return redirect(URL::to('/') . '/success-register' . '/' . (int)$user->id)->with('success','Register created successfully.');
    }


    public function viewSuccess(int $id){
        return view('front.register.success_registrasi', compact('id'));

    }
    
    public function verifiedUser(int $id){
      $user = User::where('id', $id)->first();
      $user->isActivated=1;
      $user->EmailVerification=1;
      $user->save();

      return view('front.register.verificationEmail',compact('id'));
    }


}
