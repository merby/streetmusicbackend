<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudioBookingAddons extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio_booking_addons', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->unsignedBigInteger('studio_booking_id')->unsigned()->index();
            $table->foreign('studio_booking_id')->references('id')->on('studio_bookings');
            $table->unsignedBigInteger('studio_addon_id')->unsigned()->index();
            $table->foreign('studio_addon_id')->references('id')->on('studio_service_addons');
            $table->float('hpp_price'); 
            $table->timestamps();                               
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studio_booking_addons');
    }
}
