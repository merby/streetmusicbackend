<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCafeScheduleBandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cafe_schedule_band', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('cafe_schedule_id')->unsigned()->index();
            $table->foreign('cafe_schedule_id')->references('id')->on('cafe_schedule')->onDelete('cascade');
            $table->integer('band_id');
            $table->string('pic_band');
            $table->string('pic_band_phone');
            $table->string('band_name');
            $table->string('song')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cafe_schedule_band');
    }
}
