<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBandApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
   

        Schema::create('band_apps', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('owner_id');
            $table->foreign('owner_id')->references('uid')->on('user_apps')->onDelete('cascade');
            $table->string('genres');
            $table->text('url');
            $table->string('name');
            $table->string('contact');
            $table->text('bio')->nullable();
            $table->text('website')->nullable();
            $table->text('location');
            $table->text('personil');
            $table->datetime('createdAt');
            $table->timestamps();
            $table->softDeletes();
        });

        
        // Schema::create('user_has_band', function (Blueprint $table) {
        //     $table->increments('id')->unsigned();
            
        //     $table->string('band_apps_id');
        //     $table->foreign('band_apps_id')->references('id')->on('band_apps')->onDelete('cascade');                
        //     $table->timestamps();
        //     $table->softDeletes();
        //  });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('band_apps');
        // Schema::dropIfExists('user_has_band');
    }
}
