<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ModifyStudioPriceHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('studio_price_hours', function (Blueprint $table) {
            //
            $table->string('duration_hour')->after('studio_id');            
            $table->dropColumn('start_time');
            $table->dropColumn('end_time');
            $table->dropColumn('red_day');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('studio_price_hours', function (Blueprint $table) {
            //
            $table->string('duration_hour');

        });
    }
}
