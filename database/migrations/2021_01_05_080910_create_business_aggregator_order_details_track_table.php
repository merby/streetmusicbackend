<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessAggregatorOrderDetailsTrackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_aggregator_order_details_track', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orders_details_id')->unsigned()->index();
            // $table->foreign('business_aggregator_orders_details_id')->references('id')->on('business_aggregator_orders_details');
            $table->string('judul_rilis');
            $table->string('cover_rilis');
            $table->string('nama_artis');
            $table->string('link_spotify')->nullable();;
            $table->string('foto_artis');
            $table->string('nama_artis_tamu');
            $table->string('link_spotify_tamu')->nullable();;
            $table->unsignedInteger('genre_id');
            $table->string('genre_sekunder');
            $table->unsignedInteger('bahasa_rilis_id');
            $table->string('nama_hak_cipta');
            $table->integer('tahun_rilis');
            $table->string('nama_master_rekaman');
            $table->integer('tahun_master_rekaman');
            $table->string('informasi_rilis');
            $table->string('tanggal_rilis');
            $table->string('file_music');
            $table->string('set_start');
            $table->string('nama_pencipta');
            $table->string('nama_penulis');
            $table->string('nama_produser');
            $table->unsignedInteger('bahasa_lirik_id');
            $table->unsignedInteger('explicit');
            $table->text('lirik_lagu');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_aggregator_order_details_track');
    }
}
