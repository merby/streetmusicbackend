<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUserApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

            
        Schema::create('user_apps', function (Blueprint $table) {
            $table->string('uid')->primary();
            $table->string('fullname')->nullable();
            $table->string('displayName');
            $table->text('photoURL');
            $table->string('email')->nullable();
            $table->string('religion');
            $table->string('phone');
            $table->string('gender');
            $table->string('bio');
            $table->date('birthdate');
            $table->text('job');
            $table->text('address');
            $table->boolean('available')->default(0);
            $table->boolean('additional')->default(0);
            $table->text('position')->nullable();
            $table->text('genre')->nullable();
            $table->text('gears')->nullable();
            $table->softDeletes();            
            $table->timestamps();
        });       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_apps');
    }
}
