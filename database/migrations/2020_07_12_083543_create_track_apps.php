<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTrackApps extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       
        Schema::create('track_apps', function (Blueprint $table) {            
            $table->string('id')->primary();                 
            $table->string('owner_id')->index();
            $table->foreign('owner_id')->references('id')->on('band_apps')->onDelete('cascade');                
            $table->string('album');
            $table->string('type');
            $table->string('title');        
            $table->text('url');   
            $table->text('genre')->nullable();
            $table->text('playlist')->nullable();
            $table->text('star')->nullable();
            $table->integer('starCount')->default(0);
            $table->integer('playlistCount')->default(0);
            $table->integer('playlistAdded')->default(0);     
            $table->datetime('createdAt');
            $table->timestamps();
            $table->softDeletes();
        });

        // Schema::create('band_has_tracks', function (Blueprint $table) {
        //     $table->increments('id')->unsigned();
        //     $table->string('track_apps_id')->index();
        //     $table->foreign('track_apps_id')->references('id')->on('track_apps')->onDelete('cascade');
            
        //     $table->timestamps();
        //     $table->softDeletes();
        //  });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_apps');
        // Schema::dropIfExists('band_has_tracks');
    }
}
