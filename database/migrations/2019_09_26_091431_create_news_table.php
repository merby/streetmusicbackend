<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('news_category', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('slug');
            $table->boolean('enabled');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('news_category')->onDelete('cascade');
            $table->string('title');
            $table->string('slug');
            $table->longText('content');
            $table->string('file');
            $table->string('banner_image');
            $table->dateTime('published_at');
            $table->boolean('is_published');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('tags', function (Blueprint $table) {
            $table->bigIncrements('id');            
            $table->string('tag');
            $table->string('slug');
            $table->timestamps();
        });

        Schema::create('news_tags', function (Blueprint $table) {
                $table->increments('id')->unsigned();
                $table->unsignedBigInteger('news_id')->unsigned()->index();
                $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');
                $table->unsignedBigInteger('tags_id')->unsigned()->index();
                $table->foreign('tags_id')->references('id')->on('tags')->onDelete('cascade');                
                $table->timestamps();
                $table->softDeletes();
        });

        Schema::create('news_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('news_id')->unsigned()->index();
            $table->foreign('news_id')->references('id')->on('news')->onDelete('cascade');

            $table->integer('is_reply_to_id')->unsigned()->index();
            $table->foreign('is_reply_to_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('user_id')->unsigned()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->longText('comment');
            $table->boolean('enabled');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news');
        Schema::dropIfExists('news_category');
        Schema::dropIfExists('news_tags');
        Schema::dropIfExists('news_comments');
    }
}
