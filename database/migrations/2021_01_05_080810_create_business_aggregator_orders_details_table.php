<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessAggregatorOrdersDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_aggregator_orders_details', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('orders_id')->unsigned()->index();
            // $table->foreign('business_aggregator_orders_id')->references('id')->on('business_aggregator_orders');
            $table->string('email');
            $table->string('nama_lengkap');
            $table->string('nik');
            $table->string('phone_satu');
            $table->string('phone_dua')->nullable();;
            $table->unsignedInteger('jenis_rilis');
            $table->integer('jumlah_lagu')->nullable();
            $table->integer('jumlah_artis')->nullable();
            $table->string('channel_yt')->nullable();
            $table->string('akun_ig')->nullable();
            $table->string('akun_fb')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_aggregator_orders_details');
    }
}
