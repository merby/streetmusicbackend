<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessAggregatorOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_aggregator_orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_aggreator_id')->unsigned()->index();
            $table->string('status');
            $table->string('request_name');
            $table->date('tanggal');
            $table->unsignedInteger('account_id');
            $table->string('keterangan');
            $table->timestamps();
            $table->softDeletes();

            // $table->foreign('business_aggreator_id')->references('id')->on('business_aggreator');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_aggregator_orders');
    }
}
