<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudioPriceHoursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio_price_hours', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('studio_id')->unsigned()->index();
            $table->foreign('studio_id')->references('id')->on('studios');
            $table->time('start_time');
            $table->time('end_time');
            $table->float('price');
            $table->string('red_day');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studio_price_hours');
    }
}
