<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrackMusicianAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_musician_apps', function (Blueprint $table) {
                $table->string('id')->primary();                 
                $table->string('owner_id')->index();
                $table->foreign('owner_id')->references('uid')->on('user_apps')->onDelete('cascade');                
                $table->string('album');
                $table->string('type');
                $table->string('title');        
                $table->text('url');   
                $table->text('genre')->nullable();
                $table->text('playlist')->nullable();
                $table->text('star')->nullable();
                $table->integer('starCount')->default(0);
                $table->integer('playlistCount')->default(0);
                $table->integer('playlistAdded')->default(0);     
                $table->datetime('createdAt');
                $table->timestamps();
                $table->softDeletes();
            });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('track_musician_apps');
    }
}
