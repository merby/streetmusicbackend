<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudiosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studios', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('studio_type_id')->unsigned()->index();
            $table->foreign('studio_type_id')->references('id')->on('studio_types');
            $table->string('name');
            $table->text('description');
            $table->string('photo');
            $table->time('open_time');
            $table->time('close_time');
            $table->string('studioscol');
            $table->string('location');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studios');
    }
}
