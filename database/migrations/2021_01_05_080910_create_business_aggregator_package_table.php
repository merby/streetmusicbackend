<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBusinessAggregatorPackageTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('business_aggregator_package', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('business_aggreator_id')->unsigned()->index();
            // $table->foreign('business_aggreator_id')->references('id')->on('business_aggregator');
            $table->string('package_name');
            $table->unsignedInteger('price');
            $table->string('description');
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('business_aggregator_package');
    }
}
