<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudioBookings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    

    public function up()
    {
        Schema::create('studio_bookings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('studio_id')->unsigned()->index();
            $table->foreign('studio_id')->references('id')->on('studios');
            $table->unsignedBigInteger('studio_package_id')->unsigned()->index();
            $table->foreign('studio_package_id')->references('id')->on('studio_service_packages');
            $table->float('hpp_price');
            $table->float('addon_price');
            $table->float('ppn');
            $table->float('discount');
            $table->float('total');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studio_bookings');
    }
}
