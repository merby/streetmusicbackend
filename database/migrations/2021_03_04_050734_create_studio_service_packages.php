<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudioServicePackages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('studio_service_packages', function (Blueprint $table) {          
            $table->bigIncrements('id');
            $table->unsignedBigInteger('studio_id')->unsigned()->index();
            $table->foreign('studio_id')->references('id')->on('studios');
            $table->string('name');
            $table->string('description');
            $table->integer('duration');
            $table->float('price');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('studio_service_packages');
    }
}
