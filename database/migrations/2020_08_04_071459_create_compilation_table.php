<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCompilationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compilations', function (Blueprint $table) {
            $table->bigIncrements('id');                        
            $table->string('title');
            $table->text('description')->nullable();
            $table->string('image');
            $table->boolean('is_published')->default(0);
            $table->string('createdBy');
            $table->datetime('createdAt');
            $table->timestamps();            
        });

        Schema::create('compilation_tracks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('compilation_id');
            $table->foreign('compilation_id')->references('id')->on('compilations')->onDelete('cascade');           
            $table->string('track_apps_id')->index();
            $table->foreign('track_apps_id')->references('id')->on('track_apps')->onDelete('cascade');      
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compilations');
        Schema::dropIfExists('compilation_tracks');
    }
}
