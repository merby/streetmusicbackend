<?php

namespace Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class BussinessTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('business_category')->insert([
          [
            'name' => 'Aggregator',
            'description' => '',
            'image'=>'',
          ],
          [
            'name' => 'Event Organizer',
            'description' => '',
            'image'=>'',
          ]
        ]);

        DB::table('genre_musik')->insert([
          [
            'name' => 'Alternative',
          ],
          [
            'name' => 'Ambient/New Age',
          ],
          [
            'name' => 'Blues',
          ],
          [
            'name' => 'Children Music',
          ],
          [
            'name' => 'Classical',
          ],
          [
            'name' => 'Comedy',
          ],
          [
            'name' => 'Country',
          ],
          [
            'name' => 'Easy Listening',
          ],
          [
            'name' => 'Electronic',
          ],
          [
            'name' => 'Experimental',
          ],
          [
            'name' => 'Folk',
          ],
          [
            'name' => 'Funk/R&B',
          ],
          [
            'name' => 'Hip-Hop/Rap',
          ],
          [
            'name' => 'Holiday/Christmas',
          ],
          [
            'name' => 'Inspirational',
          ],
          [
            'name' => 'Jazz',
          ],
          [
            'name' => 'Latin',
          ],
          [
            'name' => 'New Age',
          ],
          [
            'name' => 'Pop',
          ],
          [
            'name' => 'R&B',
          ],
          [
            'name' => 'Reggae',
          ],
          [
            'name' => 'Rock',
          ],
          [
            'name' => 'Soul',
          ],
          [
            'name' => 'Soundtrack',
          ],
          [
            'name' => 'Spiritual',
          ],
          [
            'name' => 'Spoken Word/Speeches',
          ],
          [
            'name' => 'Vocal/Nostalgia',
          ],
          [
            'name' => 'World',
          ],
          [
            'name' => 'Flamenco',
          ],
          [
            'name' => 'Fitness&Workout',
          ],
          [
            'name' => 'Karaoke',
          ],
          [
            'name' => 'Ska',
          ],
          [
            'name' => 'Punk',
          ],
          [
            'name' => 'Trap',
          ]
        ]);

        DB::table('bahasa')->insert([
          [
            'name' => 'Bahaha Indonesia',
            'type' => 'Rilis',
          ],
          [
            'name' => 'Bahasa Inggris / English',
            'type' => 'Rilis',
          ],
          [
            'name' => 'Konten Tanpa Bahasa',
            'type' => 'Rilis',
          ],
          [
            'name' => 'Bahaha Indonesia',
            'type' => 'Lirik',
          ],
          [
            'name' => 'Bahasa Inggris / English',
            'type' => 'Lirik',
          ],
          [
            'name' => 'Konten Tanpa Bahasa',
            'type' => 'Lirik',
          ]
        ]);
    }
}
