<!DOCTYPE html>
<html lang="zxx">
<head>
    @include('layouts.partials.admin-head')
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader ">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
<div class="page parallel">
    <div class="d-flex row">
        <div class="col-md-8 white">
            <div class="p-5 mt-5" style="font-size:28px;">
                <!-- <span style="color:#34a8eb;" class="logo-dark">Street<span style="font-weight:bold;color:#34a8eb">Corp.</span></span> -->
                <!-- <img class="img-responsive" src="assets/img/logo/smfb.png" title="Streetmusic" alt="Streetmusic for Business" style="width:170px;height:50px;" /> -->
            </div>
            <div class="p-5">
                <h3>Registrasi Akun</h3>
                <p>Ayo buruan jangkau lebih banyak customer anda</p>

                  Berhasil..silahkan cek email
                  <a href="/verification-user/{{$id}}" class="btn btn-primary">klik disini</a>



            </div>

            </div>
        </div>
    </div>
</aside>
<!-- /.right-sidebar -->
<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
<div class="control-sidebar-bg shadow white fixed"></div>
</div>
<!--/#app -->
@include('layouts.partials.admin-plugins')
</body>
</html>
