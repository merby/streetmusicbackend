<!DOCTYPE html>
<html lang="zxx">
<head>
    @include('layouts.partials.admin-head')
</head>
<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader ">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
<div class="page parallel">
    <div class="d-flex row ">
        <div class="col-md-10 white">
            <div class="p-5 mt-5" style="font-size:28px;">
                <!-- <span style="color:#34a8eb;" class="logo-dark">Street<span style="font-weight:bold;color:#34a8eb">Corp.</span></span> -->
                <!-- <img class="img-responsive" src="assets/img/logo/smfb.png" title="Streetmusic" alt="Streetmusic for Business" style="width:170px;height:50px;" /> -->
            </div>
            <div class="justify-center">
                <h3>Registrasi Akun</h3>
                <p>Ayo buruan jangkau lebih banyak customer anda</p>

                    <form method="POST" action="{{ route('store-register') }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group has-icon"><i class="icon-user-o"></i>


                                <input  placeholder="Full Name" id="name" type="text" class="form-control form-control-lg {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group has-icon"><i class="icon-envelope-o"></i>



                                <input placeholder="Email address" id="email" type="email" class="form-form-control form-control-lg {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group has-icon"><i class="icon-key3"></i>
                                <input placeholder="Password" id="password" type="password" class="form-control form-control-lg {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="new-password">

                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group has-icon"><i class="icon-key4"></i>
                                <input placeholder=" Confirm Password" id="password-confirm" type="password" class="form-control form-control-lg" name="password_confirmation" required autocomplete="new-password">

                        </div>

                        <div class="form-group has-icon"><i class="icon-user-o"></i>


                                <input  placeholder="NIK" id="nik" type="text" class="form-control form-control-lg {{ $errors->has('nik') ? ' is-invalid' : '' }}" name="nik" value="{{ old('nik') }}" required autocomplete="name" autofocus>

                                @if ($errors->has('nik'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nik') }}</strong>
                                    </span>
                                @endif

                        </div>

                        <div class="form-group has-icon"><i class="icon-user-o"></i>


                                <input  placeholder="NPWP" id="npwp" type="text" class="form-control form-control-lg {{ $errors->has('npwp') ? ' is-invalid' : '' }}" name="npwp" value="{{ old('npwp') }}" required autocomplete="name" autofocus>

                                @if ($errors->has('npwp'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('npwp') }}</strong>
                                    </span>
                                @endif

                        </div>
                        <label for="foto_selfie" class="form-label ">Foto Selfie</label>
                        <div class="form-group has-icon"><i class="icon-user-o"></i>
                          <input name="foto_selfie" id="foto_selfie"  class="form-control r-0 light s-12 " type="file">
                          @if ($errors->has('foto_selfie'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto_selfie') }}</strong>
                              </span>
                          @endif
                        </div>

                        <label for="foto_selfie" class="form-label ">Foto usaha</label>
                        <div class="form-group has-icon"><i class="icon-user-o"></i>
                          <input name="foto_usaha" id="foto_usaha"  class="form-control r-0 light s-12 " type="file">
                          @if ($errors->has('foto_usaha'))
                              <span class="invalid-feedback" role="alert">
                                  <strong>{{ $errors->first('foto_usaha') }}</strong>
                              </span>
                          @endif
                        </div>



                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>



            </div>

            </div>
        </div>
    </div>
</aside>
<!-- /.right-sidebar -->
<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->
<div class="control-sidebar-bg shadow white fixed"></div>
</div>
<!--/#app -->
@include('layouts.partials.admin-plugins')
</body>
</html>
