@extends('layouts.app')

@section('content')


            <nav aria-label="breadcrumb" class="breadcrumb-nav">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item active" aria-current="page">About us</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content pb-3">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="about-text text-center mt-3">
                                <h2 class="title text-center mb-2">Apakah Streetmusic itu ?</h2><!-- End .title text-center mb-2 -->
                                <p>
                                    Streetmusic adalah sebuah media publishing di bidang musik, kita hadir bertujuan untuk memasarkan karya-karya musisi dibawah permukaan tanah
                                    kepada masyarakat umum, untuk dapat kesempatan dipublish di media ini anda harus daftar sebagai musisi di https://musisi.streetmusic.id sebagai musisi, media digital 
                                    untuk membantu para musisi menampilkan karya-karyanya.

                                </p>
                                <br>
                                <br>

                                <img src="img/sm-dark.png" alt="image" class="mx-auto mb-6" style="width:50%">
                            </div><!-- End .about-text -->
                        </div><!-- End .col-lg-10 offset-1 -->
                    </div><!-- End .row -->
                    <div class="row justify-content-center">
                        <div class="col-lg-4 col-sm-6">
                            <div class="icon-box icon-box-sm text-center">
                                <span class="icon-box-icon">
                                    <i class="icon-puzzle-piece"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Menyingkap Talent</h3><!-- End .icon-box-title -->
                                    <p>Streetmusic adalah wadah untuk musisi dapat memperkenalkan karya-karyanya  agar dapat muncul bakat bakat baru yang tersembunyi.</p>
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-lg-4 col-sm-6 -->

                        <div class="col-lg-4 col-sm-6">
                            <div class="icon-box icon-box-sm text-center">
                                <span class="icon-box-icon">
                                    <i class="icon-life-ring"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Kenal lebih dekat</h3><!-- End .icon-box-title -->
                                    <p>Dengan adanya media ini diharapkan para penikmat musik dapat lebih dekat dengan para musisi dan artis idolanya</p>
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-lg-4 col-sm-6 -->

                        <div class="col-lg-4 col-sm-6">
                            <div class="icon-box icon-box-sm text-center">
                                <span class="icon-box-icon">
                                    <i class="icon-heart-o"></i>
                                </span>
                                <div class="icon-box-content">
                                    <h3 class="icon-box-title">Dibuat dengan hati</h3><!-- End .icon-box-title -->
                                    <p>Karena kita mengutamakan kepuasan para musisi dan para pendengar musik maupun para fans</p> 
                                </div><!-- End .icon-box-content -->
                            </div><!-- End .icon-box -->
                        </div><!-- End .col-lg-4 col-sm-6 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->

                <div class="mb-2"></div><!-- End .mb-2 -->

                <div class="bg-image pt-7 pb-5 pt-md-12 pb-md-9" style="background-image: url(assets/images/demos/demo-20/banners/banner-12.png)">
                    <div class="container">
                        <div class="row">
                            <div class="col-6 col-md-3">
                                <div class="count-container text-center">
                                    <div class="count-wrapper text-white">
                                        <span class="count" data-from="0" data-to="500" data-speed="3000" data-refresh-interval="50">0</span>+
                                    </div><!-- End .count-wrapper -->
                                    <h3 class="count-title text-white">Musisi tergabung</h3><!-- End .count-title -->
                                </div><!-- End .count-container -->
                            </div><!-- End .col-6 col-md-3 -->

                            <div class="col-6 col-md-3">
                                <div class="count-container text-center">
                                    <div class="count-wrapper text-white">
                                        <span class="count" data-from="0" data-to="5" data-speed="3000" data-refresh-interval="50">0</span>+
                                    </div><!-- End .count-wrapper -->
                                    <h3 class="count-title text-white">Artikel</h3><!-- End .count-title -->
                                </div><!-- End .count-container -->
                            </div><!-- End .col-6 col-md-3 -->

                            <div class="col-6 col-md-3">
                                <div class="count-container text-center">
                                    <div class="count-wrapper text-white">
                                        <span class="count" data-from="0" data-to="11" data-speed="3000" data-refresh-interval="50">0</span>+
                                    </div><!-- End .count-wrapper -->
                                    <h3 class="count-title text-white">Partnership</h3><!-- End .count-title -->
                                </div><!-- End .count-container -->
                            </div><!-- End .col-6 col-md-3 -->

                            <div class="col-6 col-md-3">
                                <div class="count-container text-center">
                                    <div class="count-wrapper text-white">
                                        <span class="count" data-from="0" data-to="4" data-speed="3000" data-refresh-interval="50">0</span>+
                                    </div><!-- End .count-wrapper -->
                                    <h3 class="count-title text-white">Events</h3><!-- End .count-title -->
                                </div><!-- End .count-container -->
                            </div><!-- End .col-6 col-md-3 -->
                        </div><!-- End .row -->
                    </div><!-- End .container -->
                </div><!-- End .bg-image pt-8 pb-8 -->

                <div class="bg-light-2 pt-6 pb-7 mb-6">
                    <div class="container">
                        <h2 class="title text-center mb-4">Meet Our Team</h2><!-- End .title text-center mb-2 -->

                        <div class="row">
                            <div class="col-sm-6 col-lg-3">
                                <div class="member member-2 text-center">
                                    <figure class="member-media">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F61261441_10216726041756978_1953114799940304896_o%20(1)%20(2).jpg?alt=media&token=313c2cf7-c893-4ad5-8904-688f93d0224a" alt="member photo">

                                        <figcaption class="member-overlay">
                                            <div class="social-icons social-icons-simple">
                                                <a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                                                <a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                                <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                            </div><!-- End .soial-icons -->
                                        </figcaption><!-- End .member-overlay -->
                                    </figure><!-- End .member-media -->
                                    <div class="member-content">
                                        <h3 class="member-title">Ricky Merbiansyah<span>Co-founder & CEO</span></h3><!-- End .member-title -->
                                    </div><!-- End .member-content -->
                                </div><!-- End .member -->
                            </div><!-- End .col-lg-3 -->

                            <div class="col-sm-6 col-lg-3">
                                <div class="member member-2 text-center">
                                    <figure class="member-media">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F71650138_2820343687976341_1918216275104890880_n.jpg?alt=media&token=c2a8d1bb-df59-4d81-a353-812ae6fa58a1" alt="member photo">

                                        <figcaption class="member-overlay">
                                            <div class="social-icons social-icons-simple">
                                                <a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                                                <a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                                <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                            </div><!-- End .soial-icons -->
                                        </figcaption><!-- End .member-overlay -->
                                    </figure><!-- End .member-media -->
                                    <div class="member-content">
                                        <h3 class="member-title">Dena Vicky Adiyana<span>Co-founder & Musisi Platform Lead</span></h3><!-- End .member-title -->
                                    </div><!-- End .member-content -->
                                </div><!-- End .member -->
                            </div><!-- End .col-lg-3 -->

                            <div class="col-sm-6 col-lg-3">
                                <div class="member member-2 text-center">
                                    <figure class="member-media">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/photos%2Fundefined_GRyolhalstNHBW12d3qwxAqVGEk1?alt=media&token=f75ee9af-8d90-4293-b953-386375fdbd18" alt="member photo">

                                        <figcaption class="member-overlay">
                                            <div class="social-icons social-icons-simple">
                                                <a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                                                <a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                                <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                            </div><!-- End .soial-icons -->
                                        </figcaption><!-- End .member-overlay -->
                                    </figure><!-- End .member-media -->
                                    <div class="member-content">
                                        <h3 class="member-title">Raka Pratama<span>Musisi Platform Co-Lead</span></h3><!-- End .member-title -->
                                    </div><!-- End .member-content -->
                                </div><!-- End .member -->
                            </div><!-- End .col-lg-3 -->

                            <div class="col-sm-6 col-lg-3">
                                <div class="member member-2 text-center">
                                    <figure class="member-media">
                                        <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F36378386_10209588104578557_7329292120743739392_o.jpg?alt=media&token=9356718d-1033-4441-9bac-832284ede104" alt="member photo">

                                        <figcaption class="member-overlay">
                                            <div class="social-icons social-icons-simple">
                                                <a href="#" class="social-icon" title="Facebook" target="_blank"><i class="icon-facebook-f"></i></a>
                                                <a href="#" class="social-icon" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                                <a href="#" class="social-icon" title="Instagram" target="_blank"><i class="icon-instagram"></i></a>
                                            </div><!-- End .soial-icons -->
                                        </figcaption><!-- End .member-overlay -->
                                    </figure><!-- End .member-media -->
                                    <div class="member-content">
                                        <h3 class="member-title">Rizal Ferrari<span>Designer & Programming</span></h3><!-- End .member-title -->
                                    </div><!-- End .member-content -->
                                </div><!-- End .member -->
                            </div><!-- End .col-lg-3 -->

                            

                     
                    </div><!-- End .container -->
                </div><!-- End .bg-light-2 pt-6 pb-6 -->

                <div class="container">
                    <div class="row">
                        <div class="col-lg-10 offset-lg-1">
                            <div class="brands-text text-center mx-auto mb-6">
                                <h2 class="title">Our Partnership</h2><!-- End .title -->
                                <p>Streetmusic menjalin kerja sama dengan beberapa sahabat dan mitra untuk saling memberikan manfaat untuk kemajuan musik indonesia</p>
                            </div><!-- End .brands-text -->
                            <div class="brands-display">
                                <div class="row justify-content-center">
                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/1.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/2.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/3.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/7.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/4.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/5.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/6.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->

                                    <div class="col-6 col-sm-4 col-md-3">
                                        <a href="#" class="brand">
                                            <img src="assets/images/brands/9.png" alt="Brand Name">
                                        </a>
                                    </div><!-- End .col-md-3 -->
                                </div><!-- End .row -->
                            </div><!-- End .brands-display -->
                        </div><!-- End .col-lg-10 offset-lg-1 -->
                    </div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .page-content -->
            @endsection