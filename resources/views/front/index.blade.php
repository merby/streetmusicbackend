@extends('layouts.app')

@section('content')
<!--FEATURES TOP AREA-->
<section class="features-top-area padding-100-50 wow fadeIn" id="features">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Produk Kami</h2>
                        <p>Produk didesain dengan target-target tertentu agar lebih memudahkan customer kami</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <a href="https://musisi.streetmusic.id">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.1s" style="text-align: center;">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/content.png" alt="" >
                        </div>
                        <h3 class="box-title">Musisi Platform</h3>
                        <p>Merupakan wadah bagi musisi seluruh indonesia untuk yang tetep ingin eksis di industri musik tanah air</p>
                    </div>
                </a>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <a href="https://play.streetmusic.id">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.2s" style="text-align: center;">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/keyword.png" alt="">
                        </div>
                        <h3 class="box-title">Streetmusic Play</h3>
                        <p>Portal untuk musik indonesia dimana terdapat podcast, lagu-lagu hits, berita dan update tentang musik tanah air.</p>
                    </div>
                    </a>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <a href="https://streetmusic.id">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.3s" style="text-align: center;">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/email-marketing.png" alt="">
                        </div>
                        <h3 class="box-title">Streetmusic Business</h3>
                        <p>Layanan sistem management untuk business to business (B2B) dan kemitraan, untuk menunjang berputarnya roda ekonomi</p>
                    </div>
                    </a>
                </div>
                <!-- <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/link.png" alt="">
                        </div>
                        <h3 class="box-title">Link Building</h3>
                        <p>Starting with a full audit of your technical SEO presence means our efforts won’t be wasted on unnoticed structural problems.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/analitics.png" alt="">
                        </div>
                        <h3 class="box-title">Market analysis</h3>
                        <p>Starting with a full audit of your technical SEO presence means our efforts won’t be wasted on unnoticed structural problems.</p>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="text-icon-box bg-white relative mb50 padding20 box-hover-1 wow fadeInUp" data-wow-delay="0.3s">
                        <div class="box-img-icon">
                            <img src="assets/img/icon/search.png" alt="">
                        </div>
                        <h3 class="box-title">Search Strategy</h3>
                        <p>Starting with a full audit of your technical SEO presence means our efforts won’t be wasted on unnoticed structural problems.</p>
                    </div>
                </div> -->
            </div>
        </div>
    </section>
    <!--FEATURES TOP AREA END-->

    <!--REPORT AREA-->
    <section class="report-area section-padding white relative">
        <div class="area-bg"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Ricky Merbiansyah</h2>
                        <p>"Berkolaborasi memajukan bangsa dengan peluru musik, Kesetaraan dan kesempatan yang sama dalam berkembang menjadi prioritas kita di streetmusic"</p>
                    </div>
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-8 col-md-offset-2 col-xs-12">
                    <div class="report-form">
                        <form action="#">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-12 mb30">
                                    <input type="url" name="website" id="website" placeholder="Your_Website_Url" required>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 mb30">
                                    <input type="email" name="email" id="email" placeholder="Your_Email_Address" required>
                                </div>
                                <div class="col-md-12 col-xs-12 center">
                                    <button type="submit">Get your website report</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div> -->
        </div>
    </section>
    <!--REPORT AREA END-->

    <!--ABOUT AREA-->
    <section class="about-area padding-top sky-gray-bg wow fadeIn" id="about">
        <div class="about-bottom-area">
            <div class="container">
                <div class="row flex-v-center">
                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                        <div class="about-content xs-mb50">
                            <h3 class="mb30">Apa itu streetmusic ?</h3>
                            <p>Streetmusic adalah perusahaan kecil yang bergerak di industri musik tanah air, yang bertujuan memutarkan roda perekonomian dalam industri kreatif bidang musik agar dapat memberikan hiburan bagi warga indonesia</p>
                            <a href="#features" class="read-more mt30 inline-block">Produk Kami</a>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-12">
                        <div class="about-mockup sm-mt50">
                            <img src="assets/img/about/about-5.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--ABOUT AREA END-->

    <!--TESTMONIAL AREA TWO-->
    <section class="testimonials-style-five padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Testimonial Pengguna</h2>
                        <p>Beberapa pengguna yang memberikan response positif terhadap kami</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="testmonial-style-five">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2 col-xs-12">
                                <div class="testmonial-details-content center">
                                    <div class="testmonial-details">
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Tim Felmingham</h3>
                                                    <p>SEO Specialist , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Him Tim</h3>
                                                    <p>SEO Analyst , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Angel Bura</h3>
                                                    <p>Support Engr. , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Angel Bura</h3>
                                                    <p>Support Engr. , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Angel Bura</h3>
                                                    <p>Support Engr. , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="slide-item">
                                            <div class="single-testimonial">
                                                <div class="author-content">
                                                    <img src="assets/img/quotes.png" alt="" />
                                                    <p>"Traditional on-page SEO is largely dead. Google has got much better at working out what a page is about, and deciding which page best answers a user’s question, to the point where these once-important cues are now redundant."</p>
                                                    <h3>Angel Bura</h3>
                                                    <p>Support Engr. , Hubblespy</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 col-md-offset-3 col-xs-12">
                                <div class="testmonial-photo-list">
                                    <div class="testimonial-photo">
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-1.jpg" alt="" /></div>
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-2.jpg" alt="" /></div>
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-3.jpg" alt="" /></div>
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-4.jpg" alt="" /></div>
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-5.jpg" alt="" /></div>
                                        <div class="single-testmonial-photo"><img src="assets/img/testmonial/testmonial-6.jpg" alt="" /></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="teamslide_nav">
                        <div class="testi_prev"><i class="fa fa-long-arrow-left"></i></div>
                        <div class="testi_next"><i class="fa fa-long-arrow-right"></i></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--TESTMONIAL AREA TWO END-->

    <!--PRICING AREA AREA
    <section class="pricing-area padding-top wow fadeIn" id="pricing">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Simple Pricing</h2>
                        <p>Over 55% of returns are in-effect just exchanges. Identify these opportunities in customer behavior.</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price-three sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.1s">
                        <div class="price-hidding">
                            <h3>Basic</h3>
                        </div>
                        <div class="price-rate">
                            <h3 class="font42">$49.99
                                <sub>/month</sub>
                            </h3>
                        </div>
                        <div class="price-details mb20">
                            <ul>
                                <li><i class="fa fa-check"></i> 120 Key Words Optimized</li>
                                <li><i class="fa fa-check"></i> Keyword Research and Analysis</li>
                                <li><i class="fa fa-check"></i> 24/7 Support Team</li>
                                <li><i class="fa fa-check"></i> OKR Program Reports</li>
                                <li><i class="fa fa-check"></i> Web site Analysis</li>
                                <li><i class="fa fa-check"></i> Content Optimization</li>
                            </ul>
                        </div>
                        <div class="price-button font14">
                            <a href="#" class="purchase-button">Get Started</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price-three sm-mb30 xs-mb30 wow fadeInUp" data-wow-delay="0.2s">
                        <div class="price-hidding">
                            <h3>Standard</h3>
                        </div>
                        <div class="price-rate">
                            <h3 class="font42">$29.99
                                <sub>/month</sub>
                            </h3>
                        </div>
                        <div class="price-details mb20">
                            <ul>
                                <li><i class="fa fa-check"></i> 120 Key Words Optimized</li>
                                <li><i class="fa fa-check"></i> Keyword Research and Analysis</li>
                                <li><i class="fa fa-check"></i> 24/7 Support Team</li>
                                <li><i class="fa fa-check"></i> OKR Program Reports</li>
                                <li><i class="fa fa-check"></i> Web site Analysis</li>
                                <li><i class="fa fa-check"></i> Content Optimization</li>
                            </ul>
                        </div>
                        <div class="price-button font14">
                            <a href="#" class="purchase-button">Get Started</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-price-three wow fadeInUp" data-wow-delay="0.1s">
                        <div class="price-hidding">
                            <h3>Business</h3>
                        </div>
                        <div class="price-rate">
                            <h3 class="font42">$49.99
                                <sub>/month</sub>
                            </h3>
                        </div>
                        <div class="price-details mb20">
                            <ul>
                                <li><i class="fa fa-check"></i> 120 Key Words Optimized</li>
                                <li><i class="fa fa-check"></i> Keyword Research and Analysis</li>
                                <li><i class="fa fa-check"></i> 24/7 Support Team</li>
                                <li><i class="fa fa-check"></i> OKR Program Reports</li>
                                <li><i class="fa fa-check"></i> Web site Analysis</li>
                                <li><i class="fa fa-check"></i> Content Optimization</li>
                            </ul>
                        </div>
                        <div class="price-button font14">
                            <a href="#" class="purchase-button">Get Started</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-lg-4 col-sm-6 col-xs-12 visible-sm">
                    <div class="single-price-three wow fadeInUp" data-wow-delay="0.2s">
                        <div class="price-hidding">
                            <h3>Premium</h3>
                        </div>
                        <div class="price-rate">
                            <h3 class="font42">$99.99
                                <sub>/month</sub>
                            </h3>
                        </div>
                        <div class="price-details mb20">
                            <ul>
                                <li><i class="fa fa-check"></i> 120 Key Words Optimized</li>
                                <li><i class="fa fa-check"></i> Keyword Research and Analysis</li>
                                <li><i class="fa fa-check"></i> 24/7 Support Team</li>
                                <li><i class="fa fa-check"></i> OKR Program Reports</li>
                                <li><i class="fa fa-check"></i> Web site Analysis</li>
                                <li><i class="fa fa-check"></i> Content Optimization</li>
                            </ul>
                        </div>
                        <div class="price-button font14">
                            <a href="#" class="purchase-button">Get Started</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    PRICING AREA AREA END-->

    <!--TEAM AREA-->
    <section class="team-area padding-top" id="team">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Tim Kami</h2>
                        <p>Tim kami bergerak dengan semangat dan ide-ide kreatif, yang menjadikan kita menjadi lebih progressif</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.1s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F61261441_10216726041756978_1953114799940304896_o%20(1)%20(2).jpg?alt=media&token=313c2cf7-c893-4ad5-8904-688f93d0224a" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/Merbie666/"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/MerbieInstinc"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/merby_jr/"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Ricky Merbiansyah S.Tr Kom</h4>
                                <p>Chief Executive Officer</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.2s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F71650138_2820343687976341_1918216275104890880_n.jpg?alt=media&token=c2a8d1bb-df59-4d81-a353-812ae6fa58a1" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/dena.adiyana"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/vickybahaya/"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Dena Vicky Adiana S.Kom</h4>
                                <p>Chief Operating Officer</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.3s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/photos%2Fundefined_GRyolhalstNHBW12d3qwxAqVGEk1?alt=media&token=f75ee9af-8d90-4293-b953-386375fdbd18" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/RakaSizzy"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="https://twitter.com/akaraka07"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/raka_pratama/"><i class="fa fa-instagram"></i></a></li>                                    
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>R.Iskandar Pratama, S.Sn</h4>
                                <p>CPO Musisi Platform</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FA6%20-%202%20(1).png?alt=media&token=3bc5667c-1935-4619-acca-4262b45f87e2" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/KudaGanasRevenge"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/rizalferrari/"><i class="fa fa-instagram"></i></a></li>                                    
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Rizal Ferrari</h4>
                                <p>Designer</p>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.1s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2Fari.png?alt=media&token=7f3ed626-ca57-4097-9b89-0ce944a81b3c" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a target="_blank" href="https://www.facebook.com/ari.galonart"><i class="fa fa-facebook"></i></a></li>
                                    <li><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a target="_blank" href="https://www.instagram.com/moch_ariadhy/"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Ariadhy S.ST.</h4>
                                <p>Marketing Manager SM Play</p>
                            </div>
                        </div>
                    </div>
                </div>                             
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FA6%20-%206%20(1).png?alt=media&token=8e1206a0-9379-4a70-85e7-f241bd678d04" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="https://www.facebook.com/metz.rochmat"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Rohmat Amd.Kom</h4>
                                <p>Business Development Manager</p>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FA6%20-%207.png?alt=media&token=a70d0bba-e03c-4e9a-8beb-6a2a8e8d44fb" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Ikhsan Kusma</h4>
                                <p>Operational Manager SM Play</p>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2Fbjhe.png?alt=media&token=0c7e2a7c-abc2-4358-beb2-578a75ec9606" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Bijhe Jermaya</h4>
                                <p>Development Manager MP</p>                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F15538180_357916131241354_1849330471465385984_n%20-%20Fajar%20Ramadhan.jpg?alt=media&token=dfc9c5e6-54eb-4eb9-b16e-1cd364b94f66" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Fajar Ramadhan</h4>
                                <p>Android Developer</p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FA6%20-%208%20(2).png?alt=media&token=073a3d36-93a1-4452-a5e8-15478360e23b" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Bayu Wardhana </h4>
                                <p>Designer</p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2F1601455969502%20-%20Rendy%20Sizzy.jpg?alt=media&token=0eb612a7-bda2-4fc2-970b-fd2bc7300a31" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Rendy Septiadi Akbar</h4>
                                <p>Operational Manager MP</p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FA6%20-%209.png?alt=media&token=d618c711-2e42-4ae1-a6f8-f4e026bd00e5" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Windra Teciandri</h4>
                                <p>Business Development</p>                                
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-3 col-lg-3 col-sm-6 col-xs-12">
                    <div class="single-team-four mb30 center wow fadeInUp" data-wow-delay="0.4s">
                        <div class="member-image">
                            <img src="https://firebasestorage.googleapis.com/v0/b/streetmusic-id.appspot.com/o/Merby%2FWhatsApp%20Image%202020-11-06%20at%201.57.13%20PM%20-%20Billy%20Winangun.jpeg?alt=media&token=6c2b4954-d5b6-4d58-9574-6c2554770175" alt="">
                            <div class="member-social-bookmark">
                                <ul>
                                    <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                    <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                    <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                                    <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="member-details">
                            <div class="name-and-designation">
                                <h4>Billy Winangun</h4>
                                <p>Operational Team</p>                                
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </section>
    <!--TEAM AREA END-->

    <!--BLOG AREA
    <section class="blog-area padding-100-50 wow fadeIn" id="news">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h2>Latest News</h2>
                        <p>Over 55% of returns are in-effect just exchanges. Identify these opportunities in customer behavior.</p>
                    </div>
                </div>
            </div>
            <div class="row blog-slider">
                <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item mb50 shadow">
                        <div class="blog-thumb">
                            <a href="blog.html"><img src="assets/img/blog/blog-1.jpg" alt=""></a>
                        </div>
                        <div class="blog-details padding30">
                            <p class="blog-meta font12"><a href="#">September 4, 2018 </a></p>
                            <h3 class="blog-title mt10"><a href="blog.html">4 Novel Ways to Generate B2B Leads on Autopilot System</a></h3>
                            <a class="read-more" href="blog.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item mb50 shadow">
                        <div class="blog-thumb">
                            <a href="blog.html"><img src="assets/img/blog/blog-2.jpg" alt=""></a>
                        </div>
                        <div class="blog-details padding30">
                            <p class="blog-meta font12"><a href="#">September 4, 2018 </a></p>
                            <h3 class="blog-title mt10"><a href="blog.html">4 Novel Ways to Generate B2B Leads on Autopilot System</a></h3>
                            <a class="read-more" href="blog.html">Read More</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 col-sm-6 col-xs-12">
                    <div class="single-blog-item mb50 shadow">
                        <div class="blog-thumb">
                            <a href="blog.html"><img src="assets/img/blog/blog-3.jpg" alt=""></a>
                        </div>
                        <div class="blog-details padding30">
                            <p class="blog-meta font12"><a href="#">September 4, 2018 </a></p>
                            <h3 class="blog-title mt10"><a href="blog.html">4 Novel Ways to Generate B2B Leads on Autopilot System</a></h3>
                            <a class="read-more" href="blog.html">Read More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    BLOG AREA END-->

    <!--CLIENT AREA-->
    <section class="client-area padding-bottom wow fadeIn">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-md-offset-2 col-lg-offset-2 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <p>Bisnis dan Kemitraan</p>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="client-list client-slider">
                        <div class="single-client"><img src="assets/img/client/client-6.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-7.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-8.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-9.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-10.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-11.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-12.png" alt=""></div>
                        <div class="single-client"><img src="assets/img/client/client-13.png" alt=""></div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--CLIENT AREA END-->

    <!--FOOER AREA-->
    <footer class="footer-area sky-gray-bg padding-bottom relative wow fadeIn white" id="contact">
        <div class="area-bg"></div>
        <div class="footer-top-area section-padding relative">
            <div class="area-bg"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                        <div class="area-title text-center">
                            <h2>Hubungi Kami</h2>
                            <p>Ayo turut berkontribusi dalam membangun bangsa dengan peluru musik</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-8 col-md-offset-2">
                        <div class="contact-form wow fadeIn">
                            <form action="#" id="contact-form" method="post">
                                <div class="row">
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="name-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="form-name" name="form-name" placeholder="Name.." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6 col-lg-6 col-sm-12 col-xs-12">
                                        <div class="form-group" id="email-field">
                                            <div class="form-input">
                                                <input type="email" class="form-control" id="form-email" name="form-email" placeholder="Email.." required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="phone-field">
                                            <div class="form-input">
                                                <input type="text" class="form-control" id="form-phone" name="form-phone" placeholder="Subject..">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group" id="message-field">
                                            <div class="form-input">
                                                <textarea class="form-control" rows="6" id="form-message" name="form-message" placeholder="Your Message Here..." required></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
                                        <div class="form-group mb0">
                                            <button type="submit">Send Message</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
       
    <!--FOOER AREA END-->
    @endsection
