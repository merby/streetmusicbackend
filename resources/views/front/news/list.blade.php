@extends('layouts.app')

@section('content')

            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">News</a></li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->
		
            <div class="page-content">
                <div class="container">
      
                	<div class="row">
                		<div class="col-lg-9">
                        
                        @foreach ($data as $type)                    
                            <article class="entry entry-list">
                                <div class="row align-items-center">
                                    <div class="col-md-5">
                                        <figure class="entry-media">
                                            <a href="single.html">
                                                <img src="{{ $type->banner_image }}" alt="image desc" style=" width: 400px;
  height: 300px;
  object-fit: cover;">
                                            </a>
                                        </figure><!-- End .entry-media -->
                                    </div><!-- End .col-md-5 -->

                                    <div class="col-md-7">
                                        <div class="entry-body">
                                            <div class="entry-meta">
                                                <span class="entry-author">
                                                    by <a href="#">{{ $type->user->name }}</a>
                                                </span>
                                                <span class="meta-separator">|</span>
                                                <a href="#">{{ date('M d Y', strtotime($type->published_at)) }}</a>
                                              
                                            </div><!-- End .entry-meta -->

                                            <h2 class="entry-title">
                                                <a href="{{ url('berita/view',$type->slug) }}">{{ $type->title }}</a>
                                            </h2><!-- End .entry-title -->

                                            <div class="entry-cats">
                                                Tag :
                                                @foreach ($type->tags as $tags)          
                                                <a href="{{ url('berita/tag',$tags->tag) }}">{{ $tags->tag }}</a>,
                                                @endforeach
                                                
                                            </div><!-- End .entry-cats -->

                                            <div class="entry-content">
                                            {!! str_limit(strip_tags($type->content),300,'...')!!}

                                                <a href="{{ url('berita/view',$type->slug) }}" class="read-more">Continue Reading</a>
                                            </div><!-- End .entry-content -->
                                        </div><!-- End .entry-body -->
                                    </div><!-- End .col-md-7 -->
                                </div><!-- End .row -->
                            </article><!-- End .entry -->
                            @if($loop->index==2)
                            <article class="entry entry-list">
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                            <ins class="adsbygoogle"
                                style="display:block"
                                data-ad-client="ca-pub-9404655824796132"
                                data-ad-slot="7180452060"
                                data-ad-format="auto"
                                data-full-width-responsive="true"></ins>
                            <script>
                                (adsbygoogle = window.adsbygoogle || []).push({});
                            </script>
                            </article>
                            @endif
                   
                            @endforeach
                           
     <nav aria-label="Page navigation">
							     <ul class="pagination"> 
                                    {!! $data->links() !!}
							        <!-- <li class="page-item disabled">
							            <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
							                <span aria-hidden="true"><i class="icon-long-arrow-left"></i></span>Prev
							            </a>
							        </li>
							        <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
							        <li class="page-item"><a class="page-link" href="#">2</a></li>
							        <li class="page-item">
							            <a class="page-link page-link-next" href="#" aria-label="Next">
							                Next <span aria-hidden="true"><i class="icon-long-arrow-right"></i></span>
							            </a>
							        </li> -->
							   </ul> 
							</nav>

                			
                		</div><!-- End .col-lg-9 -->

                		<aside class="col-lg-3">
                			<div class="sidebar">
     				            <div class="widget widget-search">
                                 <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
                                    <ins class="adsbygoogle"
                                        style="display:block"
                                        data-ad-client="ca-pub-9404655824796132"
                                        data-ad-slot="8042091685"
                                        data-ad-format="auto"
                                        data-full-width-responsive="true"></ins>
                                    <script>
                                        (adsbygoogle = window.adsbygoogle || []).push({});
                                    </script>

                				</div>

                                <div class="widget widget-cats">
                                    <h3 class="widget-title">Categories</h3><!-- End .widget-title -->

                                    <ul>
                                    	<li>
                                        @foreach ($category as $cat)
                                            <li><a href="{{ url('berita/cat',$cat->id) }}">{{$cat->name}}</a></li>
                                    	
                                        @endforeach
                                    </ul>
                                </div><!-- End .widget -->

                                <div class="widget">
                                    <h3 class="widget-title">Popular Posts</h3><!-- End .widget-title -->

                                    <ul class="posts-list">
                                    	<li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>

                                     
                                        @foreach ($popular as $pop)                 
                                        <li>
                                            <figure>
                                                <a href="{{ url('berita/view',$pop->slug) }}">
                                                    <img src="{{ $pop->banner_image }}" alt="post" style=" width: 100px;
  height: 80px;
  object-fit: cover;">
                                                </a>
                                            </figure>

                                            <div>
                                                <span>{{ date('M d Y', strtotime($pop->published_at)) }}</span>
                                                <h4><a href="{{ url('berita/view',$pop->slug) }}">{{ $pop->title }}</a></h4>
                                            </div>
                                        </li>
                                        @endforeach
                                        <li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>
                                        
                                    </ul><!-- End .posts-list -->
                                </div><!-- End .widget -->

                              

                                <div class="widget">
                                    <h3 class="widget-title">Browse Tags</h3><!-- End .widget-title -->

                                    <div class="tagcloud">
                                 
                                        @foreach ($tags2 as $tg)
                                           <a href="{{ url('berita/tag',$tg->tag) }}">{{$tg->tag}}</a>
                                        @endforeach
                                        <!-- <a href="#">fashion</a>
                                        <a href="#">style</a>
                                        <a href="#">women</a>
                                        <a href="#">photography</a>
                                        <a href="#">travel</a>
                                        <a href="#">shopping</a>
                                        <a href="#">hobbies</a> -->
                                    </div><!-- End .tagcloud -->
                                </div><!-- End .widget -->

                               
                			</div><!-- End .sidebar -->
                		</aside><!-- End .col-lg-3 -->
                	</div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .page-content -->
@endsection