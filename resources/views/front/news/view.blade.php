@extends('layouts.app')

@section('content')

            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="#">view</a></li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div class="container">
                	<div class="row">
                		<div class="col-lg-9">
                        @foreach ($data as $type)       
                            <article class="entry single-entry">
                                <figure class="entry-media">
                                    <img src="{{ $type->banner_image }}" alt="image desc" style=" width: 100%;
  height: 350px;
  object-fit:cover;">
                                </figure><!-- End .entry-media -->

                                <div class="entry-body">
                                    <div class="entry-meta">
                                        <span class="entry-author">
                                            by <a href="#">{{$type->user->name}}</a>
                                        </span>
                                        <span class="meta-separator">|</span>
                                        <a href="#">{{ date('M d Y', strtotime($type->published_at)) }}</a>
                                        <!-- <span class="meta-separator">|</span> -->
                                        <!-- <a href="#">2 Comments</a> -->
                                    </div><!-- End .entry-meta -->

                                    <h2 class="entry-title">
                                        {{$type->title}}
                                    </h2><!-- End .entry-title -->

                                    <div class="entry-cats">
                                            Tag :
                                                @foreach ($type->tags as $tags)          
                                                <a href="{{ url('berita/tag',$tags->tag) }}">{{ $tags->tag }}</a>,
                                                @endforeach
                                    </div><!-- End .entry-cats -->

                                    <div class="entry-content editor-content">
                                    {!! $type->content !!}                                    
                                    </div><!-- End .entry-content -->

                                    <div class="entry-footer row no-gutters flex-column flex-md-row">
                                        <div class="col-md">
                                            <div class="entry-tags">
                                                <span>Tags:</span> 
                                                @foreach ($type->tags as $tags)          
                                                <a href="{{ url('berita/tag',$tags->tag) }}">{{ $tags->tag }}</a>
                                                @endforeach
                                                                                               
                                            </div><!-- End .entry-tags -->
                                        </div><!-- End .col -->

                                        <div class="col-md-auto mt-2 mt-md-0">
                                            <div class="social-icons social-icons-color">
                                                <span class="social-label">Share this post:</span>                                              

                                                <a data-href="http://streetmusic.id" class="social-icon social-facebook" title="Facebook"><i class="icon-facebook-f"></i></a>
                                                <a href="#" class="social-icon social-twitter" title="Twitter" target="_blank"><i class="icon-twitter"></i></a>
                                                <a href="#" class="social-icon social-pinterest" title="Pinterest" target="_blank"><i class="icon-pinterest"></i></a>
                                                <a href="#" class="social-icon social-linkedin" title="Linkedin" target="_blank"><i class="icon-linkedin"></i></a>
                                            </div><!-- End .soial-icons -->
                                        </div><!-- End .col-auto -->
                                    </div><!-- End .entry-footer row no-gutters -->
                                </div><!-- End .entry-body -->                                
                            </article><!-- End .entry -->

                          @endforeach

                      

                         
                          
                		</div><!-- End .col-lg-9 -->

                		<aside class="col-lg-3">
                			<div class="sidebar">
                				<div class="widget widget-search">
                                    <h3 class="widget-title">Search</h3><!-- End .widget-title -->

                                    <form action="#">
                                        <label for="ws" class="sr-only">Search in blog</label>
                                        <input type="search" class="form-control" name="ws" id="ws" placeholder="Search in blog" required>
                                        <button type="submit" class="btn"><i class="icon-search"></i><span class="sr-only">Search</span></button>
                                    </form>
                				</div><!-- End .widget -->

                                <div class="widget widget-cats">
                                    <h3 class="widget-title">Categories</h3><!-- End .widget-title -->

                                    <ul>
                                         @foreach ($category as $cat)
                                            <li><a href="{{ url('berita/cat',$cat->id) }}">{{$cat->name}}</a></li>
                                        @endforeach
                                    </ul>
                                </div><!-- End .widget -->

                                <div class="widget">
                                    <h3 class="widget-title">Popular Posts</h3><!-- End .widget-title -->

                                    <ul class="posts-list">
                                    <li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>
                                    @foreach ($popular as $pop)                 
                                        <li>
                                            <figure>
                                                <a href="{{ url('berita/view',$pop->slug) }}">
                                                    <img src="{{ $pop->banner_image }}" alt="post" style=" width: 100px;
  height: 80px;
  object-fit: cover;">
                                                </a>
                                            </figure>

                                            <div>
                                                <span>{{ date('M d Y', strtotime($pop->published_at)) }}</span>
                                                <h4><a href="{{ url('berita/view',$pop->slug) }}">{{ $pop->title }}</a></h4>
                                            </div>
                                        </li>
                                        @endforeach
                                        <li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>
                                    </ul><!-- End .posts-list -->
                                </div><!-- End .widget -->

                              

                                <div class="widget">
                                    <h3 class="widget-title">Browse Tags</h3><!-- End .widget-title -->

                                    <div class="tagcloud">
                                        @foreach ($tags2 as $tg)
                                           <a href="{{ url('berita/tag',$tg->tag) }}">{{$tg->tag}}</a>
                                        @endforeach
                                    </div><!-- End .tagcloud -->
                                </div><!-- End .widget -->

                               
                			</div><!-- End .sidebar sidebar-shop -->
                		</aside><!-- End .col-lg-3 -->
                	</div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .page-content -->
@endsection