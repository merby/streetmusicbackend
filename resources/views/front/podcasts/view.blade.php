@extends('layouts.app')

@section('content')

            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-3">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                        <li class="breadcrumb-item"><a href="{{url('podcasts1')}}">Podcast</a></li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div class="container">
                	<div class="row">
                		<div class="col-lg-9">

                        @foreach ($data as $type)                    
                            <article class="entry entry-list">
                                <div class="row align-items-center">
                                    <div class="col-md-4">
                                        <figure class="entry-media">
                                            <a href="single.html">
                                                <img src="{{ $type->image }}" alt="image desc">
                                            </a>
                                        </figure><!-- End .entry-media -->
                                    </div><!-- End .col-md-5 -->

                                    <div class="col-md-8">
                                        <div class="entry-body" style="valign:top;">
                                            <div class="entry-meta">
                                                <span class="entry-author">
                                                     <a href="#">{{ $type->category->name }}</a>
                                                </span>
                                                <span class="meta-separator">|</span>
                                                <a href="#">{{ date('d M Y', strtotime($type->created_at)) }}</a>
                                              
                                            </div><!-- End .entry-meta -->

                                            <h2 class="entry-title">
                                                <a href="{{ url('berita/view',$type->slug) }}">{{ $type->title }}</a>
                                            </h2><!-- End .entry-title -->

                                         

                                            <div class="entry-content">
                                            <br>
                                        
                                            {!! str_limit(strip_tags($type->description),300,'...')!!}
                                            <br>
                                            <br>
                                            <br>
                                            <audio controls preload="none"><source src="horse.ogg" type="audio/ogg"><source src="{{$type->file}}" type="audio/mpeg">Your browser does not support the audio element.</audio>

                                           
                                            </div><!-- End .entry-content -->
                                        </div><!-- End .entry-body -->
                                    </div><!-- End .col-md-7 -->
                                </div><!-- End .row -->
                            </article><!-- End .entry -->

                            @endforeach
                           
                            <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="7180452060"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                            <div class="related-posts">
                                <h3 class="title">Related Podcast</h3><!-- End .title -->

                                <div class="owl-carousel owl-simple" data-toggle="owl" 
                                    data-owl-options='{
                                        "nav": false, 
                                        "dots": true,
                                        "margin": 20,
                                        "loop": false,
                                        "responsive": {
                                            "0": {
                                                "items":1
                                            },
                                            "480": {
                                                "items":5
                                            },
                                            "768": {
                                                "items":5
                                            }
                                        }
                                    }'>
                                    @foreach ($data2 as $rel)    
                                          
                                    <article class="entry entry-grid">
                                        <figure class="entry-media">
                                            <a href="{{ url('podcasts1/view',$rel->slug) }}">
                                                <img src="{{$rel->image}}" alt="image desc">
                                            </a>
                                        </figure><!-- End .entry-media -->

                                        <div class="entry-body">
                                            <div class="entry-meta">
                                                <a href="#">{{ date('M d Y', strtotime($rel->created_at)) }}</a>
                                            </div><!-- End .entry-meta -->

                                            <h2 class="entry-title">
                                                <a href="{{ url('podcasts1/view',$rel->slug) }}">{{$rel->title}}</a>
                                            </h2><!-- End .entry-title -->

                                            <div class="entry-cats">
                                                in <a href="#">{{$rel->category->name}}</a>
                                            </div><!-- End .entry-cats -->
                                        </div><!-- End .entry-body -->
                                    </article><!-- End .entry -->
                                    @endforeach
                                </div><!-- End .owl-carousel -->
                            </div><!-- End .related-posts -->



                         
                		</div><!-- End .col-lg-9 -->

                		<aside class="col-lg-3">
                			<div class="sidebar">
                				

                            <div class="widget">
                                    <h3 class="widget-title">Popular Podcast</h3><!-- End .widget-title -->

                                    <ul class="posts-list">
                                    <li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>
                                    @foreach ($popular as $pop)                 
                                        <li>
                                            <figure>
                                                <a href="{{ url('podcasts1/view',$pop->slug) }}">
                                                    <img src="{{ $pop->image }}" alt="post" style=" width: 80px;height: 80px;object-fit: cover;">
                                                </a>
                                            </figure>

                                            <div>
                                                <span>{{ date('M d Y', strtotime($pop->created_at)) }}</span>
                                                <h4><a href="{{ url('podcasts1/view',$pop->slug) }}">{{ $pop->title }}</a></h4>
                                            </div>
                                        </li>
                                        @endforeach
                                        <li>
                                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-format="fluid"
     data-ad-layout-key="-ho+b-17-4n+d5"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="3321098107"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>
                                        </li>
                                    </ul><!-- End .posts-list -->
                                </div><!-- End .widget -->
                              
                                <!-- <div class="widget widget-banner-sidebar">
                                    <div class="banner-sidebar-title">ad box 280 x 280</div><!-- End .ad-title
                                    
                                    <div class="banner-sidebar banner-overlay">
                                        
                                        <a href="#">
                                            <img src="{{asset('assets/images/blog/sidebar/banner.jpg')}}" alt="banner">
                                        </a>
                                    </div>End .banner-ad
                                </div>End .widget -->                               
                			</div><!-- End .sidebar -->
                		</aside><!-- End .col-lg-3 -->
                	</div><!-- End .row -->
                </div><!-- End .container -->
            </div><!-- End .page-content -->
@endsection