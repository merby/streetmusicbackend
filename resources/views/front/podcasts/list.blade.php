@extends('layouts.app')
@section('css')
<style>
     .product-media{
    display:block;
  }
  .small_fig{
    display:none;
  }

    div.player {
  border: 1px solid #eaeaea;
  margin-bottom: 20px;
  max-width: 750px;
  margin: auto;
  margin-top: 40px; }
  div.player:after {
    content: "";
    display: table;
    clear: both; }
  div.player img.album-art {
    width: 245px;
    height: 245px;
    float: left; }

/*
  Small only
*/
@media screen and (max-width: 39.9375em) {
  div.player img.album-art {
    width: 100%;
    height: auto; } }
/*
  Medium only
*/
/*
  Large Only
*/
div.meta-container {
  float: left;
  width: calc(100% - 270px);
  padding: 10px; }
  div.meta-container div.song-title {
    text-align: center;
    color: #263238;
    font-size: 30px;
    font-weight: 600;
    font-family: "Open Sans", sans-serif; }
  div.meta-container div.song-artist {
    text-align: center;
    font-family: "Open Sans", sans-serif;
    font-size: 16px;
    color: #263238;
    margin-top: 10px; }
  div.meta-container div.time-container {
    font-family: Helvetica;
    font-size: 18px;
    color: #000;
    margin-bottom: 10px; }
    div.meta-container div.time-container:after {
      content: "";
      display: table;
      clear: both; }
    div.meta-container div.time-container div.current-time {
      float: left; }
    div.meta-container div.time-container div.duration {
      float: right; }

/*
  Small only
*/
@media screen and (max-width: 39.9375em) {
  div.meta-container {
    width: 100%; } }

/*
  Medium only
*/
/*
  Large Only
*/
div.control-container {
  text-align: center;
  margin-top: 40px; }
  div.control-container div.amplitude-prev {
    width: 28px;
    height: 24px;
    cursor: pointer;
    background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/multiple-songs/previous.svg");
    display: inline-block;
    vertical-align: middle; }
  div.control-container div.amplitude-play-pause {
    width: 40px;
    height: 44px;
    cursor: pointer;
    display: inline-block;
    vertical-align: middle; }
    div.control-container div.amplitude-play-pause.amplitude-paused {
      background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/multiple-songs/play.svg"); }
    div.control-container div.amplitude-play-pause.amplitude-playing {
      background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/multiple-songs/pause.svg"); }
  div.control-container div.amplitude-next {
    width: 28px;
    height: 24px;
    cursor: pointer;
    background: url("https://521dimensions.com/img/open-source/amplitudejs/examples/multiple-songs/next.svg");
    display: inline-block;
    vertical-align: middle; }

/*
  Small only
*/
@media screen and (max-width: 39.9375em) {
  div.control-container div.amplitude-prev {
    margin-right: 10px; }
  div.control-container div.amplitude-next {
    margin-left: 10px; } }
    .product-media{
    display:none;
  }
  .small_fig{
    display:block;
  }
/*
  Medium only
*/
@media screen and (min-width: 40em) and (max-width: 63.9375em) {
  div.control-container div.amplitude-prev {
    margin-right: 20px; }
  div.control-container div.amplitude-next {
    margin-left: 20px; } }
    .product-media{
    display:none;
  }
  .small_fig{
    display:block;
  }
/*
  Large Only
*/
@media screen and (min-width: 64em) {
  div.control-container div.amplitude-prev {
    margin-right: 25px; }
  div.control-container div.amplitude-next {
    margin-left: 25px; } }
progress.amplitude-song-played-progress:not([value]) {
  background-color: #313252; }

progress.amplitude-song-played-progress {
  background-color: #d7dee3;
  -webkit-appearance: none;
  -moz-appearance: none;
  appearance: none;
  width: 100%;
  height: 5px;
  display: block;
  cursor: pointer;
  border-radius: 3px;
  height: 8px;
  border: none; }

progress[value]::-webkit-progress-bar {
  background-color: #d7dee3;
  border-radius: 3px; }

progress[value]::-moz-progress-bar {
  background-color: #00a0ff;
  border-radius: 3px; }

progress[value]::-webkit-progress-value {
  background-color: #00a0ff;
  border-radius: 3px; }
    .product-media{
    display:block;
  }
  .small_fig{
    display:none;
  }

</style>
@endsection
@section('content')

            <nav aria-label="breadcrumb" class="breadcrumb-nav mb-2">
                <div class="container">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                   
                        <li class="breadcrumb-item active" aria-current="page">Podcasts</li>
                    </ol>
                </div><!-- End .container -->
            </nav><!-- End .breadcrumb-nav -->

            <div class="page-content">
                <div class="container">
                
                <nav class="blog-nav">
                        <ul class="menu-cat entry-filter justify-content-center">
                            <li class="active"><a href="#" data-filter="*">All Podcasts<span>{{count($data)}}</span></a></li>
                            @foreach ($category as $cat)
                                <li><a href="#" data-filter=".{{$cat->slug}}">{{$cat->name}}</a></li>
                            @endforeach
                           
                         
                        </ul><!-- End .blog-menu -->
                    </nav><!-- End .blog-nav -->
                    
                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center">
                        {!! $data->links() !!}
                            <!-- <li class="page-item disabled">
                                <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
                                    <span aria-hidden="true"><i class="icon-long-arrow-left"></i></span>Prev
                                </a>
                            </li>
                            <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item">
                                <a class="page-link page-link-next" href="#" aria-label="Next">
                                    Next <span aria-hidden="true"><i class="icon-long-arrow-right"></i></span>
                                </a>
                            </li> -->
                        </ul>
                    </nav>
                	<div class="entry-container max-col-4" data-layout="fitRows">
                    @foreach ($data as $pod)
                      @if(($loop->index==3) or ($loop->index==8))
                        <div class="entry-item {{$pod->category->slug}} col-xs-6 col-sm-6 col-md-3 col-lg-3">     
                        <div class="product">
                        <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-9404655824796132"
     data-ad-slot="8042091685"
     data-ad-format="auto"
     data-full-width-responsive="true"></ins>
<script>
     (adsbygoogle = window.adsbygoogle || []).push({});
</script>

</div>
                        </div>
                        @endif
                        <div class="entry-item {{$pod->category->slug}} col-xs-6 col-sm-6 col-md-3 col-lg-3">     
                                       
                            <div class="product">
                                <span class="product-label label-sale">{{$pod->category->name}}</span>
                                <figure class="product-media">
                                    <a href="{{ url('podcasts1/view',$pod->slug) }}">
                                        <img src="{{$pod->image}}" alt="Product image" class="product-image">
                                    </a>
                                </figure>
                                <!-- End .product-media -->
                                <div class="product-body">                            
                                    <h3 class="product-title"><a href="{{ url('podcasts1/view',$pod->slug) }}">{{$pod->title}}</a></h3><!-- End .product-title -->
                                    <div class="product-price">
                                        <span class="new-price">{{ date('d M Y', strtotime($pod->created_at)) }}</span>                                        
                                    </div><!-- End .product-price -->
                                    <div class="product-cat" style="  text-align: center;">
                                    
                                        <div class="time-container">
                                            <div class="current-time">
                                                <span class="amplitude-current-minutes" data-amplitude-song-index="{{$loop->index}}"></span>:<span class="amplitude-current-seconds" data-amplitude-song-index="{{$loop->index}}"></span>
                                            </div>
                                        
                                        </div>
                                        <progress class="amplitude-song-played-progress" data-amplitude-song-index="{{$loop->index}}" id="song-played-progress-{{$loop->index}}"></progress>
                                        <div class="control-container">
                                      
                                      
                                          <div class="small_fig">
                                            <a href="product.html">
                                                <img src="{{$pod->image}}" alt="Product image" width="10" height="50">
                                            </a>
                                          </div>
                                    
                                        <div class="amplitude-prev">

                                        </div>
                                        <div class="amplitude-play-pause" data-amplitude-song-index="{{$loop->index}}">

                                        </div>
                                        <div class="amplitude-next">
                                        </div>
                                    
                            </div>
                                    </div><!-- End .product-cat -->
                            </div>
                            </div>
                          
        
                        </div><!-- End .entry-item -->
                    @endforeach
                        
                	</div><!-- End .entry-container -->

                    <nav aria-label="Page navigation">
                        <ul class="pagination justify-content-center">
                        {!! $data->links() !!}
                            <!-- <li class="page-item disabled">
                                <a class="page-link page-link-prev" href="#" aria-label="Previous" tabindex="-1" aria-disabled="true">
                                    <span aria-hidden="true"><i class="icon-long-arrow-left"></i></span>Prev
                                </a>
                            </li>
                            <li class="page-item active" aria-current="page"><a class="page-link" href="#">1</a></li>
                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                            <li class="page-item">
                                <a class="page-link page-link-next" href="#" aria-label="Next">
                                    Next <span aria-hidden="true"><i class="icon-long-arrow-right"></i></span>
                                </a>
                            </li> -->
                        </ul>
                    </nav>
                    
                </div><!-- End .container -->
               
            </div><!-- End .page-content -->


@endsection
@section('js')
        <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/amplitudejs@5.2.0/dist/amplitude.js"></script>
        <script>
            Amplitude.init({
                "songs": <?php echo $data2; ?>
                });
                @foreach ($data as $pod)
                document.getElementById('song-played-progress-{{$loop->index}}').addEventListener('click', function( e ){
                if( Amplitude.getActiveIndex() == 0 ){
                    var offset = this.getBoundingClientRect();
                    var x = e.pageX - offset.left;

                    Amplitude.setSongPlayedPercentage( ( parseFloat( x ) / parseFloat( this.offsetWidth) ) * 100 );
                }
                });
                @endforeach

        </script>                        
        @endsection