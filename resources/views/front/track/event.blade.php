
@extends('layouts.apps')
@section('facebook_meta')
    <meta property="og:title" content="Streetmusic Play Event">
    <meta property="og:image" content="{{$data->photo}}">
    <meta property="og:description" content="Event {{$data->name}} - {{$data->location}}">
@endsection
@section('content')
<section style="margin-top:-700px">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-3 col-lg-offset-3 col-sm-12 col-xs-12">
                    <div class="area-title text-center">
                        <h3>Selamat Datang</h3>
                        <p>Di aplikasi streetmusic play apps, klik link dibawah ini untuk membuka tujuan anda</p>

                        <a href='' class="btn btn-success btn-md" target="_blank">Buka Aplikasi</a>                       
                    </div>
                </div>
            </div>            
        </div>
    </section>            
 @endsection
            
