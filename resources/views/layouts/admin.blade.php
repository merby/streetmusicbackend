<!DOCTYPE html>
<html lang="en">
<head>
    @include('layouts.partials.admin-head')
    @yield('css')
</head>

<body class="light">
<!-- Pre loader -->
<div id="loader" class="loader">
    <div class="plane-container">
        <div class="preloader-wrapper small active">
            <div class="spinner-layer spinner-blue">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-red">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-yellow">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>

            <div class="spinner-layer spinner-green">
                <div class="circle-clipper left">
                    <div class="circle"></div>
                </div><div class="gap-patch">
                <div class="circle"></div>
            </div><div class="circle-clipper right">
                <div class="circle"></div>
            </div>
            </div>
        </div>
    </div>
</div>
<div id="app">
@include('layouts.partials.admin-left-sidebar')

@include('layouts.partials.admin-top')
            <!--Top Menu Start -->
<div class="page has-sidebar-left height-full">
  @include('layouts.partials.admin-breadcrumb')

  {{ Breadcrumbs::render() }}
  <br>
  @yield('content')
</div>
@include('layouts.partials.admin-right-sidebar')

<!-- /.right-sidebar -->
<!-- Add the sidebar's background. This div must be placed
         immediately after the control sidebar -->

<div class="control-sidebar-bg shadow white fixed"></div>
</div>
<!--/#modal -->
@include('layouts.partials.admin-modal')
<!--/#app -->
@include('layouts.partials.admin-plugins')
@yield('js')
</body>
</html>
