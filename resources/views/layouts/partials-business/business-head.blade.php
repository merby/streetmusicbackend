<meta name="description" content="Streetmusic for biz" />
<meta name="author" content="Ricky Merbiansyah" />
<meta name="robots" content="noindex, nofollow" />

<!-- Favicons -->
<link rel="apple-touch-icon" href="biz/assets/img/favicons/apple-touch-icon.png" />
<link rel="icon" href="biz/assets/img/favicons/favicon.ico" />

<!-- Google fonts -->
<!-- <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,900%7CRoboto+Slab:300,400%7CRoboto+Mono:400" /> -->
{!! Html::style(asset('http://fonts.googleapis.com/css?family=Roboto:300,400,400italic,500,900%7CRoboto+Slab:300,400%7CRoboto+Mono:400')) !!}

<!-- Page JS Plugins CSS -->
{!! Html::style(asset('biz/assets/js/plugins/slick/slick.min.css')) !!}

{!! Html::style(asset('biz/assets/js/plugins/slick/slick-theme.min.css')) !!}



<!-- AppUI CSS stylesheets -->

{!! Html::style(asset('biz/assets/css/font-awesome.css')) !!}

{!! Html::style(asset('biz/assets/css/jquery.dataTables.min.css')) !!}

{!! Html::style(asset('biz/assets/css/dropzone.min.css')) !!}

{!! Html::style(asset('biz/assets/css/select2.min.css')) !!}

{!! Html::style(asset('biz/assets/css/select2-bootstrap.css')) !!}

{!! Html::style(asset('biz/assets/css/ionicons.css')) !!}

{!! Html::style(asset('biz/assets/css/bootstrap.css')) !!}

{!! Html::style(asset('biz/assets/css/app.css')) !!}

{!! Html::style(asset('biz/assets/css/app-custom.css')) !!}


<!-- End Stylesheets -->
