<?php
  use App\Models\Business\Business;

  $list = Business::get();
?>

<nav class="drawer-main">
  <ul class="nav nav-drawer">

    <li class="nav-item nav-drawer-header">Menu</li>

    <li class="nav-item active">
      <a href="{{ route('business')}}"><i class="ion-home"></i> Dashboard</a>
    </li>

    <li class="nav-item">
      <a href="{{ route('business.list')}}"><i class="ion-folder"></i> Business</a>
    </li>

    <!-- <li class="nav-item">
    <a href="{{ route('business.create')}}"><i class="ion-folder"></i>Create New Business</a>
    </li> -->

    <!-- <li class="nav-item">
      <a href="{{ route('business.employee')}}"><i class="ion-ios-people"></i> Employee</a>
    </li> -->

    <li class="nav-item">
      <a href="{{ route('business.setting')}}"><i class="ion-android-settings"></i> Settings</a>
    </li>

    <li class="nav-item nav-drawer-header">My Business</li>

    @foreach($list as $li)
    <li class="nav-item">
      <a href="/business/dashboard-business/{{$li->id}}"><i class="ion-folder"></i> {{ucwords($li->name)}}  @if ($li->businessCategories->id === 1) <code  class="pull-right" style="background:#fda085;color:white;">{{$li->businessCategories->name}}</code> @elseif($li->businessCategories->id === 2) <code  class="pull-right" style="background:#00BFFF;color:white;">EO</code> @endif</a>
    </li>
    @endforeach

  </ul>
</nav>
