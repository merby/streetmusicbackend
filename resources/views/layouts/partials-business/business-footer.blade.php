<div class="col-sm-6 copyright sm-text-center">
  <span>
    &copy; 2019 Streetcode</a>
  </span>
</div>

<div class="col-sm-4 col-sm-offset-2 footer-socials mt-mdm-10">
  <div class="social-icons text-right sm-text-center">
    <a href="#"><i class="fa fa-twitter"></i></a>
    <a href="#"><i class="fa fa-facebook"></i></a>
    <a href="#"><i class="fa fa-google-plus"></i></a>
    <a href="#"><i class="fa fa-linkedin"></i></a>
    <a href="#"><i class="fa fa-vimeo"></i></a>
  </div>
</div>
