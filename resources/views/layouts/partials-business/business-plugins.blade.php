<div class="app-ui-mask-modal"></div>

<!-- AppUI Core JS: jQuery, Bootstrap, slimScroll, scrollLock and App.js -->
{!! Html::script(asset('biz/assets/js/core/jquery.min.js')) !!}
{!! Html::script(asset('biz/assets/js/core/bootstrap.min.js')) !!}
{!! Html::script(asset('biz/assets/js/core/jquery.slimscroll.min.js')) !!}
{!! Html::script(asset('biz/assets/js/core/jquery.scrollLock.min.js')) !!}
{!! Html::script(asset('biz/assets/js/core/jquery.placeholder.min.js')) !!}
{!! Html::script(asset('biz/assets/js/app.js')) !!}
{!! Html::script(asset('biz/assets/js/app-custom.js')) !!}
{!! Html::script(asset('biz/assets/js/dropzone.min.js')) !!}
{!! Html::script(asset('biz/assets/js/select2.full.min.js')) !!}
{!! Html::script(asset('biz/assets/js/toast.js')) !!}

<!-- Page Plugins -->
{!! Html::script(asset('biz/assets/js/base_tables_datatables.js')) !!}
{!! Html::script(asset('biz/assets/js/jquery.dataTables.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/slick/slick.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/chartjs/Chart.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/flot/jquery.flot.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/flot/jquery.flot.pie.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/flot/jquery.flot.stack.min.js')) !!}
{!! Html::script(asset('biz/assets/js/plugins/flot/jquery.flot.resize.min.js')) !!}
{!! Html::script(asset('biz/assets/js/base_js_charts_flot.js')) !!}

<!-- Page JS Code -->
{!! Html::script(asset('biz/assets/js/pages/index.js')) !!}
<script>
    $(function()
    {
        // Init page helpers (Slick Slider plugin)
        App.initHelpers('slick');
    });

    @if(Session::has('success'))
      $.toast({
        heading: 'Berhasil',
        text: '{{Session::get("success")}}',
        hideAfter: 12000,
        icon: 'success',
        position: 'top-right',
        stack: false,
        loaderBg: '#f96868'
      })
    @endif

    @if(Session::has('error'))
      $.toast({
        heading: 'Gagal',
        text: '{{Session::get("error")}}',
        hideAfter: 12000,
        icon: 'error',
        position: 'top-right',
        stack: false,
        loaderBg: '#f96868'
      })

    @endif
</script>
