<!-- Required meta tags -->
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Slim">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="http://themepixels.me/slim/img/slim-social.png">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/slim">
    <meta property="og:title" content="Slim">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <link rel="icon" href="assets/favicon.ico" />
    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>StreetCorp Indonesia</title>
    {{-- CSRF Token --}}
        <meta name="csrf-token" content="{{ csrf_token() }}">

    {!! Html::style('css/app.css') !!}
    {!! Html::style(asset('vendor/Datatables/datatables.min.css')) !!}
    {!! Html::style(asset('vendor/select2-4.0.13/dist/css/select2.css')) !!}
    {!! Html::style('css/ribbon.css') !!}
    <style>
        .loader {
            position: fixed;
            left: 0;
            top: 0;
            width: 100%;
            height: 100%;
            background-color: #F5F8FA;
            z-index: 9998;
            text-align: center;
        }

        .plane-container {
            position: absolute;
            top: 50%;
            left: 50%;
        }

        form label.required:after{
          color:red;
          content: ' *'
        }

        .select2-container .select2-selection--single .select2-selection__rendered {
            color: #86939e;
            line-height: 1.42857;
            padding-top: 5px;
            padding-right: 10;
            font-size: 12px;
        }

        .left-side, .main-sidebar {
          /* top: 0;
          left: 0;
          min-height: 100%;
          position: absolute;
          width: 270px; */
          z-index: 999;
          /* background: #fff;
          -webkit-transition: -webkit-transform .3s ease-in-out,width .3s ease-in-out;
          -moz-transition: -moz-transform .3s ease-in-out,width .3s ease-in-out;
          -o-transition: -o-transform .3s ease-in-out,width .3s ease-in-out;
          transition: transform .3s ease-in-out,width .3s ease-in-out; */
      }
    </style>
         <script>
            window.Laravel = {!! json_encode([
                'csrfToken' => csrf_token(),
            ]) !!};
        </script>
