<aside class="main-sidebar fixed offcanvas shadow">
    <section class="sidebar">
        <div class="mt-3 mb-3 ml-3" style="font-size:28px;">
          Street<span style="font-weight:bold;">Corp.</span>
            <!-- <img src="{{ asset('img/sm-dark.png') }}" alt="" width="160"> -->
        </div>
        <div class="relative">
            <a data-toggle="collapse" href="#userSettingsCollapse" role="button" aria-expanded="false"
               aria-controls="userSettingsCollapse" class="btn-fab btn-fab-sm fab-right fab-top btn-primary shadow1 ">
                <i class="icon icon-cogs"></i>
            </a>
            <div class="user-panel p-3 light mb-2">
                <div>
                    <div class="float-left image">
                        <img class="user_avatar" src=" {{ asset('img/dummy/u2.png') }}" alt="User Image">
                    </div>
                    <div class="float-left info">
                        <h6 class="font-weight-light mt-2 mb-1">{{ Auth::user()->name}}</h6>
                        <a href="#"><i class="icon-circle text-success blink"></i> Online</a>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="collapse multi-collapse" id="userSettingsCollapse">
                    <div class="list-group mt-3 shadow">
                        <a href="{{ route('user.show', Auth::user()->id)  }}" class="list-group-item list-group-item-action ">
                            <i class="mr-2 icon-user-secret  text-red"></i>Profile
                        </a>
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-cogs text-blue"></i>Settings</a>
                        <a href="#" class="list-group-item list-group-item-action"><i
                                class="mr-2 icon-security text-purple"></i>Change Password</a>
                        <a href="{{ route('logout') }}" class="list-group-item list-group-item-action"

                            onclick="event.preventDefault(); document.getElementById('frm-logout').submit();">
                            <i
                                class="mr-2 icon-exit_to_app"></i> Logout
                        </a>
                        <form id="frm-logout" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>




                    </div>
                </div>
            </div>
        </div>
        <ul class="sidebar-menu">
            <li class="header"><strong>ADMIN PANEL</strong></li>
            <li class="treeview"><a href="#">
                <i class="icon icon-sailing-boat-water purple-text s-18"></i> Dashboard
                <!-- <i class="icon icon-angle-left s-18 pull-right"></i> -->
            </a>
                <!-- <ul class="treeview-menu">
                    <li><a href="index.html"><i class="icon icon-folder5"></i>Panel Dashboard 1</a>
                    </li>
                    <li><a href="panel-page-dashboard2.html"><i class="icon icon-folder5"></i>Panel Dashboard 2</a>
                    </li>
                    <li><a href="panel-page-dashboard3.html"><i class="icon icon-folder5"></i>Panel Dashboard 3</a>
                    </li>
                    <li><a href="panel-page-dashboard4.html"><i class="icon icon-folder5"></i>Panel Dashboard 4</a>
                    </li>
                    <li><a href="panel-page-dashboard5.html"><i class="icon icon-folder5"></i>Panel Dashboard 5</a>
                    </li>
                </ul> -->
            </li>
            <li class="treeview"><a href="#"><i class="icon icon-account_box light-green-text s-18"></i>Users
                    <i class="icon icon-angle-left s-18 pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="{{ route('users')}}"><i class="icon icon-circle-o"></i>All Users</a>
                    </li>
                    <li><a href="{{ url('roles')}}"><i class="icon icon-circle-o"></i>Roles &amp; Permission</a>
                    <li><a href="{{ url('users/create')}}"><i class="icon icon-add"></i>Add User</a> </li>
                    <!-- <li><a href="panel-page-profile.html"><i class="icon icon-user"></i>User Profile </a> -->
                    </li>
                </ul>
            </li>


            <!-- <li class="treeview no-b"><a href="#">
                <i class="icon icon-package light-green-text s-18"></i>
                <span>Inbox</span>
                <span class="badge r-3 badge-success pull-right">20</span>
            </a>
                <ul class="treeview-menu">
                    <li><a href="panel-page-inbox.html"><i class="icon icon-circle-o"></i>All Messages</a>
                    </li>
                    <li><a href="panel-page-inbox-create.html"><i class="icon icon-add"></i>Compose</a>
                    </li>
                </ul>
            </li> -->
            <li class="header light mt-3"><strong>MENUS</strong></li>

            <li class="treeview ">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> Musisi Platform
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a  href="{{ route('musisi-platform')}}"><i class="icon icon-circle-o"></i>List Musisi</a>
                    </li>
                    <li><a  href="{{ route('musisi-platform.band')}}"><i class="icon icon-circle-o"></i>List Band</a>
                    </li>
                    <li><a  href="{{ route('musisi-platform.track')}}"><i class="icon icon-circle-o"></i>List Track</a>
                    </li>
                    <li><a  href="{{ route('musisi-platform.trackMusician')}}"><i class="icon icon-circle-o"></i>List Track Musician</a>
                    </li>
                    <li><a  href="{{ route('compilation')}}"><i class="icon icon-circle-o"></i>Compilation</a>
                    </li>
                    <li><a  href=""><i class="icon icon-circle-o"></i>Podcast</a>
                    </li>
                    <li><a  href="{{ route('genre')}}"><i class="icon icon-circle-o"></i>Genre</a>
                    </li>

                </ul>
            </li>
            <li class="treeview ">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> News
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a  href="{{ route('news.index')}}"><i class="icon icon-circle-o"></i>List</a>
                    </li>
                    <li><a href="{{ route('news-categories.index')}}"><i class="icon icon-date_range"></i>Category</a>
                    </li>
                </ul>
            </li>
            <li class="treeview ">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> Podcast
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a  href="{{ route('podcast')}}"><i class="icon icon-circle-o"></i>List</a>
                    </li>
                    <li><a href="{{ route('podcast-categories.index')}}"><i class="icon icon-date_range"></i>Category</a>
                    </li>
                </ul>
            <li class="treeview ">
                <a href="#">
                    <i class="icon icon-package text-lime s-18"></i> Business
                    <i class="icon icon-angle-left s-18 pull-right"></i>
                </a>
                <ul class="treeview-menu">

                    <li><a href="{{ route('business-categories.index')}}"><i class="icon icon-date_range"></i>Category</a></li>
                    <li><a href="{{ route('partner.index')}}"><i class="icon icon-date_range"></i>Partner</a>
                    </li>
                    <li><a href="{{ route('list-business.index')}}"><i class="icon icon-date_range"></i>List business</a>
                    </li>
                </ul>

            </li>
            <li class="treeview ">
              <a href="{{route('event')}}">
                <i class="icon icon-package text-lime s-18"></i> Events
              </a>
            </li>
            <li class="treeview ">
              <a href="{{route('cafe')}}">
                <i class="icon icon-package text-lime s-18"></i> Cafe
              </a>
            </li>
            <li class="treeview ">
              <a href="{{route('banner')}}">
                <i class="icon icon-package text-lime s-18"></i> Banner
              </a>
            </li>

        </ul>
    </section>
</aside>
