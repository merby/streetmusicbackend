<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-sm" style="z-index: 1050">
  <div class="modal-dialog modal-sm">
    <div class="modal-content" id="modal-content-sm">

    </div>
  </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true" id="modal-lg" style="z-index: 1050">
  <div class="modal-dialog modal-lg">
    <div class="modal-content" id="modal-content-lg">

    </div>
  </div>
</div>
