{!! Html::script('js/app.js') !!}
<script type="text/javascript">

$('#confirmDelete').on('show.bs.modal', function (e) {
    var message = $(e.relatedTarget).attr('data-message');
    var title = $(e.relatedTarget).attr('data-title');
    var form = $(e.relatedTarget).closest('form');
    $(this).find('.modal-body p').text(message);
    $(this).find('.modal-title').text(title);
    $(this).find('.modal-footer #confirm').data('form', form);
});
$('#confirmDelete').find('.modal-footer #confirm').on('click', function(){
    $(this).data('form').submit();
});

</script>


<!-- ssave -->
<script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>

<script type="text/javascript">

$('#confirmSave').on('show.bs.modal', function (e) {
  var message = $(e.relatedTarget).attr('data-message');
  var title = $(e.relatedTarget).attr('data-title');
  var form = $(e.relatedTarget).closest('form');
  $(this).find('.modal-body p').text(message);
  $(this).find('.modal-title').text(title);
  $(this).find('.modal-footer #confirm').data('form', form);
});
$('#confirmSave').find('.modal-footer #confirm').on('click', function(){
    $(this).data('form').submit();
});
</script>

<!-- tooltips user -->
<script type="text/javascript">
    $(function () {
        var is_touch_device = 'ontouchstart' in document.documentElement;
        if (!is_touch_device) {
            $('[data-toggle="tooltip"]').tooltip();
        }
    });
</script>
{!! Html::script(asset('vendor/Datatables/datatables.min.js')) !!}
{!! Html::script(asset('vendor/select2-4.0.13/dist/js/select2.full.js')) !!}
