<!doctype html>
<html lang="zxx">

<head>
    <!--====== USEFULL META ======-->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Streetmusic is indonesian music ecosystem" />
    <meta name="keywords" content="musisi, streetmusic, Business, music, musik, income" />
    @yield('facebook_meta')

    <!--====== TITLE TAG ======-->
    <title>Streetmusic Indonesia</title>

    <!--====== FAVICON ICON =======-->
    <link rel="shortcut icon" type="image/ico" href="assets/img/favicon.png" />
    @include('layouts.partials-frontend.front-head')
    @yield('css')

    <!--====== STYLESHEETS ======-->
   
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body class="home-three" data-spy="scroll" data-target=".mainmenu-area" data-offset="90">

    <!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--- PRELOADER -->
    <div class="preeloader">
        <div class="preloader-spinner"></div>
    </div>

    <!--SCROLL TO TOP-->
    <a href="#scroolup" class="scrolltotop"><i class="fa fa-long-arrow-up"></i></a>

    <!--START TOP AREA-->
    <header class="top-area" id="home">
    @include('layouts.partials-frontend.front-top')

    </header>
    <!--END TOP AREA-->

    @yield('content') 
        
   	

    @include('layouts.partials-frontend.front-plugins')

    @yield('js')

   
</body>

</html>
