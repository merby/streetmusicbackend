<!DOCTYPE html>

<html class="app-ui">

  <head>
    <!-- Meta -->
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" />

    <!-- Document title -->
    <title>Dashboard | Business</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <style>

    .center-cropped2 {
      object-fit: cover;
      height: 120px;
      width: 100%;
      background-position: center center;
      background-repeat: no-repeat;
      overflow: hidden;
    }

    </style>

    @include('layouts.partials-business.business-head')

  </head>

  <body class="app-ui layout-has-drawer layout-has-fixed-header">
    <div class="app-layout-canvas">
      <div class="app-layout-container">

        <!-- Drawer -->
        <aside class="app-layout-drawer">

          <!-- Drawer scroll area -->
          <div class="app-layout-drawer-scroll">

            <!-- Drawer logo -->
            <div id="logo" class="drawer-header">
              <a href="index.html"><img class="img-responsive" style="width:60%;height:auto;  display: block; margin: 0 auto;margin-top:10px;" src="{{ asset('img/sm-dark.png') }}" title="WEW" alt="AppUI" /></a>
            </div>

            <!-- Drawer navigation -->
            @include('layouts.partials-business.business-top-nav')

            <div class="drawer-footer">
              <p class="copyright">Streetmusic Indonesia &copy;</p>
              <!-- <a href="https://shapebootstrap.net/item/1525731-appui-admin-frontend-template/?ref=rustheme" target="_blank" rel="nofollow">Purchase a license</a> -->
            </div>
          </div>
          <!-- End drawer scroll area -->

        </aside>
        <!-- End drawer -->

        <!-- Header -->
        <header class="app-layout-header">
          @include('layouts.partials-business.business-header')
        </header>
        <!-- End header -->

        <main class="app-layout-content">
          @yield('content')
        </main>

      </div>
          <!-- .app-layout-container -->
    </div>
    <!-- .app-layout-canvas -->
        @include('layouts.partials-business.business-plugins')
    <!-- Apps Modal -->
    <!-- Opens from the button in the header -->
    <div id="apps-modal" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-sm modal-dialog modal-dialog-top">
        <div class="modal-content">
          <!-- Apps card -->
          <div class="card m-b-0">
            <div class="card-header bg-app bg-inverse">
              <h4>Apps</h4>
              <ul class="card-actions">
                <li>
                  <button data-dismiss="modal" type="button"><i class="ion-close"></i></button>
                </li>
              </ul>
            </div>

            <div class="card-block">
              <div class="row text-center">
                <div class="col-xs-6">
                  <a class="card card-block m-b-0 bg-app-secondary bg-inverse" href="index.html">
                    <i class="ion-speedometer fa-4x"></i>
                    <p>Admin</p>
                  </a>
                </div>
                <div class="col-xs-6">
                  <a class="card card-block m-b-0 bg-app-tertiary bg-inverse" href="frontend_home.html">
                    <i class="ion-laptop fa-4x"></i>
                      <p>Frontend</p>
                  </a>
                </div>
              </div>
            </div>
            <!-- .card-block -->
          </div>
          <!-- End Apps card -->
        </div>
      </div>
    </div>
    <!-- End Apps Modal -->


    @yield('script')
  </body>

</html>
