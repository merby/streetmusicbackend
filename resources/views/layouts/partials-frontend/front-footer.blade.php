<div class="footer-bottom-area">
            <div class="container">
                <div class="row">
                    <div class="col-md-4 col-xs-12 sm-center xs-center sm-mb50 xs-mb50">
                        <div class="footer-logo mb50">
                            <a href="#"><img src="{{asset('assets/img/sm-logo-white.png')}}" alt=""></a>
                        </div>
                        <div class="footer-about">
                            <p>Workshop :</p>
                            <p>Bentang Artha Residence Blok D 04 <br>Buah batu, Bandung.</p>
                            <p>Phone :</p>
                            <p>+62 88218413764</p>
                            <p>Email :</p>
                            <p>streetmusic_official@streetmusic.id</p>
                        </div>
                    </div>
                    <div class="col-md-7 col-md-offset-1 col-xs-12">
                        <div class="row">
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-footer-widget">
                                    <h4>Pages</h4>
                                    <ul>
                                        <li><a href="#">How we work</a></li>
                                        <li><a href="#">About us</a></li>
                                        <li><a href="#">Contact us</a></li>
                                        <li><a href="#">Media</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-footer-widget">
                                    <h4>Produk</h4>
                                    <ul>
                                        <li><a href="https://musisi.streetmusic.id">Musisi Platform</a></li>
                                        <li><a href="https://play.streetmusic.id">Streetmusic Play</a></li>
                                        <li><a href="https://streetmusic.id">Streetmusic Business</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                <div class="single-footer-widget">
                                    <h4>Legal</h4>
                                    <ul>
                                        <li><a href="#">Terms of Service</a></li>
                                        <li><a href="#">Security Policy</a></li>
                                        <li><a href="#">Privacy Policy</a></li>
                                        <li><a href="#">Media</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="footer-copyright mt50 center">
                            <p><a target="_blank" href="https://www.templateshub.net">Streetmusic Indonesia &copy; 2019</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>