
    
    {!! Html::script('assets/js/vendor/jquery-1.12.4.min.js') !!}
    {!! Html::script('assets/js/vendor/bootstrap.min.js') !!}
    {!! Html::script('assets/js/vendor/jquery.easing.1.3.js') !!}
    {!! Html::script('assets/js/vendor/jquery-migrate-1.2.1.min.js') !!}

    {!! Html::script('assets/js/vendor/jquery.appear.js') !!}
    {!! Html::script('assets/js/owl.carousel.min.js') !!}
    {!! Html::script('assets/js/swiper.min.js') !!}

    {!! Html::script('assets/js/jquery.bxslider.min.js') !!}
    {!! Html::script('assets/js/jquery.parallax-scroll.js') !!}

    {!! Html::script('assets/js/stellar.js') !!}
    {!! Html::script('assets/js/wow.min.js') !!}


    
    {!! Html::script('assets/js/stellarnav.min.js') !!}
    {!! Html::script('assets/js/placeholdem.min.js') !!}
    {!! Html::script('assets/js/jquery.sticky.js') !!}
    {!! Html::script('assets/js/main.js') !!}
    
    <script>
        $(document).on('ready', function() {
            $('.testimonials-slider .slider').bxSlider({
                // adaptiveHeight: true,
                auto: false,
                controls: true,
                nextText: '<i class="fa fa-long-arrow-right"></i>',
                prevText: '<i class="fa fa-long-arrow-left"></i>',
                mode: 'fade',
                pause: 3000,
                speed: 500,
                pager: true,
                pagerCustom: '#testimonials-slider-pager'
            });
        })

    </script>