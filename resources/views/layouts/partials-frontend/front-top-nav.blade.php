<div class="header-top-area" id="scroolup">
            <!--MAINMENU AREA-->
            <div class="mainmenu-area" id="mainmenu-area">
                <div class="mainmenu-area-bg"></div>
                <nav class="navbar">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="#home" class="navbar-brand"><img src="{{asset('assets/img/sm-logo.png')}}" style="width: 156px;height: 42px;" alt="logo"></a>
                        </div>
                        <div class="mainmenu-and-right-button">
                            <div id="main-nav" class="stellarnav">
                                <ul id="nav" class="nav navbar-nav">
                                    <li class="active"><a href="#home">Home</a></li>
                                    <li><a href="#features">Produk</a></li>
                                    <li><a href="#about">Tentang Kami</a></li>
                                    <li><a href="#contact">Kontak Kami</a></li>
                                    <!-- <li><a href="#pricing">Pricing</a></li> -->
                                    <li><a href="#team">Team</a></li>
                                    <!-- <li><a href="#news">Blog</a></li> -->
                                    <!-- <li><a href="#">Pages</a>
                                        <ul>
                                            <li><a href="blog.html">BLog</a></li>
                                            <li><a href="single-blog.html">Single Blog</a></li>
                                        </ul>
                                    </li> -->
                                </ul>
                            </div>
                            <div class="login-register">
                                <ul>
                                    <li><a href="https://api.whatsapp.com/send?phone=6288218413764&text=Saya%20tertarik%20mengenai%20streetmusic%20nih%20%3F">Hubungi Kami</a></li>
                                </ul>
                                <ul>

                                    <!-- <li><a href="/partnership" style="text-align:right">Become Partner</a></li> -->

                                </ul>
                            </div>

                        </div>
                    </div>
                </nav>
            </div>
            <!--END MAINMENU AREA END-->
        </div>
        <div class="welcome-text-area white">
            <div class="area-bg"></div>
            <div class="welcome-area">
                <div class="container">
                    <div class="row flex-v-center">
                        <div class="col-md-7 col-lg-6 col-sm-12 col-xs-12">
                            <div class="welcome-text">
                                <h1 class="wow fadeInUp" data-wow-delay="0.2s">Menjadi ekosistem musik indonesia yang dapat memutarkan roda perekonomian</h1>
                                <p class="wow fadeInUp" data-wow-delay="0.3s">Streetmusic adalah solusi ekonomi kreatif dibidang musik di negara indonesia, bertujuan untuk memutarkan roda perekonomian dari mulai yang terkecil melalui musik</p>
                                <div class="home-button mt60 xs-mt40 wow fadeInUp" data-wow-delay="0.4s">
                                    <a class="left" href="#">Contact us</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="right-layer wow fadeInRight">
                    <img src="assets/img/home/home-mockup-3.png" alt="">
                </div>
            </div>
        </div>
