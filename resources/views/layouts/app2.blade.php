<!DOCTYPE html>
<html lang="en">


<!-- molla/index-20.html  22 Nov 2019 10:01:15 GMT -->
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Streetmusic</title>
    <meta name="keywords" content="Streetmusic adalah media untuk para penikmat musik dan pecinta musik tanah air, dapatkan pengalaman seru bersama streetmusic dan menjadi lebih dekat dengan musisi tanah air ada podcast, berita dan lainnya">
    <meta name="description" content="Streetmusic adalah platform aplikasi dari indonesia dari musisi untuk musisi">
    <meta name="author" content="p-themes">
    <meta name="image" content="https://streetmusic.id/img/sm-light.png">
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="180x180" href="assets/images/icons/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="assets/images/icons/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="assets/images/icons/favicon-16x16.png">
    <link rel="manifest" href="assets/images/icons/site.html">
    <link rel="mask-icon" href="assets/images/icons/safari-pinned-tab.svg" color="#666666">
    <link rel="shortcut icon" href="assets/images/icons/favicon.ico">
    <meta name="apple-mobile-web-app-title" content="Molla">
    <meta name="application-name" content="Molla">
    <meta name="msapplication-TileColor" content="#cc9966">
    <meta name="msapplication-config" content="assets/images/icons/browserconfig.xml">
    <meta name="theme-color" content="#ffffff">
    @if(isset($news))         
        @foreach ($data as $type)  
      	<meta property="og:type" content="website">
		<meta property="og:title" content="{{$type->title}}">
		<meta property="og:image" content="{{$type->banner_image}}">
		<meta property="og:description" content="{!! str_limit(strip_tags($type->content),100,'...')!!}">
		<meta property="og:url" content="{{Request::url()}}">
		<meta property="fb:app_id" content="961237140600553">    
 		<meta name="title" content="{{$type->title}}">
    	<meta name="image" content="{{$type->banner_image}}">
		<meta name="description" content="Streetmusic adalah platform aplikasi dari indonesia dari musisi untuk musisi">    	
		@endforeach
	@endif

    <meta name="csrf-token" content="{{ csrf_token() }}">
    @include('layouts.partials-frontend.front-head')
    @yield('css')
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
</head>

<body>
    <div class="page-wrapper">
        <header class="header header-12">
            @include('layouts.partials-frontend.front-top-nav')
            
        </header><!-- End .header -->

        <main class="main">
            @yield('content')            
        </main><!-- End .main -->

        <footer class="footer footer-2">
            @include('layouts.partials-frontend.front-footer')        	
        </footer><!-- End .footer -->
    </div><!-- End .page-wrapper -->
    <button id="scroll-top" title="Back to Top"><i class="icon-arrow-up"></i></button>

    <!-- Mobile Menu -->
    
    @include('layouts.partials-frontend.front-mobile')      
    @include('layouts.partials-frontend.front-sign-modal')     
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.4&appId=241110544128";
    fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
    
    @include('layouts.partials-frontend.front-plugins')
    @yield('js')
    <script>
     (adsbygoogle = window.adsbygoogle || []).push({});
    </script>
</body>

