@extends('layouts.admin')
  
@section('content')
<div class="container-fluid my-3">
<form action="{{ route('news.store') }}" method="POST" style="padding:10px;" accept-charset="UTF-8" enctype="multipart/form-data">
<div class="row">
            <div class="col-md-8">
                <div class="card" style="padding:10px;">
                    <div class="card-body b-b no-p">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <strong>Whoops!</strong> There were some problems with your input.<br><br>
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                                        
               
                        @csrf
                        <div class="form-row">
                                        <div class="form-group col-md-9">
                                  

                                            <div class="form-group">
                                                <label for="exampleFormControlInput12">Title</label>
                                                <input type="text" name="title" class="form-control r-0" id="exampleFormControlInput12"
                                            placeholder="Title">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect4">Category</label>
                                                <select name="category_id" class="custom-select my-1 mr-sm-2 form-control r-0 light s-12" id="inlineFormCustomSelectPref">
                                                @foreach ($category as $cat)
                                                    <option value="{{ $cat->id }}">{{ $cat->name }}</option>
                                                @endforeach
                                            </select>
                                            </div>
                                    </div>
                                  
                                    <div class="form-group col-md-3">
                                        <img id="img" src="" style="width:100px;height:100px;"/>
                                        <input type="file" name="image" id="upload" >
                                        
                                    </div>
                               
                                    </div>
                                                <textarea class="form-control editor" name="content" placeholder="Write Something..." rows="17" required>
                                                 
                                                </textarea>
                                        
                      
                  
                 
                    </div>
                   
                </div>
            </div>
            <div class="col-md-4">
            <div class="card" style="padding:10px;">
                    <div class="card-body b-b no-p">
               
                    <div class="form-group">
                        <label for="exampleFormControlInput12">Tags</label>
                        <input type="text" class="tags-input" name="tags"
                            value="Amsterdam,Washington,Sydney,Beijing,Cairo"
                            data-options='{
                            "tagClass":   "badge badge-danger"
                            }' >
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlInput12"> Published </label>
                        <div class="material-switch float-right">
                                        <input id="someSwitchOptionDefault" name="isPublished"  type="checkbox"/>
                                        <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                                    </div>
                    </div>
                                  
                    <hr>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="icon-save mr-2"></i>Save Data</button>
                    
                   
                                  
             
              
                    </div>
        </div>
        </form>      
</div>

@endsection
@section('js')
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>

<script>
$(function(){
  $('#upload').change(function(){
    var input = this;
    var url = $(this).val();
    var ext = url.substring(url.lastIndexOf('.') + 1).toLowerCase();
    if (input.files && input.files[0]&& (ext == "gif" || ext == "png" || ext == "jpeg" || ext == "jpg")) 
     {
        var reader = new FileReader();

        reader.onload = function (e) {
           $('#img').attr('src', e.target.result);
        }
       reader.readAsDataURL(input.files[0]);
    }
    else
    {
      $('#img').attr('src', '/assets/no_preview.png');
    }
  });

});
</script>
@stop