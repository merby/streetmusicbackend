@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-7  offset-md-2">
                <div class="card no-b  no-r">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                 {{ $news->title }}
            </div>
            <hr>
            
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">               
                <img src="{{ $news->banner_image}}" width="300" height="200"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">               
                {{ $news->content }}
            </div>
        </div>
    </div>
    </div>
    <hr>
    <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ route('news-categories.index') }}"> Back</a>
            </div>
</div>
</div>
</div>    
</div>    
@endsection