@extends('layouts/admin')

@section('content')
<style media="screen">
  .btn .icon {
    padding: 0px;
  }
</style>
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
      <div class="row">
    <div class="col-md-12">
      <!-- /.box-header -->
      <div class="card" >
        <div class="card-header">
          <strong>Track List</strong>
          <div class="float-right">          
            <a href="{{route('banner.create')}}" class="btn btn-sm btn-outline-primary" title="Create"><i class="icon icon-plus"></i></a>
            <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload" title="refresh table"><i class="icon icon-refresh"></i></button>
          </div>
        </div>
        <div class="card-body">
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <p>{{ $message }}</p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          <div class="table-responsive">

            <table class="table table-bordered" style="background:white;" id="datatable">
                <thead>
                  <tr>
                      <th width="2" class="text-nowrap">No</th>
                      <th>Title</th>
                      <th>Owner</th>
                      <th>link</th>
                      <th>Album</th>
                      <th width="10%">Genre</th>
                      <th width="10%">Type</th>
                      <th width="10%">Star Count</th>
                      <th width="10%">Playlist Count</th>
                      <th width="5%">Playlist Added</th>                      
                  </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {
  $('#datatable').DataTable({
  serverSide: true,
  processing: true,
  responsive: true,
  ordering: true,
  ajax: "{{route('musisi-platform.dataTrack')}}",
  columns:[
    {data: 'DT_RowIndex', className: 'text-nowrap'},
    {data: 'title'},
    {data: 'owner'},
    {data: 'url',
    render:function(data){
      return '<audio controls preload="none"><source src="horse.ogg" type="audio/ogg"><source src="'+data+'" type="audio/mpeg">Your browser does not support the audio element.</audio>'  
    }},
    {data: 'album'},

    
    // {data: 'photoURL', className: 'text-nowrap', orderable: false, searchable:false,
    //   render: function(data) {
    //     return '<img src="'+data+'" width="100" height="85">';
    //   }
    // },
    {data: 'genre', className: 'text-nowrap',},
    {data: 'type', className: 'text-nowrap',},

    {data: 'starCount', className: 'text-nowrap',},
    {data: 'playlistCount', className: 'text-nowrap',},
    {data: 'playlistAdded', className: 'text-nowrap',},
    // {data: 'job', className: 'text-nowrap',},
    // {data: 'address', className: 'text-nowrap',},
    // // {data: 'author', className: 'text-nowrap',},
    // {data: 'available', className: 'text-nowrap', orderable: false, searchable:false,
    //   render: function(data) {
    //     if (data == 1) {
    //       return '<span class="badge badge-success">Yes</span>';
    //     }

    //     if (data == 0) {
    //       return '<span class="badge badge-danger">No</span>';
    //     }
    //   }
    // },
    // {data: 'additional', className: 'text-nowrap', orderable: false, searchable:false,
    //   render: function(data) {
    //     if (data == 1) {
    //       return '<span class="badge badge-success">Yes</span>';
    //     }

    //     if (data == 0) {
    //       return '<span class="badge badge-danger">No</span>';
    //     }
    //   }
    // },
    // {data: 'position', className: 'text-nowrap',},
    // {data: 'genre', className: 'text-nowrap',},
    // {data: 'gears', className: 'text-nowrap',},
    // {data: 'position', className: 'text-nowrap', orderable: false, searchable:false,
    //   render: function(data) {
    //     if (data == 1) {
    //       return '<span class="badge badge-success">Yes</span>';
    //     }

    //     if (data == 0) {
    //       return '<span class="badge badge-danger">No</span>';
    //     }
    //   }
    // },
    // {data: 'id', className: 'text-nowrap', orderable: false, searchable:false,
    //   render: function(data) {
    //     var btn =
    //       '<div class="btn-group">'
    //       +
    //       '<a href="{{route('banner.show')}}?data='+data+'" class="btn btn-sm btn-outline-primary text-center" title="show"><i class="icon icon-search" ></i></a>'
    //       +
    //       '<a href="{{route('banner.edit')}}?data='+data+'" class="btn btn-sm btn-outline-primary " title="edit"><i class="icon icon-edit"></i></a>'
    //       +
    //       '<button type="button" data-id="'+data+'" class="btn btn-sm btn-outline-primary btn-delete" title="delete"><i class="icon icon-trash"></i></button>'
    //       +
    //       '</div>'
    //       return btn;
    //   }
    // },
  ]
});
});

$(document).on('click', '.btn-reload', function(e){
  e.preventDefault();
  $('#datatable').DataTable().ajax.reload();
});

$(document).on('click', '.btn-delete', function(e){
  e.preventDefault();
  var id = $(this).attr('data-id');
  jQuery('#modal-content-sm').html('');
  setTimeout(function () {
    jQuery.ajax({
      type: 'GET',
      url: '{{route("banner.delete")}}',
      dataType: 'json',
      data: {id:id},
      success: function(data){
        $('#modal-content-sm').html(data);
        $('#modal-sm').modal({ backdrop: 'static' }, 'show');
      }
    });
  }, 100);
});

</script>
@endsection
