@extends('layouts/admin')

@section('content')
<style media="screen">
  .btn .icon {
    padding: 0px;
  }
</style>
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
      <div class="row">
    <div class="col-md-12">
      <!-- /.box-header -->
      <div class="card" >
        <div class="card-header">
          <strong>Podcast List</strong>
          <div class="float-right">
            <a href="{{route('podcast.create')}}" class="btn btn-sm btn-outline-primary" title="Create"><i class="icon icon-plus"></i></a>
            <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload" title="refresh table"><i class="icon icon-refresh"></i></button>
          </div>
        </div>
        <div class="card-body">
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <p>{{ $message }}</p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          <div class="table-responsive">

            <table class="table table-bordered" style="background:white;" id="datatable">
                <thead>
                  <tr>
                      <th width="2" class="text-nowrap">No</th>
                      <th>Title</th>
                      <th width="10%">Image</th>
                      <th width="10%">Category</th>
                      <th width="10%">Author</th>
                      <th width="5%">Is Published</th>
                      <th width="5%">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {
  $('#datatable').DataTable({
  serverSide: true,
  processing: true,
  responsive: true,
  ordering: false,
  ajax: "{{route('podcast.data')}}",
  columns:[
    {data: 'DT_RowIndex', className: 'text-nowrap'},
    {data: 'title'},
    {data: 'image', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        return '<img src="'+data+'" width="100" height="85">';
      }
    },
    {data: 'category', className: 'text-nowrap',},
    {data: 'author', className: 'text-nowrap',},
    {data: 'is_published', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        if (data == 1) {
          return '<span class="badge badge-success">Published</span>';
        }

        if (data == 0) {
          return '<span class="badge badge-danger">Not Published</span>';
        }
      }
    },
    {data: 'id', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        var btn =
          '<div class="btn-group">'
          +
          '<a href="{{route('podcast.show')}}?data='+data+'" class="btn btn-sm btn-outline-primary text-center" title="show"><i class="icon icon-search" ></i></a>'
          +
          '<a href="{{route('podcast.edit')}}?data='+data+'" class="btn btn-sm btn-outline-primary " title="edit"><i class="icon icon-edit"></i></a>'
          +
          '<button type="button" data-id="'+data+'" class="btn btn-sm btn-outline-primary btn-delete" title="delete"><i class="icon icon-trash"></i></button>'
          +
          '</div>'
          return btn;
      }
    },
  ]
});
});

$(document).on('click', '.btn-reload', function(e){
  e.preventDefault();
  $('#datatable').DataTable().ajax.reload();
});

$(document).on('click', '.btn-delete', function(e){
  e.preventDefault();
  var id = $(this).attr('data-id');
  jQuery('#modal-content-sm').html('');
  setTimeout(function () {
    jQuery.ajax({
      type: 'GET',
      url: '{{route("podcast.delete")}}',
      dataType: 'json',
      data: {id:id},
      success: function(data){
        $('#modal-content-sm').html(data);
        $('#modal-sm').modal({ backdrop: 'static' }, 'show');
      }
    });
  }, 100);
});

</script>
@endsection
