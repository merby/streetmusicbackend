@extends('layouts/admin')

@section('content')


    <div class="content-wrapper animatedParent animateOnce">
        <div class="container">
            <section class="paper-card">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="box-header with-border">
                                <h5 class="box-title"> List Business</h5>
                            </div>

                            <!-- /.box-header -->
                            <div class="box-body">

<!-- <div class="text-right"><a class="btn btn-success pull-right " href="{{ route('list-business.create') }}"> Create New</a></div> -->
                            <table class="table table-bordered" style="margin:20px;">

                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Business Category Name</th>
                                    <th>Description</th>
                                    <th>Image</th>
                                    <th width="280px">Action</th>
                                </tr>
                                @foreach ($listbusiness as $data)
                                <tr>
                                    <td>{{ $data->id }}</td>
                                    <td>{{ $data->name }}</td>
                                    <td>{{ $data->businessCategories->name }}</td>
                                    <td>{{ $data->businessCategories->description }}</td>
                                    <td>  <img class="img-avatar img-avatar-128 img-avatar-thumb" src="{{ asset($data->businessCategories->image)}}" alt="" style="width: 128px;"/></td>


                                    <td>
                                        <!-- <form action="{{ route('list-business.destroy',$data->id) }}" method="POST">

                                            <a class="btn btn-info" href="{{ route('list-business.show',$data->id) }}">Show</a>

                                            <a class="btn btn-primary" href="{{ route('list-business.edit',$data->id) }}">Edit</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form> -->
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                     <td colspan="6" align="center">
                                    {!! $listbusiness->links() !!}
                                    </td>
                                </tr>
                            </table>
                            </div>

                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>




@endsection
