@extends('layouts/admin')

@section('content')
<style media="screen">
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<div class="container-fluid animatedParent animateOnce">
  <div class="animated fadeInUpShort">
    <div class="row">
  <div class="col-md-12">
    <!-- /.box-header -->
    <div class="card">
      <div class="card-header">
        <strong>Edit Compilations</strong>
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <form class="" action="{{route('compilation.update')}}" method="post" enctype="multipart/form-data">
          @csrf
          <input type="hidden" name="id" value="{{$data->id}}">
          <div class="row">
          <div class="col-lg-5 col-md-5 col-sm-12">        
          <div class="card">
    <div class="card-header">
      <strong>Basic Information</strong>
    </div>
    <div class="card-body">
            <div class="form-group row">
              <label class="col-form-label col-md-3 col-sm-12 required">Title</label>
              <div class="col-md-9 col-sm-12">
                <input type="text" name="title" id="title" value="{{old('title', $data->title)}}" class="form-control">
              </div>
            </div>
            <div class="form-group row">
              <label class="col-md-3 col-sm-12 col-form-label">Description</label>
              <div class="col-md-9 col-sm-12">
                <textarea name="description" id="description" rows="4" class="form-control">{{old('description', $data->description)}}</textarea>
                <textarea name="tracks" style="display:none;" id="tracks" rows="4" class="form-control">{{old('tracks', $tracks)}}</textarea>    
              </div>
            </div>   
            
            <div class="form-group row">
                <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Published </label>
                <div class="col-md-9 col-sm-12">
                  <div class="material-switch">
                  <input id="someSwitchOptionDefault" name="isPublished"  type="checkbox" {{ $data->is_published == '1' ? 'checked' : '' }}/>
                      <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                  </div>
                </div>
            </div>
            <div class="form-group row">
            <label class="col-md-3 col-sm-12 col-form-label">Description</label>
            <div class="col-md-9 col-sm-12">
            <img id="imageResult" src="{{$data->image}}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
              <div class="fileUpload btn btn-success">
                <span>Browse Image</span>
                <input type="file" name="image" onchange="readURL(this);" class="upload">
              </div>
            </div>
            </div>       
          </div>
          </div>
          </div>
          <div class="col-lg-7 col-md-7 col-sm-12">
          <div class="table-responsive">
                <div class="card" >
                <div class="card-header">
                  <strong>List Song</strong>
                  <div class="float-right">
                    <a href="{{route('compilation.create')}}" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg" title="Create"><i class="icon icon-plus"></i>Choose</a>
                    <!-- <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload2" title="refresh table"><i class="icon icon-refresh"></i></button> -->
                  </div>
                </div>
                <div class="card-body">
               
                <table class="table table-bordered" style="background:white;" id="datatable">
                    <thead>
                      <tr>
                          <th width="2" class="text-nowrap">No</th>
                          <th>Title</th>
                          <th>Singer</th>
                          <th width="10%">Album</th>                                            
                          <th width="5%">Genre</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
                </div>
                </div>
    </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12 text-center">
          <input type="submit" name="submit" value="Save" class="btn btn-outline-primary">
          <a href="{{route('compilation')}}" class="btn btn-outline-secondary">Back</a>
         
        </div>
        </form>
      </div>
    </div>
  </div>
</div>
  </div>
</div>




<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
    <div class="card" >
    <form id="submit_song">
                  <div class="card-header">
                    <strong>List Song</strong>
                    <div class="float-right">
                      <!-- <a href="{{route('compilation.create')}}" class="btn btn-sm btn-outline-primary" data-toggle="modal" data-target=".bd-example-modal-lg" title="Create"><i class="icon icon-plus"></i></a> -->
                      

                      <input type="submit"  class="btn btn-sm btn-success" value="add">
                      <!-- <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload" title="refresh table"><i class="icon icon-refresh"></i></button> -->
                    </div>
                  </div>
                  <div class="card-body">
                 
    <div class="table-responsive">
    
      <table class="table table-bordered" style="background:white;" id="datatable2">
          <thead>
            <tr>
              <th width="0" class="text-nowrap">Id</th>
                <th>Title</th>
                <th>Owner</th>
                <th>Album</th>
                <th width="10%">Genre</th>                 
            </tr>
          </thead>
      </table>
      </form>
      </div>
      </div>
</div>
    </div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')

<script type="text/javascript">
$(document).ready(function() {
  var selected = [];
  var selected_data = [];


  //init
  var init = $('#tracks').val();
  if(init.length>0)
  {
    var data = JSON.parse(init);
    $.each(data, function(i, item) {
        selected.push(item.id);
        selected_data.push(item);
    });
  }
  

  var datab1 = $('#datatable').DataTable({
  serverSide: false,
  dataType: "json",
  processing: true,
  responsive: true,
  ordering: true, 
  "aaData" : selected_data,
  "aoColumns":[
    {mData: 'id' ,className:"text-nowrap"},
    {mDataProp: 'title'},
    {mDataProp: 'owner'},
    {mDataProp: 'album'},
    {mDataProp: 'genre', className: 'text-nowrap',},
  ]
});




  var datab = $('#datatable2').DataTable({
  serverSide: true,
  processing: true,
  responsive: true,
  ordering: true,
  deferRender: true,
  ajax: "{{route('musisi-platform.dataTrack')}}",
  columnDefs: [{
    orderable: false,
    targets: 0,
    'checkboxes': {
            'selectRow': true
         },

  }],
  rowId: 'extn',
  dom: 'Bfrtip',
  select: {
    style: 'multi',
    selector: 'td:first-child'
  },
  'rowCallback': function( row, data ) {   
            if ( $.inArray(data.id, selected) !== -1 ) {
                $(row).addClass('selected');
            }
  },
  columns:[
    { data: 'id' ,className:"text-nowrap"},
    {data: 'title'},
    {data: 'owner'},
    {data: 'album'},
    {data: 'genre', className: 'text-nowrap',},
  ]
});
  $('#datatable2 tbody').on('click', 'tr', function () {
        
      // var customerId = $(this).find("td").eq(1).html();    
    
      //   console.log(this,customerId);
        var id = $(this).find("td").eq(0).html();            
        var title= $(this).find("td").eq(1).html();            
        var owner = $(this).find("td").eq(2).html();            
        var album = $(this).find("td").eq(3).html();            
        var genre = $(this).find("td").eq(4).html();                    

        var arr = {"id":id,"title":title,"owner":owner,"album":album,"genre":genre};
        var index = $.inArray(id, selected);
        if ( index === -1 ) {
            selected.push( id );
            selected_data.push(arr);            
        } else {
            
            selected.splice( index, 1 );
            selected_data.splice(index,1);            
        }
        console.log(selected,selected_data);
        $(this).toggleClass('selected');
    } );


$('#submit_song').on('submit', function(e){
  //Place submit button within the form
  e.preventDefault();

  var form = this;
  datab1.clear();
  datab1.rows.add(JSON.parse(JSON.stringify(selected_data)));
  datab1.draw(); 
  $('.modal').modal('hide');
  $('#tracks').val(JSON.stringify(selected_data));
  console.log(JSON.parse(JSON.stringify(selected_data)));
  // $('#datatable').DataTable().ajax.reload();
  
  
});


});

$(document).on('click', '.btn-reload', function(e){
  e.preventDefault();
  $('#datatable2').DataTable().ajax.reload();
});


$(document).on('click', '.btn-reload2', function(e){
  e.preventDefault();
  $('#datatable').DataTable().ajax.reload();
  console.log(selected_data);
});



</script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});
</script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});
</script>
@endsection
