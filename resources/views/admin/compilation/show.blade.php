@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
        <div class="card">
          <div class="card-header">
            <strong>Detail Genre</strong>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Name</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->name}}">
                  </div>
                </div>     
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Description</label>
                  <div class="col-sm-9">
                    <textarea rows="4" class="form-control" readonly>{{$data->description}}</textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Icon Name</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->icon_name}}">
                  </div>
                </div>                
              <div class="col-md-6 col-sm-12">
                <img id="imageResult" src="{{ asset($data->image) }}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
              </div>
              <div class="col-md-12 col-sm-12 text-center">
                <a href="{{route('compilation')}}" class="btn btn-outline-secondary">Back</a>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
