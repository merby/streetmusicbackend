@extends('layouts/admin')

@section('content')

<div class="container-fluid animatedParent animateOnce">
  <div class="animated fadeInUpShort">
    <div class="row">
  <div class="col-md-12">
    <!-- /.box-header -->
    <div class="card">
      <div class="card-header">
        <strong>Create Genre</strong>
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <form class="" action="{{route('genre.store')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">        
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Name</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-sm-12 col-form-label">Description</label>
                <div class="col-md-9 col-sm-12">
                  <textarea name="description" id="description" rows="4" class="form-control">{{ old('description') }}</textarea>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Icon Name</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="icon_name" id="icon_name" value="{{ old('icon_name') }}" class="form-control">
                </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
              <img id="imageResult" src="{{asset('images/banner-sample.jpeg')}}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
              <div class="fileUpload btn btn-success">
                <span>Browse Image</span>
                <input type="file" name="image" onchange="readURL(this);" class="upload">
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 text-center">
            <input type="submit" name="submit" value="Save" class="btn btn-outline-primary">
            <a href="{{route('genre')}}" class="btn btn-outline-secondary">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});
</script>
@endsection
