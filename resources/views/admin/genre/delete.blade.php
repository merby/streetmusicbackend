<form class="" action="{{route('genre.destroy')}}" method="post">
  @csrf
  <input type="hidden" name="id" value="{{$data->id}}">
  <div class="modal-header">
    <h5 class="modal-title" id="exampleModalLabel">Delete Genre</h5>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    Are you sure you want to delete {{$data->title}}?
  </div>
  <div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
    <button type="submit" class="btn btn-danger btn-sm btn-destroy">Yes, Delete</button>
  </div>
</form>
