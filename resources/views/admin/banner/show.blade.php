@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
        <div class="card">
          <div class="card-header">
            <strong>Detail Banner</strong>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Author</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control"  id="staticEmail" value="{{$data->author->name}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Category</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->category->name}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Title</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->title}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Slug</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->slug}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Description</label>
                  <div class="col-sm-9">
                    <textarea rows="4" class="form-control" readonly>{{$data->description}}</textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Published </label>
                  <div class="col-md-9 col-sm-12">
                    @if($data->is_published == 1)
                      <span class="badge badge-success">Published</span>
                    @else
                      <span class="badge badge-danger">Not Published</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-12">
                <img id="imageResult" src="{{ asset($data->image) }}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
              </div>
              <div class="col-md-12 col-sm-12 text-center">
                <a href="{{route('banner')}}" class="btn btn-outline-secondary">Back</a>
              </div>
            </div>
          </div>
        </div>
    </div>
</div>
@endsection
