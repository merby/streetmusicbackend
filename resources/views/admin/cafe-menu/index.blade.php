@extends('layouts/admin')

@section('content')
<style media="screen">
  .btn .icon {
    padding: 0px;
  }
</style>
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
      <div class="row">
    <div class="col-md-12">
      <!-- /.box-header -->
      <div class="card" >
        <div class="card-header">
          <strong>Menu Cafe "{{$cafe->name}}"</strong>
          <div class="float-right">
            <a href="{{route('cafe.menu.create')}}?cafe={{$cafe->id}}" class="btn btn-sm btn-outline-primary" title="Create"><i class="icon icon-plus"></i></a>
            <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload" title="refresh table"><i class="icon icon-refresh"></i></button>
            <a href="{{route('cafe')}}" class="btn btn-sm btn-outline-primary" title="Back">Back</a>
          </div>
        </div>
        <div class="card-body">
          @if ($message = Session::get('success'))
          <div class="alert alert-success alert-dismissible fade show" role="alert">
              <p>{{ $message }}</p>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          @if ($errors->any())
          <div class="alert alert-danger alert-dismissible fade show" role="alert">
              <ul>
                  @foreach ($errors->all() as $error)
                      <li>{{ $error }}</li>
                  @endforeach
              </ul>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
          </div>
          @endif
          <div class="table-responsive">

            <table class="table table-bordered" style="background:white;" id="datatable">
                <thead>
                  <tr>
                      <th width="2%" class="text-nowrap">No</th>
                      <th width="15%">Name</th>
                      <th width="10%">Price</th>
                      <th width="20%">Description</th>
                      <th width="10%">Image</th>
                      <th width="5%">Is Active</th>
                      <th width="2%">Action</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
<script type="text/javascript">
$(document).ready(function() {
  $('#datatable').DataTable({
  serverSide: true,
  processing: true,
  responsive: true,
  ordering: false,
  ajax: {
      url: "{{route('cafe.menu.data')}}",
      data: function(d) {
        d.cafe_id = '{{request()->get("cafe")}}';
      }
    },
  columns:[
    {data: 'DT_RowIndex', className: 'text-nowrap'},
    {data: 'name'},
    {data: 'price',},
    {data: 'description'},
    {data: 'image', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        return '<img src="'+data+'" width="100" height="85">';
      }
    },
    {data: 'is_active', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        if (data == 1) {
          return '<span class="badge badge-success">Active</span>';
        }

        if (data == 0) {
          return '<span class="badge badge-danger">Not active</span>';
        }
      }
    },
    {data: 'id', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data, type, full, meta) {
        var btn =
          '<div class="btn-group">'
          +
          '<a href="{{route('cafe.menu.edit')}}?data='+data+'&cafe='+full.cafe_id+'" class="btn btn-sm btn-outline-primary " title="edit"><i class="icon icon-edit"></i></a>'
          +
          '<button type="button" data-id="'+data+'" class="btn btn-sm btn-outline-primary btn-delete" title="delete"><i class="icon icon-trash"></i></button>'
          +
          '</div>'
          return btn;
      }
    },
  ]
});
});

$(document).on('click', '.btn-reload', function(e){
  e.preventDefault();
  $('#datatable').DataTable().ajax.reload();
});

$(document).on('click', '.btn-delete', function(e){
  e.preventDefault();
  var id = $(this).attr('data-id');
  jQuery('#modal-content-sm').html('');
  setTimeout(function () {
    jQuery.ajax({
      type: 'GET',
      url: '{{route("cafe.menu.delete")}}',
      dataType: 'json',
      data: {id:id},
      success: function(data){
        $('#modal-content-sm').html(data);
        $('#modal-sm').modal({ backdrop: 'static' }, 'show');
      }
    });
  }, 100);
});

</script>
@endsection
