@extends('layouts/admin')

@section('content')


    <div class="content-wrapper animatedParent animateOnce">
        <div class="container">
            <section class="paper-card">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="box-header with-border">
                                <h5 class="box-title">Business Category List</h5>
                            </div>

                            <!-- /.box-header -->
                            <div class="box-body">

<div class="text-right"><a class="btn btn-success pull-right " href="{{ route('business-categories.create') }}"> Create New Category</a></div>
                            <table class="table table-bordered" style="margin:20px;">

                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Details</th>
                                    <th>Images</th>
                                    <th width="280px">Action</th>
                                </tr>
                                @foreach ($businessCategories as $type)
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->description }}</td>
                                    <td>  <img class="img-avatar img-avatar-128 img-avatar-thumb" src="{{ asset($type->image)}}" alt="" style="width: 128px;"/></td>

                                    <td>
                                        <form action="{{ route('business-categories.destroy',$type->id) }}" method="POST">

                                            <a class="btn btn-info" href="{{ route('business-categories.show',$type->id) }}">Show</a>

                                            <a class="btn btn-primary" href="{{ route('business-categories.edit',$type->id) }}">Edit</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                     <td colspan="4" align="center">
                                    {!! $businessCategories->links() !!}
                                    </td>
                                </tr>
                            </table>
                            </div>

                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>




@endsection
