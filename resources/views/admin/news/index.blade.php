@extends('layouts/admin')

@section('content')
<div class="container-fluid my-2">
<div class="row">
            <div class="col-md-12">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif

                            <!-- /.box-header -->
                           


                            <table class="table table-bordered" style="background:white;">
                                <tr>
                                    <td colspan="6">
                                        <h4> News List </h4>
                                    </td>
                                    <td style="border-left:0px;">
                                         <a class="btn btn-success pull-right" href="{{ route('news.create') }}"> Create News</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>No</th>
                                    <th>Title</th>
                                    <th>Slug</th>
                                    <th>Category</th>
                                   
                                    <th>isPublished</th>
                                    <th>Author</th>
                                    <th width="280px">Action</th>
                                </tr>
                                @foreach ($news as $type)
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->title }}</td>
                                    <td>{{ $type->slug }}</td>
                                    <td>{{ $type->category->name }}</td>
                                    <td>
                                        @if( $type->is_published == '1' )
                                            <span class="badge badge-success">Published</span>
                                        @else
                                            <span class="badge badge-primary">Not Published</span>
                                        @endif

                                    </td>
                                    <td>{{ $type->user->name }}</td>
                                    <td>
                                        <form action="{{ route('news.destroy',$type->id) }}" method="POST">

                                            <a class="btn btn-info" href="{{ route('news.show',$type->id) }}">Show</a>

                                            <a class="btn btn-primary" href="{{ route('news.edit',$type->id) }}">Edit</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                     <td colspan="7" >
                                    {!! $news->links() !!}
                                    </td>
                                </tr>
                            </table>
                            </div>
                            </div>
                            </div>
                        <!-- /.box -->
            




@endsection
