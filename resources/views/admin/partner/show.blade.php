@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-7  offset-md-2">
                <div class="card no-b  no-r">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5> View detail</h5>
            </div>
            <hr>

        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $partner->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email:</strong>
                {{ $partner->email }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>IsActivated :</strong>
                {{ $partner->isActivated }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Email Verification :</strong>
                {{ $partner->emailVerification }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Password :</strong>
                {{ $partner->password }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Npwp:</strong>
                {{ $partner->UserDetail->npwp }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>NIK:</strong>
                {{ $partner->UserDetail->nik }}
            </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Foto Usaha:</strong>
                 <img class="img-avatar img-avatar-128 img-avatar-thumb" src="{{ asset($partner->UserDetail->foto_usaha)}}" alt="" style="width: 128px;"/>
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Foto Selfie:</strong>
              <img class="img-avatar img-avatar-128 img-avatar-thumb" src="{{ asset($partner->UserDetail->foto_selfie)}}" alt="" style="width: 128px;"/>
            </div>
        </div>



    </div>
    </div>
    <hr>
    <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ route('partner.index') }}"> Back</a>
            </div>
</div>
</div>
</div>
</div>
@endsection
