@extends('layouts/admin')

@section('content')


    <div class="content-wrapper animatedParent animateOnce">
        <div class="container">
            <section class="paper-card">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="box">

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="box-header with-border">
                                <h5 class="box-title">Partner List</h5>
                            </div>

                            <!-- /.box-header -->
                            <div class="box-body">

<div class="text-right"><a class="btn btn-success pull-right " href="{{ route('partner.create') }}"> Create New Partner</a></div>
                            <table class="table table-bordered" style="margin:20px;">
                              <?php $no=1; ?>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>IsActivated</th>
                                    <th>Email Verification</th>
                                    <th width="280px">Action</th>
                                </tr>
                                @foreach ($partner as $listPartner)
                                <tr>
                                    <td>{{ $no++ }}</td>
                                    <td>{{ $listPartner->name }}</td>
                                    <td>{{ $listPartner->email }}</td>
                                    <td>{{ $listPartner->isActivated }}</td>
                                    <td>{{ $listPartner->emailVerification }}</td>

                                    <td>
                                        <form action="{{ route('partner.destroy',$listPartner->id) }}" method="POST">

                                            <a class="btn btn-info" href="{{ route('partner.show',$listPartner->id) }}">Show</a>

                                            <a class="btn btn-primary" href="{{ route('partner.edit',$listPartner->id) }}">Edit</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                     <td colspan="4" align="center">
                                    {!! $partner->links() !!}
                                    </td>
                                </tr>
                            </table>
                            </div>

                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </div>




@endsection
