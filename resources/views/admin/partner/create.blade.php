@extends('layouts.admin')

@section('content')

<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-8  offset-md-2">
                <div class="card no-b  no-r">

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('partner.store') }}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="card no-b  no-r">
    <div class="card-body">
        <h5 class="card-title">Create Partner</h5>
        <hr>
        <div class="form-row">
            <div class="col-md-8">
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NAME</label>
                    <input  placeholder="Full Name" id="name" type="text" class="form-control r-0 light s-12 {{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                    @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">Email</label>
                    <input placeholder="Email address" id="email" type="email" class="form-control r-0 light s-12 {{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autocomplete="email">

                    @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">Password</label>
                    <input placeholder="Password" id="password" type="password" class="form-control r-0 light s-12 {{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required autocomplete="new-password">

                    @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">Reply Password</label>
                      <input placeholder=" Confirm Password" id="password-confirm" type="password" class="form-control r-0 light s-12" name="password_confirmation" required autocomplete="new-password">
                </div>

                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NIK</label>
                    <input  placeholder="NIK" id="nik" type="text" class="form-control r-0 light s-12 {{ $errors->has('nik') ? ' is-invalid' : '' }}" name="nik" value="{{ old('nik') }}" required autocomplete="name" autofocus>

                    @if ($errors->has('nik'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('nik') }}</strong>
                        </span>
                    @endif
                </div>

                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NPWP</label>
                    <input  placeholder="NPWP" id="npwp" type="text" class="form-control r-0 light s-12 {{ $errors->has('npwp') ? ' is-invalid' : '' }}" name="npwp" value="{{ old('npwp') }}" required autocomplete="name" autofocus>

                    @if ($errors->has('npwp'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('npwp') }}</strong>
                        </span>
                    @endif
                </div>




                <div class="form-group m-0">
                    <label for="image" class="col-form-label s-12">Foto Selfie</label>
                    <input name="foto_selfie" id="foto_selfie"  class="form-control r-0 light s-12 " type="file">
                    @if ($errors->has('foto_selfie'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('foto_selfie') }}</strong>
                        </span>
                    @endif
                  </div>

                <div class="form-group m-0">
                    <label for="image" class="col-form-label s-12">Foto Usaha</label>
                    <input name="foto_usaha" id="foto_usaha"  class="form-control r-0 light s-12 " type="file">
                    @if ($errors->has('foto_usaha'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('foto_usaha') }}</strong>
                        </span>
                    @endif

                </div>

            </div>
        </div>
    </div>

    <hr>
    <div class="card-body">
        <button type="submit" class="btn btn-primary btn-sm"><i class="icon-save mr-2"></i>Save Data</button>
    </div>
</div>
</form>
</div>
</div>
</div>
</div>
@endsection
