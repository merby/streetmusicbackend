@extends('layouts.admin')

@section('content')

<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-7  offset-md-2">
                <div class="card no-b  no-r">

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('partner.update',$partner->id) }}" method="POST" enctype="multipart/form-data">
    @csrf
    @method('PUT')
    <div class="card no-b  no-r">
    <div class="card-body">
        <h5 class="card-title">Edit Partner</h5>
        <hr>
        <div class="form-row">
            <div class="col-md-8">
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NAME</label>
                    <input name="name" id="name" value="{{ $partner->name }}" placeholder="Enter Category Name" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">Email</label>
                    <input name="email" id="name"  value="{{ $partner->email }}" placeholder="Enter Email" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NIK</label>
                    <input name="nik" id="name"  value="{{ $partner->UserDetail->nik }}" placeholder="Enter NIK" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">NPWP</label>
                    <input name="npwp" id="name"  value="{{ $partner->UserDetail->npwp }}" placeholder="Enter Description" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">isActivated</label>
                    <input name="isActivated" id="name"  value="{{ $partner->isActivated }}" placeholder="Enter Description" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">Email Verification</label>
                    <input name="emailVerification" id="name"  value="{{ $partner->emailVerification }}" placeholder="Enter Description" class="form-control r-0 light s-12 " type="text">
                </div>
                <div class="form-group m-0">
                  <label for="name" class="col-form-label s-12">Foto Usaha</label>
                  <img class="img-thumbnail" src="{{ asset($partner->UserDetail->foto_usaha) }}" alt="" style="width: 200px;" />
                  <input name="foto_usaha" id="name"  value="{{ $partner->UserDetail->foto_usaha }}" class="form-control r-0 light s-12 " type="file">
                </div>
                <input value="{{$partner->UserDetail->foto_usaha}}" type="hidden" name="oldImage" />
              

                <div class="form-group m-0">
                  <label for="name" class="col-form-label s-12">Foto Selfie</label>
                  <img class="img-thumbnail" src="{{ asset($partner->UserDetail->foto_selfie) }}" alt="" style="width: 200px;" />
                  <input name="foto_selfie" id="name"  value="{{ $partner->image }}" class="form-control r-0 light s-12 " type="file">
                </div>
                <input value="{{$partner->UserDetail->foto_selfie}}" type="hidden" name="oldImage" />
                <input value="{{$partner->id}}" type="hidden" name="id" />
            </div>
        </div>
    </div>

    <hr>
    <div class="card-body">
        <button type="submit" class="btn btn-primary btn-sm"><i class="icon-save mr-2"></i>Save Data</button>
    </div>
</div>
</form>
</div>
</div>
</div>
</div>
@endsection

<!--

<form action="#">
                        <div class="card no-b  no-r">
                            <div class="card-body">
                                <h5 class="card-title">About User</h5>
                                <div class="form-row">
                                    <div class="col-md-8">
                                        <div class="form-group m-0">
                                            <label for="name" class="col-form-label s-12">STUDENT NAME</label>
                                            <input id="name" placeholder="Enter User Name" class="form-control r-0 light s-12 " type="text">
                                        </div>

                                        <div class="form-row">
                                            <div class="form-group col-6 m-0">
                                                <label for="cnic" class="col-form-label s-12"><i class="icon-fingerprint"></i>CNIC / FORM B</label>
                                                <input id="cnic" placeholder="Enter Form B or CNIC Number" class="form-control r-0 light s-12 date-picker" type="text">
                                            </div>
                                            <div class="form-group col-6 m-0">
                                                <label for="dob" class="col-form-label s-12"><i class="icon-calendar mr-2"></i>DATE OF BIRTH</label>
                                                <input id="dob" placeholder="Select Date of Birth" class="form-control r-0 light s-12 datePicker" data-time-picker="false"
                                                       data-format-date='Y/m/d' type="text">
                                            </div>
                                        </div>

                                        <div class="form-group m-0">
                                            <label for="dob" class="col-form-label s-12">GENDER</label>
                                            <br>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="male" name="gender" class="custom-control-input">
                                                <label class="custom-control-label m-0" for="male">Male</label>
                                            </div>
                                            <div class="custom-control custom-radio custom-control-inline">
                                                <input type="radio" id="female" name="gender" class="custom-control-input">
                                                <label class="custom-control-label m-0" for="female">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3 offset-md-1">
                                        <input hidden id="file" name="file"/>
                                        <div class="dropzone dropzone-file-area pt-4 pb-4" id="fileUpload">
                                            <div class="dz-default dz-message">
                                                <span>Drop A passport size image of user</span>
                                                <div>You can also click to open file browser</div>
                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <div class="form-row mt-1">
                                    <div class="form-group col-4 m-0">
                                        <label for="email" class="col-form-label s-12"><i class="icon-envelope-o mr-2"></i>Email</label>
                                        <input id="email" placeholder="user@email.com" class="form-control r-0 light s-12 " type="text">
                                    </div>

                                    <div class="form-group col-4 m-0">
                                        <label for="phone" class="col-form-label s-12"><i class="icon-phone mr-2"></i>Phone</label>
                                        <input id="phone" placeholder="05112345678" class="form-control r-0 light s-12 " type="text">
                                    </div>
                                    <div class="form-group col-4 m-0">
                                        <label for="mobile" class="col-form-label s-12"><i class="icon-mobile-phone mr-2"></i>Mobile</label>
                                        <input id="mobile" placeholder="eg: 3334709643" class="form-control r-0 light s-12 " type="text">
                                    </div>

                                </div>
                                <div class="form-row">
                                    <div class="form-group col-9 m-0">
                                        <label for="address"  class="col-form-label s-12">Address</label>
                                        <input type="text" class="form-control r-0 light s-12" id="address"
                                               placeholder="Enter Address">
                                    </div>

                                    <div class="form-group col-3 m-0">
                                        <label for="inputCity" class="col-form-label s-12">City</label>
                                        <input type="text" class="form-control r-0 light s-12" id="inputCity">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                <h5 class="card-title">ENROLLMENT</h5>
                                <div class="form-row">
                                    <div class="form-group col-5 m-0">
                                        <label for="roll1" class="col-form-label s-12"># ID NUMBER</label>
                                        <input id="roll1" placeholder="Enter ID Number" class="form-control r-0 light s-12 " type="text">
                                    </div>
                                    <div class="form-group col m-0">
                                        <label for="roll2" class="col-form-label s-12">CLASS</label>
                                        <input id="roll2" placeholder="Select Class" class="form-control r-0 light s-12 " type="text">
                                    </div>
                                    <div class="form-group col m-0">
                                        <label for="roll4" class="col-form-label s-12">SECTION</label>
                                        <input id="roll4" placeholder="Select Class" class="form-control r-0 light s-12 " type="text">
                                    </div>
                                </div>

                                <div class="form-row">
                                    <div class="form-group col-4 m-0">
                                        <label for="joining" class="col-form-label s-12"><i class="icon-calendar mr-2"></i>DATE OF JOINING</label>
                                        <input id="joining" placeholder="user@email.com" class="form-control r-0 light s-12 datePicker" data-time-picker="false"
                                               data-format-date='Y/m/d' type="text">
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="card-body">
                                <h5 class="card-title">PARENT / GUARDIAN</h5>
                                <div class="form-row">
                                    <div class="form-group col-5 m-0">
                                        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Select A parent</label>
                                        <select class="custom-select my-1 mr-sm-2 form-control r-0 light s-12" id="inlineFormCustomSelectPref">
                                            <option selected>Choose...</option>
                                            <option value="1">One</option>
                                            <option value="2">Two</option>
                                            <option value="3">Three</option>
                                        </select>
                                    </div>
                                </div>
                                <a href="#" class="btn btn-primary bg-primary btn-sm mt-2">Add New Guardian</a>
                            </div>
                            <hr>
                            <div class="card-body">
                                <button type="submit" class="btn btn-primary btn-lg"><i class="icon-save mr-2"></i>Save Data</button>
                            </div>
                        </div>
                    </form> -->
