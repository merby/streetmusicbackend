@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
      <div class="row">
        <div class="col-md-12 col-sm-12" style="margin-bottom:10px">
          <div class="card">
            <div class="card-header">
              <strong>Detail Cafe</strong>
              <div class="float-right">
                <a href="{{route('cafe')}}" class="btn btn-sm btn-outline-primary">Back</a>
              </div>
            </div>
            <div class="card-body">
              <div class="row">
                <div class="col-md-6 col-sm-12">
                  <img id="imageResult" src="{{ asset($data->logo) }}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block mb-2" style="max-height:200px;min-height:200px">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Owner</label>
                    <div class="col-sm-9">
                      <input type="text" readonly class="form-control"  id="staticEmail" value="{{$data->owner->name}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Name</label>
                    <div class="col-sm-9">
                      <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->name}}">
                    </div>
                  </div>
                </div>
                <div class="col-md-6 col-sm-12">
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Phone</label>
                    <div class="col-sm-9">
                      <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->phone}}">
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Address</label>
                    <div class="col-sm-9">
                      <textarea rows="3" class="form-control" readonly>{{$data->address}}</textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Description</label>
                    <div class="col-sm-9">
                      <textarea rows="3" class="form-control" readonly>{{$data->description}}</textarea>
                    </div>
                  </div>
                  <div class="form-group row">
                    <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Active </label>
                    <div class="col-md-9 col-sm-12">
                      @if($data->is_active == 1)
                        <span class="badge badge-success">ACtive</span>
                      @else
                        <span class="badge badge-danger">Not Active</span>
                      @endif
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12" style="margin-bottom:10px">
          <div class="card ">
            <div class="card-header">
              <strong>Cafe Image</strong>
            </div>
            <div class="card-body">
              <div class="row">
                @foreach($data->cafe_image as $ci)
                <div class="col-md-3 col-sm-6">
                  <img  src="{{ asset($ci->image) }}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block mb-2">
                </div>
                @endforeach
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 col-sm-12">
          <div class="card ">
            <div class="card-header">
              <strong>Cafe Menu</strong>
            </div>
            <div class="card-body">
              @foreach($data->cafe_menu as $cm)
              <div class="col-md-12 col-sm-12">
                <div class="card">
                  <div class="card-body">
                    <div class="ribbon-wrapper">
                        @if($cm->is_active == 1)
                        <div class="ribbon green">Active</div>
                        @else
                        <div class="ribbon red" style="height:35px">Not <br> Active</div>
                        @endif
                    </div>
                    <div class="row">
                      <div class="col-md-3 col-sm-12 text-center">
                        <img src="{{$cm->image}}" class="img-fluid col-md-10">
                      </div>
                      <div class="col-md-9 col-sm-12">
                        <div class="form-group">
                          <input type="text" name="text" class="form-control form-control-sm col-md-10" readonly value="{{$cm->name}}">
                        </div>
                        <div class="form-group">
                          <input type="text" name="text" class="form-control form-control-sm" readonly value="{{$cm->price}}">
                        </div>
                        <div class="form-group">
                          <textarea name="name" rows="3" class="form-control form-control-sm" readonly>{{$cm->description}}</textarea>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              @endforeach
            </div>
          </div>
        </div>
      </div>
    </div>
</div>
@endsection
