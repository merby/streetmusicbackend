@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
    <div class="animated fadeInUpShort">
        <div class="card">
          <div class="card-header">
            <strong>Detail Event</strong>
          </div>
          <div class="card-body">
            <div class="row">
              <div class="col-md-6 col-sm-12">                
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Type</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->type}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Name</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->name}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Slug</label>
                  <div class="col-sm-9">
                    <input type="text" readonly class="form-control" id="staticEmail" value="{{$data->slug}}">
                  </div>
                </div>
                <div class="form-group row">
                  <label for="staticEmail" class="col-sm-12 col-md-3 col-form-label">Description</label>
                  <div class="col-sm-9">
                    <textarea rows="4" class="form-control" readonly>{{$data->description}}</textarea>
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Open For Band </label>
                  <div class="col-md-9 col-sm-12">
                    @if($data->open_band == 1)
                      <span class="badge badge-success">Yes</span>
                    @else
                      <span class="badge badge-danger">No</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Ticket Distribution </label>
                  <div class="col-md-9 col-sm-12">
                    @if($data->ticket_distribution == 1)
                      <span class="badge badge-success">Yes</span>
                    @else
                      <span class="badge badge-danger">No</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Published </label>
                  <div class="col-md-9 col-sm-12">
                    @if($data->is_published == 1)
                      <span class="badge badge-success">Published</span>
                    @else
                      <span class="badge badge-danger">Not Published</span>
                    @endif
                  </div>
                </div>
                <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required">Event banner </label>
                  <div class="col-md-9 col-sm-12">
                  <img id="imageResult" src="{{ asset($data->photo) }}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
                  </div>
                </div>
                

              </div>
              <div class="col-md-6 col-sm-12">
              <table class="table table-bordered" style="background:white;" id="datatable">
                    <thead>
                      <tr>
                          <th width="2" class="text-nowrap">No</th>
                          <th>Title</th>
                          <th>Singer</th>
                          <th width="10%">Album</th>                                            
                          <th width="5%">Genre</th>
                      </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
              </div>
              <div class="col-md-12 col-sm-12 text-center">
                <a href="{{route('event')}}" class="btn btn-outline-secondary">Back</a>
              </div>
            </div>
          </div>
        </div>
        
    </div>


</div>
@endsection
