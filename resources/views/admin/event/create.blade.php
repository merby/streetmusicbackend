@extends('layouts/admin')

@section('content')
<style media="screen">
.fileUpload {
    position: relative;
    overflow: hidden;
    margin: 10px;
}
.fileUpload input.upload {
    position: absolute;
    top: 0;
    right: 0;
    margin: 0;
    padding: 0;
    font-size: 20px;
    cursor: pointer;
    opacity: 0;
    filter: alpha(opacity=0);
}
</style>
<div class="container-fluid animatedParent animateOnce">
  <div class="animated fadeInUpShort">
    <div class="row">
  <div class="col-md-12">
    <!-- /.box-header -->
    <div class="card">
      <div class="card-header">
        <strong>Create Event</strong>
      </div>
      <div class="card-body">
        @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif
        <form class="" action="{{route('event.store')}}" method="post" enctype="multipart/form-data">
          @csrf
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Category</label>
                <div class="col-md-9 col-sm-12">
                  <select class="form-control" name="type" id="type">
                    <option value="">Select Type Event</option>
                    <option value="Personal">Personal</option>
                    <option value="Professional">Professional</option>
                  </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Nama Event</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="name" id="name" value="{{ old('name') }}" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-sm-12 col-form-label">Description</label>
                <div class="col-md-9 col-sm-12">
                  <textarea name="description" id="description" rows="4" class="form-control">{{ old('description') }}</textarea>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-md-3 col-sm-12 col-form-label required">Kota/Kabupaten</label>
                <div class="col-md-9 col-sm-12">
                <select name="city" class="custom-select my-1 mr-sm-2 form-control r-0 light s-12" id="inlineFormCustomSelectPref">
                        @foreach ($cities as $city)
                            <option value="{{ $city }}">{{ $city }}</option>
                        @endforeach
                    </select>
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Location</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="location" id="location" value="{{ old('location') }}" class="form-control">
                </div>
              </div>
              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Coordinate Location</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="coordinate" value="{{ old('coordinate') }}" class="form-control">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Tanggal mulai (YYYY-MM-DD)</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="start_date" id="start_date" value="{{ old('start_date') }}" class="form-control">
                </div>
              </div>

              <div class="form-group row">
                <label class="col-form-label col-md-3 col-sm-12 required">Tanggal Selesai (YYYY-MM-DD)</label>
                <div class="col-md-9 col-sm-12">
                  <input type="text" name="end_date" id="end_date" value="{{ old('end_date') }}" class="form-control">
                </div>
              </div>

              
              
              <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Open Slot Band </label>
                  <div class="col-md-9 col-sm-12">
                    <div class="material-switch">
                        <input id="openband" name="open_band"  type="checkbox"/>
                        <label for="openband" class="bg-secondary"></label>
                    </div>
                  </div>
              </div>
              <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Open Ticket Distribution </label>
                  <div class="col-md-9 col-sm-12">
                    <div class="material-switch">
                        <input id="openticket" name="ticket_distribution"  type="checkbox"/>
                        <label for="openticket" class="bg-secondary"></label>
                    </div>
                  </div>
              </div>
              <div class="form-group row">
                  <label for="exampleFormControlInput12" class="col-md-3 col-sm-12 required"> Published </label>
                  <div class="col-md-9 col-sm-12">
                    <div class="material-switch">
                        <input id="someSwitchOptionDefault" name="isPublished"  type="checkbox"/>
                        <label for="someSwitchOptionDefault" class="bg-secondary"></label>
                    </div>
                  </div>
              </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12 text-center">
              <img id="imageResult" src="{{asset('images/banner-sample.jpeg')}}" alt="" class="img-fluid rounded shadow-sm mx-auto d-block" style="max-height:200px;min-height:200px">
              <div class="fileUpload btn btn-success">
                <span>Browse Image</span>
                <input type="file" name="image" onchange="readURL(this);" class="upload">
              </div>
            </div>
          </div>
          <div class="col-md-12 col-sm-12 text-center">
            <input type="submit" name="submit" value="Save" class="btn btn-outline-primary">
            <a href="{{route('event')}}" class="btn btn-outline-secondary">Back</a>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
 <script type="text/javascript">
            $(function () {
                $('#datetimepicker1').datetimepicker();
            });
        </script>
<script type="text/javascript">
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imageResult')
                .attr('src', e.target.result);
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$(function () {
    $('#upload').on('change', function () {
        readURL(input);
    });
});
</script>
@endsection
