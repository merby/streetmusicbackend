@extends('layouts/admin')

@section('content')

<div class="container-fluid my-2">
<div class="row">
            <div class="col-md-12">
          

                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                 
                            <table class="table table-bordered" style="background:white;">
                                <tr>
                                    <td colspan="3">
                                       <h4> Podcasts Category </h4>
                                    </td>
                                    <td>
                                    <a class="btn btn-success pull-right" href="{{ route('podcast-categories.create') }}"> Create  Category</a>
                                    </td>
                                </tr>
                                <tr>
                                    <th>No</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th width="280px">Action</th>
                                </tr>
                                @foreach ($NewsCategories as $type)
                                <tr>
                                    <td>{{ $type->id }}</td>
                                    <td>{{ $type->name }}</td>
                                    <td>{{ $type->slug }}</td>
                                    <td>
                                        <form action="{{ route('podcast-categories.destroy',$type->id) }}" method="POST">

                                            <a class="btn btn-info" href="{{ route('podcast-categories.show',$type->id) }}">Show</a>

                                            <a class="btn btn-primary" href="{{ route('podcast-categories.edit',$type->id) }}">Edit</a>

                                            @csrf
                                            @method('DELETE')

                                            <button type="submit" class="btn btn-danger">Delete</button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                                <tr>
                                     <td colspan="4" align="center">
                                    {!! $NewsCategories->links() !!}
                                    </td>
                                </tr>
                            </table>
             
        </div>
    </div>

</div>


@endsection
