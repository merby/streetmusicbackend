@extends('layouts.admin')
@section('content')
<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-7  offset-md-2">
                <div class="card no-b  no-r">

    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h5> View detail</h5>
            </div>
            <hr>
            
        </div>
    </div>
   
    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $podcastCategory->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Slug:</strong>
                {{ $podcastCategory->slug }}
            </div>
        </div>
    </div>
    </div>
    <hr>
    <div class="pull-right">
                <a class="btn btn-primary btn-sm" href="{{ route('podcast-categories.index') }}"> Back</a>
            </div>
</div>
</div>
</div>    
</div>    
@endsection