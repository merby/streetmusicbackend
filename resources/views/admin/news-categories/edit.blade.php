@extends('layouts.admin')
  
@section('content')

<div class="container-fluid animatedParent animateOnce">
        <div class="animated fadeInUpShort">
            <div class="row my-3">
                <div class="col-md-7  offset-md-2">
                <div class="card no-b  no-r">

@if ($errors->any())
    <div class="alert alert-danger">
        <strong>Whoops!</strong> There were some problems with your input.<br><br>
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form action="{{ route('news-categories.update',$newsCategory->id) }}" method="POST">
    @csrf
    @method('PUT')
    <div class="card no-b  no-r">
    <div class="card-body">
        <h5 class="card-title">Edit Category</h5>
        <hr>
        <div class="form-row">
            <div class="col-md-8">
                <div class="form-group m-0">
                    <label for="name" class="col-form-label s-12">CATEGORY NAME</label>
                    <input name="name" id="name" value="{{ $newsCategory->name }}" placeholder="Enter Category Name" class="form-control r-0 light s-12 " type="text">
                </div>
            </div>
        </div>
    </div>
    
    <hr>
    <div class="card-body">
        <button type="submit" class="btn btn-primary btn-sm"><i class="icon-save mr-2"></i>Save Data</button>
    </div>
</div>
</form>      
</div>
</div>
</div>
</div>
@endsection
