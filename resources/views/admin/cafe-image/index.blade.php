@extends('layouts/admin')

@section('content')
<style media="screen">
  .btn .icon {
    padding: 0px;
  }
</style>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
{!! Html::style(asset('vendor/jquery-image-uploader-preview-and-delete/dist/image-uploader.min.css')) !!}
<div class="container-fluid animatedParent animateOnce">
  <div class="animated fadeInUpShort">
    <div class="row">
      <div class="col-md-12 mb-4">
        <div class="card">
          <div class="card-header">
            <strong>Image Cafe "{{$cafe->name}}"</strong>
            <div class="float-right">
              <button type="button" name="button" class="btn btn-outline-primary btn-sm btn-reload" title="refresh table"><i class="icon icon-refresh"></i></button>
              <a href="{{route('cafe')}}" class="btn btn-sm btn-outline-primary" title="Back">Back</a>
            </div>
          </div>
          <div class="card-body">
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                <p>{{ $message }}</p>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif
            @if ($errors->any())
            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
            @endif

            <div class="row">
              <div class="col-md-6 col-sm-12">
                <form method="POST" name="form-example-1" id="form-example-1" enctype="multipart/form-data" action="{{route('cafe.image.store')}}">
                  @csrf
                  <input type="hidden" name="cafe_id" value="{{$cafe->id}}">
                  <div class="input-field">
                      <label class="active">Images</label>
                      <div class="input-images-1" style="padding-top: .5rem;"></div>
                  </div>

                  <input type="submit" name="upload" value="Upload" class="btn btn-primary">

                </form>
              </div>

              <div class="col-md-6 col-sm-12">
                <div class="table-responsive">

                  <table class="table table-bordered" style="background:white;" id="datatable">
                      <thead>
                        <tr>
                            <th width="2%" class="text-nowrap">No</th>
                            <th width="50%">Image</th>
                            <th width="2%">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                      </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /.box -->
@endsection
@section('js')
{!! Html::script(asset('vendor/jquery-image-uploader-preview-and-delete/dist/image-uploader.min.js')) !!}
<script type="text/javascript">
$('.input-images-1').imageUploader();


$(document).ready(function() {
  $('#datatable').DataTable({
  serverSide: true,
  processing: true,
  responsive: true,
  ordering: false,
  ajax: {
      url: "{{route('cafe.image.data')}}",
      data: function(d) {
        d.cafe_id = '{{request()->get("cafe")}}';
      }
    },
  columns:[
    {data: 'DT_RowIndex', className: 'text-nowrap'},
    {data: 'image', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data) {
        return '<img src="'+data+'" width="100" height="85">';
      }
    },
    {data: 'id', className: 'text-nowrap', orderable: false, searchable:false,
      render: function(data, type, full, meta) {
        var btn =
          '<div class="btn-group">'
          +
          '<button type="button" data-id="'+data+'" class="btn btn-sm btn-outline-primary btn-delete" title="delete"><i class="icon icon-trash"></i></button>'
          +
          '</div>'
          return btn;
      }
    },
  ]
});
});

$(document).on('click', '.btn-reload', function(e){
  e.preventDefault();
  $('#datatable').DataTable().ajax.reload();
});

$(document).on('click', '.btn-delete', function(e){
  e.preventDefault();
  var id = $(this).attr('data-id');
  jQuery('#modal-content-sm').html('');
  setTimeout(function () {
    jQuery.ajax({
      type: 'GET',
      url: '{{route("cafe.image.delete")}}',
      dataType: 'json',
      data: {id:id},
      success: function(data){
        $('#modal-content-sm').html(data);
        $('#modal-sm').modal({ backdrop: 'static' }, 'show');
      }
    });
  }, 100);
});

</script>
@endsection
