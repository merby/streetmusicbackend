@extends('layouts.business')

@section('content')

<!-- Page Content -->
<div class="container-fluid p-y-md">
<!-- Stats -->
<div class="row">
<div class="col-sm-6 col-lg-3">
<a class="card" href="javascript:void(0)">
<div class="card-block clearfix">
<div class="pull-right">
<p class="h6 text-muted m-t-0 m-b-xs">Employee</p>
<p class="h3 text-blue m-t-sm m-b-0">$120.9k</p>
</div>
<div class="pull-left m-r">
<span class="img-avatar img-avatar-48 bg-blue bg-inverse"><i class="ion-ios-bell fa-1-5x"></i></span>
</div>
</div>
</a>
</div>
<!-- .col-sm-6 -->

<div class="col-sm-6 col-lg-3">
<a class="card bg-green bg-inverse" href="javascript:void(0)">
<div class="card-block clearfix">
<div class="pull-right">
<p class="h6 text-muted m-t-0 m-b-xs">Total visitors</p>
<p class="h3 m-t-sm m-b-0">920 000</p>
</div>
<div class="pull-left m-r">
<span class="img-avatar img-avatar-48 bg-gray-light-o"><i class="ion-ios-people fa-1-5x"></i></span>
</div>
</div>
</a>
</div>
<!-- .col-sm-6 -->

<div class="col-sm-6 col-lg-3">
<a class="card bg-blue bg-inverse" href="javascript:void(0)">
<div class="card-block clearfix">
<div class="pull-right">
<p class="h6 text-muted m-t-0 m-b-xs">Revenue</p>
<p class="h3 m-t-sm m-b-0">$340.5k</p>
</div>
<div class="pull-left m-r">
<span class="img-avatar img-avatar-48 bg-gray-light-o"><i class="ion-ios-speedometer fa-1-5x"></i></span>
</div>
</div>
</a>
</div>
<!-- .col-sm-6 -->

<div class="col-sm-6 col-lg-3">
<a class="card bg-purple bg-inverse" href="javascript:void(0)">
<div class="card-block clearfix">
<div class="pull-right">
<p class="h6 text-muted m-t-0 m-b-xs">Messages</p>
<p class="h3 m-t-sm m-b-0">3 new</p>
</div>
<div class="pull-left m-r">
<span class="img-avatar img-avatar-48 bg-gray-light-o"><i class="ion-ios-email fa-1-5x"></i></span>
</div>
</div>
</a>
</div>
<!-- .col-sm-6 -->
</div>
<!-- .row -->
<!-- End stats -->


<!-- .row -->
</div>
<!-- .container-fluid -->
<!-- End Page Content -->
@endsection
