@extends('layouts.business')

@section('content')


     <div class="container-fluid p-y-md">
          <div class="row">

            <div class=" col-sm-6 col-lg-3">
              <div class="card">
                <div class="card-block text-center bg-img">
                  <img class="img-thumbnail" src="{{ asset($business->image) }}" alt="" />
                </div>

                @if ($business->businessCategories->id == 2)
                <div class="card-block text-center" style="background-color:#00BFFF;">
                @else
                <div class="card-block text-center" style="background-color:coral;">
                @endif
                  <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
                  <b><i>{{$business->businessCategories->name}}</i></b>
                </div>
                <div class="card-block text-center">
                  <a href="/business/dashboard-business/{{$business->id}}">Back To Dashboard</a>
                </div>
              </div>
            </div>

            @if ($business->businessCategories->id !== 1)
            <div class="col-md-8">
              <div class="card">
                <div class="card-block clearfix text-center">
                  <h4> Masih Dalam Garapan</i></h4>
                </div>
              </div>
            </div>

            @elseif ($business->businessCategories->id === 1)
            <div class="col-md-8">

              <div class="card">

                <ul class="nav nav-tabs nav-tabs-icons-block nav-justified" data-toggle="tabs">
                  <li @if ((int)$tag === 2 || $tag === null) class="active" @else @endif>
                    <a href="#request">Request</a>
                  </li>

                  <li @if ((int)$tag === 3) class="active" @else @endif>
                    <a href="#in_progress">In Progress</a>
                  </li>

                  <li @if ((int)$tag === 4) class="active" @else @endif>
                    <a href="#revision">Revision</a>
                  </li>

                  <li @if ((int)$tag === 5) class="active" @else @endif>
                    <a href="#finish">Finish</a>
                  </li>
                </ul>

                <div class="card-block tab-content">
                  <div @if ((int)$tag === 2 || $tag === null) class="tab-pane active" @else class="tab-pane" @endif id="request">
                    <div class="card-block clearfix">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th class="text-center" style="width: 50px;">Id</th>
                            <th>Nama Request</th>
                            <th>Tanggal</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                          </tr>
                        </thead>

                        <tbody>
                            @if (!count(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 2)))
                            <tr>
                              <td colspan="4" class="text-center">Data Tidak Ada</td>
                            </tr>
                            @else
                            @foreach(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 2) as $bu)
                            <tr>
                              <td class="text-center">{{$bu->id}}</td>
                              <td><a href="/business/detail-order/{{$bu->id}}/{{$business->id}}">{{ucwords($bu->request_name)}}</a></td>
                              <td>{{$bu->tanggal}}</td>
                              <td class="text-center">
                                <div class="btn-group">
                                  <button onclick="location.href='/business/status/{{$bu->id}}/{{$business->businessAggregator[0]->id}}'" class="btn btn-primary" type="button">Ubah Status</a>
                                  <!-- <button onclick="location.href='/business/delete/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Remove client"><i class="ion-close"></i></button> -->
                                </div>
                              </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                       </table>
                    </div>
                  </div>

                  <div @if ((int)$tag === 3) class="tab-pane active" @else class="tab-pane" @endif id="in_progress">
                    <div class="card-block clearfix">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th class="text-center" style="width: 50px;">Id</th>
                            <th>Nama Request</th>
                            <th>Tanggal</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                          </tr>
                        </thead>

                        <tbody>
                            @if (!count(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 3)))
                            <tr>
                              <td colspan="4" class="text-center">Data Tidak Ada</td>
                            </tr>
                            @else
                            @foreach(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 3) as $bu)
                            <tr>
                              <td class="text-center">{{$bu->id}}</td>
                              <td><a href="/business/detail-order/{{$bu->id}}/{{$business->businessAggregator[0]->id}}">{{ucwords($bu->request_name)}}</a></td>
                              <td>{{$bu->tanggal}}</td>
                              <td class="text-center">
                                <div class="btn-group">
                                  <button onclick="location.href='/business/status/{{$bu->id}}/{{$business->businessAggregator[0]->id}}'" class="btn btn-primary" type="button">Ubah Status</a>
                                  <!-- <button onclick="location.href='/business/delete/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Remove client"><i class="ion-close"></i></button> -->
                                </div>
                              </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                       </table>
                    </div>
                  </div>

                  <div @if ((int)$tag === 4) class="tab-pane active" @else class="tab-pane" @endif id="revision">
                    <div class="card-block clearfix">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th class="text-center" style="width: 50px;">Id</th>
                            <th>Nama Request</th>
                            <th>Tanggal</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                          </tr>
                        </thead>

                        <tbody>
                            @if (!count(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 4)))
                            <tr>
                              <td colspan="4" class="text-center">Data Tidak Ada</td>
                            </tr>
                            @else
                            @foreach(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 4) as $bu)
                            <tr>
                              <td class="text-center">{{$bu->id}}</td>
                              <td><a href="/business/detail-order/{{$bu->id}}/{{$business->businessAggregator[0]->id}}">{{ucwords($bu->request_name)}}</a></td>
                              <td>{{$bu->tanggal}}</td>
                              <td class="text-center">
                                <div class="btn-group">
                                  <button onclick="location.href='/business/status/{{$bu->id}}/{{$business->businessAggregator[0]->id}}'" class="btn btn-primary" type="button">Ubah Status</a>
                                  <!-- <button onclick="location.href='/business/delete/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Remove client"><i class="ion-close"></i></button> -->
                                </div>
                              </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                       </table>
                    </div>
                  </div>

                  <div @if ((int)$tag === 5) class="tab-pane active" @else class="tab-pane" @endif id="finish">
                    <div class="card-block clearfix">
                      <table class="table table-bordered table-striped">
                        <thead>
                          <tr>
                            <th class="text-center" style="width: 50px;">Id</th>
                            <th>Nama Request</th>
                            <th>Tanggal</th>
                            <th class="text-center" style="width: 100px;">Actions</th>
                          </tr>
                        </thead>

                        <tbody>
                            @if (!count(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 5)))
                            <tr>
                              <td colspan="4" class="text-center">Data Tidak Ada</td>
                            </tr>
                            @else
                            @foreach(collect($business->businessAggregator[0]->businessAggregatorOrder)->where('status', 5) as $bu)
                            <tr>
                              <td class="text-center">{{$bu->id}}</td>
                              <td><a href="/business/detail-order/{{$bu->id}}/{{$business->businessAggregator[0]->id}}">{{ucwords($bu->request_name)}}</a></td>
                              <td>{{$bu->tanggal}}</td>
                              <td class="text-center">
                                <div class="btn-group">
                                  <button onclick="location.href='#'" class="btn btn-primary" type="button">Download Data</a>
                                  <!-- <button onclick="location.href='/business/delete/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Remove client"><i class="ion-close"></i></button> -->
                                </div>
                              </td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                       </table>
                    </div>
                  </div>
                </div>
              </div>

            </div>
            @endif

          </div>
        </div>

  @endsection
