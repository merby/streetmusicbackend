@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->business->image) }}" alt="" />
            </div>

            <div class="card-block text-center" style="background-color:coral;">
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->business->businessCategories->name}}</i></b>
            </div>

            <div class="card-block text-center">
              <a href="/business/detail/{{$business->id}}">Kembali Ke List Order</a>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">


            <div class="card-block clearfix">
              <h4> <i>Detail Order</i> </h4>
              </br>
              <table>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Lengkap</b></td>
                    <td>{{ucwords($detail->nama_lengkap)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Email</b></td>
                    <td>{{$detail->email}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>NIK</b></td>
                    <td>{{$detail->nik}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Telepon 1</b></td>
                    <td>{{$detail->phone_satu}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Telepon 2</b></td>
                    <td>{{$detail->phone_dua}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Jenis Rilis</b></td>
                    <td>@if($detail->jenis_rilis == 1) Non Kompilasi @else Kompilasi @endif</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Jumlah Artis</b></td>
                    <td>{{$detail->jumlah_artis}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Youtube Channel</b></td>
                    <td>{{$detail->channel_yt}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Facebook</b></td>
                    <td>{{$detail->akun_fb}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Instagram</b></td>
                    <td>{{$detail->akun_ig}}</td>
                  </tr>
               </table>
            </div>

            </br>

            <div class="card-block clearfix">
              <h4> <i>Detail Track Lagu</i> </h4>
              </br>
              <table class="table table-bordered table-striped table-vcenter">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 50px;">No</th>
                    <th>Judul</th>
                    <th>Nama Artis</th>
                    <th>Genre</th>
                    <th>Tanggal Rilis</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                  </tr>
                </thead>

                <tbody>
                  @foreach($detail->orderDetailTrack as $index => $ordT)
                  <?php
                    $gen = collect($genre)->where('id', $ordT->genre_id)->first()->name;
                  ?>
                  <tr>
                    <td class="text-center">{{$index+1}}</td>
                    <td>{{ucwords($ordT->judul_rilis)}}</td>
                    <td>{{$ordT->nama_artis}}</td>
                    <td>{{$gen}}</td>
                    <td>{{$ordT->tanggal_rilis}}</td>
                    <td class="text-center">
                      <div class="btn-group">
                        <button onclick="location.href='/business/detail-track/{{$ordT->id}}/{{$business->id}}'" class="btn btn-primary" type="button"  title="Edit client">Detail</a>
                      </div>
                    </td>
                  </tr>
                  @endforeach
                </tbody>
               </table>
            </div>
          </div>
        </div>

      </div>
  </div>

  @endsection
