@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->image) }}" alt="" />
            </div>

            <div class="card-block text-center" style="background-color:coral;">
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->businessCategories->name}}</i></b>
            </div>

            <div class="card-block text-center">
              <a href="/business/configuration/{{$business->id}}">Kembali Ke Konfigurasi</a>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix">
              <h4> Tambah Paket Baru</h4>
              </br>

              <form class="form-horizontal" action="{{ route('business.storePackage') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <input value="{{$business->id}}" type="hidden" name="bId" />
                <div class="form-group">
                  <div class="col-xs-10">
                    <label for="example-nf-email">Name</label>
                      <input required class="form-control" type="text" name="name" value="{{ old('names') }}"/>
                  </div>
                </div>
                </br>
                <div class="form-group">
                  <div class="col-xs-10">
                    <label for="example-nf-email">Price</label>
                      <input placeholder="Masukan Dalam Angka (IDR)" required class="form-control" type="text" name="price" value="{{ old('names') }}"/>
                  </div>
                </div>
                </br>
                <div class="form-group">
                  <div class="col-xs-10">
                    <label for="example-nf-email">Keterangan</label>
                      <textarea required class="form-control" id="example-textarea-input" name="description" rows="6"></textarea>
                  </div>
                </div>
                </br>
                <div class="form-group m-b-0 text-center">
                    <button class="btn btn-primary" type="submit">Tambah Paket</button>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
  </div>

  @endsection
