@extends('layouts.business')

@section('content')

<div class="container-fluid p-y-md">
  <div class="row">
    <div class="row" style="width: 80%;margin: 0 auto;">
      <div class="col-sm-6 col-lg-12">
        <div class="pull-left">
          <p class="h6">Streetmusic For Business</p>
          <p class="h2 text-read m-t-sm m-b-0" style="font-weight:bold;">Reach Your Market in Music</p>
          <p>&nbsp;</p>
        </div>
      </div>

      <div class="col-sm-6 col-lg-3">
        <a href="/business/create" class="card" href="javascript:void(0)">
          <div class="card-block text-center bg-img" >
            <img class="img-avatar img-avatar-128 img-avatar-thumb" src="{{ asset('img/dummy/uplus.png') }}" alt="" />
          </div>
          <div class="card-block text-center">
            <p class="h4 profile-title m-b-xs">Create Your Business </p>
          </div>
        </a>
      </div>

      @foreach($businessBanner as $buBan)

        <div class=" col-sm-6 col-lg-3">
          <a href="/business/detail/{{$buBan->id}}" class="card" href="javascript:void(0)">
            <div class="card-block text-center bg-img center-cropped2">
              <img class="img-thumbnail" src="{{ asset($buBan->image) }}" alt="" />
            </div>

            @if ($buBan->businessCategories->id == 2)
            <div class="card-block text-center" style="background-color:#00BFFF;">
            @else
            <div class="card-block text-center" style="background-color:coral;">
            @endif
              <p class="h4 profile-title m-b-xs">{{ucwords($buBan->name)}}</p>
              <b><i>{{$buBan->businessCategories->name}}</i></b>
            </div>

          </a>
        </div>
      @endforeach
    </div>

    <div class="card clearfix" style="width: 78%;margin: 0 auto;">
      <div class="card-block clearfix">
        <table class="table table-bordered table-striped table-vcenter js-dataTable-full">
          <thead>
            <tr>
              <th class="text-center" style="width: 50px;"></th>
              <th>Nama</th>
              <th class="hidden-xs w-15">Tipe Bisnis</th>
              <th class="text-center" style="width: 100px;">Actions</th>
            </tr>
          </thead>

          <tbody>
              @foreach($business as $bu)
              <tr>
                <td class="text-center">{{$bu->id}}</td>
                <td><a href="/business/detail/{{$bu->id}}">{{ucwords($bu->name)}}</a></td>
                <td class="hidden-xs">{{$bu->businessCategories->name}}</td>
                <td class="text-center">
                  <div class="btn-group">
                    <button onclick="location.href='/business/edit/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Edit client"><i class="ion-edit"></i></a>
                    <!-- <button onclick="location.href='/business/delete/{{$bu->id}}'" class="btn btn-xs btn-default" type="button"  title="Remove client"><i class="ion-close"></i></button> -->
                  </div>
                </td>
              </tr>
              @endforeach
          </tbody>
         </table>
      </div>
    </div>
  </div>
</div>

@endsection

@section('script')
<script>

</script>
@endsection
