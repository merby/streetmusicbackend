@extends('layouts.business')

@section('content')

 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->business->image) }}" alt="" />
            </div>

            <div class="card-block text-center" style="background-color:coral;">
              <p class="h4 profile-title m-b-xs">{{ucwords($detail->judul_rilis)}}</p>
              <b><i>{{$detail->nama_pencipta}}</i></b>
            </div>

            <div class="card-block text-center">
              <a href="/business/detail-order/{{$detail->orders_details_id}}/{{$business->id}}">Kembali Ke Detail Order</a>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">

            <?php
              $gen = collect($genre)->where('id', $detail->genre_id)->first()->name;
              $bhs = collect($bahasa)->where('id', $detail->bahasa_rilis_id)->first()->name;
              $bhs_lirik = collect($bahasa)->where('id', $detail->bahasa_lirik_id)->first()->name;
            ?>
            <div class="card-block clearfix">
              <h4> Detail Track Music <i>{{ucwords($detail->judul_rilis)}}</i> </h4>
              </br>
              <table>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Judul Rilis</b></td>
                    <td>{{ucwords($detail->judul_rilis)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Cover Rilis</b></td>
                    <td>{{ucwords($detail->cover_rilis)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Artis</b></td>
                    <td>{{ucwords($detail->nama_artis)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Link Spotify</b></td>
                    <td>@if($detail->link_spotify == null) Tidak Ada @else {{$detail->link_spotify}} @endif</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Link Spotify Tamu</b></td>
                    <td>{{$detail->link_spotify_tamu}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Artis Tamu</b></td>
                    <td>{{ucwords($detail->nama_artis_tamu)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Genre Lagu</b></td>
                    <td>{{$gen}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Genre Sekunder</b></td>
                    <td>{{ucwords($detail->genre_sekunder)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Bahasa Rilis</b></td>
                    <td>{{$bhs}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Hak Cipta</b></td>
                    <td>{{ucwords($detail->nama_hak_cipta)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Tahun Rilis</b></td>
                    <td>{{$detail->tahun_rilis}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Master Rekaman</b></td>
                    <td>{{ucwords($detail->nama_master_rekaman)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Tahun Master Rekaman</b></td>
                    <td>{{$detail->tahun_master_rekaman}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Informasi Rilis</b></td>
                    <td>{{ucwords($detail->informasi_rilis)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Tanggal Rilis</b></td>
                    <td>{{$detail->tanggal_rilis}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Pencipta</b></td>
                    <td>{{ucwords($detail->nama_pencipta)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Penulis</b></td>
                    <td>{{ucwords($detail->nama_penulis)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Nama Produser</b></td>
                    <td>{{ucwords($detail->nama_produser)}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Bahasa Lirik</b></td>
                    <td>{{$bhs_lirik}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Explicit</b></td>
                    <td>@if($detail->explicit == 1) Ya @else Tidak @endif</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Link File Music</b></td>
                    <td>{{$detail->file_music}}</td>
                  </tr>
                  <tr style="height: 30px;">
                    <td style="width: 200px;"><b>Set Start Lagu</b></td>
                    <td>{{$detail->set_start}}</td>
                  </tr>

               </table>
            </div>

          </br>

          <div class="card-block clearfix">
            <h4> <i>Lirik Lagu</i> </h4>

            </br>

            <p>{{ucwords($detail->lirik_lagu)}}</p>
          </div>

          </div>
        </div>

      </div>
  </div>

  @endsection
