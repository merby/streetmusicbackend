@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->business->image) }}" alt="" />
            </div>

            <div class="card-block text-center" style="background-color:coral;">
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->business->businessCategories->name}}</i></b>
            </div>

            <div class="card-block text-center">
              <a href="/business/detail/{{$business->id}}">Kembali Ke List Order</a>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix">
              <h4> Ubah Status Request <i>{{ucwords($order->request_name)}}</i></h4>
              </br>

              <form class="form-horizontal" action="{{ route('business.updateStatus') }}" enctype="multipart/form-data" method="POST">
                @csrf
                <input value="{{$order->id}}" type="hidden" name="id" />
                <input value="{{$business->id}}" type="hidden" name="bId" />
                <div class="form-group">
                  <div class="col-md-8">
                    <select class="form-control" name="status" style="width: 100%;">
    									<option value="3" @if($order->status == 3) selected @endif>In Progress</option>
    									<option value="4" @if($order->status == 4) selected @endif>Revision</option>
    									<option value="5" @if($order->status == 5) selected @endif>Finish</option>
		                </select>
                  </div>
                </div>
                </br>
                <div class="form-group">
                  <div class="col-xs-10">
                    <label for="example-nf-email">Keterangan</label>
                      <textarea required class="form-control" id="example-textarea-input" name="keterangan" rows="6"></textarea>
                    <br>
                  </div>
                </div>
                </br>
                <div class="form-group m-b-0 text-center">
                    <button class="btn btn-primary" type="submit">Ubah Status</button>
                </div>
              </form>
            </div>
          </div>
        </div>

      </div>
  </div>

  @endsection
