<?php
  $total = [];
?>

@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->image) }}" alt="" />
            </div>

            @if ($business->businessCategories->id == 2)
            <div class="card-block text-center" style="background-color:#00BFFF;">
            @else
            <div class="card-block text-center" style="background-color:coral;">
            @endif
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->businessCategories->name}}</i></b>
            </div>
            <div class="card-block text-center">
              <a href="/business/dashboard-business/{{$business->id}}">Back To Dashboard</a>
            </div>
          </div>
        </div>

        @if ($business->businessCategories->id !== 1)
        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix text-center">
              <h4> Masih Dalam Garapan</i></h4>
            </div>
          </div>
        </div>

        @elseif ($business->businessCategories->id === 1)
        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix">
              <h4>Report Sales</h4>
              </br>

              <table class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th class="text-center" style="width: 50px;">Id</th>
                    <th>Nama Order</th>
                    <th>Nama Paket</th>
                    <th>Price</th>
                    <th class="text-center" style="width: 100px;">Actions</th>
                  </tr>
                </thead>

                <tbody>
                    @if (!count(collect($order)->where('status', 5)))
                    <tr>
                      <td colspan="4" class="text-center">Data Tidak Ada</td>
                    </tr>
                    @else
                    @foreach(collect($order)->where('status', 5) as $bu)
                    <?php
                      array_push($total, $bu->businessPackage->price);
                    ?>
                    <tr>
                      <td class="text-center">{{$bu->id}}</td>
                      <td>{{ucwords($bu->request_name)}}</td>
                      <td>{{ucwords($bu->businessPackage->package_name)}}</td>
                      <td>{{$bu->businessPackage->price}}</td>
                      <td class="text-center">
                        <div class="btn-group">
                          <button onclick="location.href=''" class="btn btn-primary" type="button">Download Report</a>
                        </div>
                      </td>
                    </tr>
                    @endforeach
                    @endif
                </tbody>
               </table>
                </br>
               <h4>Total  :  {{array_sum($total)}}</h4>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix">
              <h4>Chart Sales</h4>

              </br>
              <div class="card-block">
                  <!-- Bars Chart Container -->
                  <div class="js-flot-bars" style="height: 330px;"></div>
              </div>
            </div>
          </div>
        </div>
        @endif

      </div>
  </div>

  @endsection
