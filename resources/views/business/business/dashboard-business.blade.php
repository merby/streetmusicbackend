@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->image) }}" alt="" />
            </div>

            @if ($business->businessCategories->id == 2)
            <div class="card-block text-center" style="background-color:#00BFFF;">
            @else
            <div class="card-block text-center" style="background-color:coral;">
            @endif
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->businessCategories->name}}</i></b>
            </div>
            <div class="card-block text-center">
              <a href="/business/configuration/{{$business->id}}">Konfigurasi</a>
            </div>
            <div class="card-block text-center">
              <a href="/business/detail/{{$business->id}}">Detail Order</a>
            </div>
            <div class="card-block text-center">
              <a href="/business/report-business/{{$business->id}}">Report</a>
            </div>
          </div>
        </div>

        @if ($business->businessCategories->id !== 1)
        <div class="col-md-8">
          <div class="card">
            <div class="card-block clearfix text-center">
              <h4> Masih Dalam Garapan</i></h4>
            </div>
          </div>
        </div>

        @elseif ($business->businessCategories->id === 1)
        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-header bg-blue bg-inverse">
                        <h4>Total Order Masuk</h4>

                    </div>
                    <div class="card-block">
                        <h4>{{count(collect($business->businessAggregator[0]->businessAggregatorOrder))}}</h4>
                    </div>
                </div>
            </div>
            <!-- .col-sm-6 -->

            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-header bg-blue bg-inverse">
                        <h4>Total Request</h4>

                    </div>
                    <div class="card-block">
                        <h4>{{count(collect($order)->where('status', 2))}}</h4>
                    </div>
                </div>
            </div>
            <!-- .col-sm-6 -->

            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-header bg-blue bg-inverse">
                        <h4>Total In Progress</h4>

                    </div>
                    <div class="card-block">
                        <h4>{{count(collect($order)->where('status', 3))}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-header bg-blue bg-inverse">
                        <h4>Total Revision</h4>

                    </div>
                    <div class="card-block">
                        <h4>{{count(collect($order)->where('status', 4))}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card">
                    <div class="card-header bg-green bg-inverse">
                        <h4>Total Finish</h4>

                    </div>
                    <div class="card-block">
                        <h4>{{count(collect($order)->where('status', 5))}}</h4>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
              <div class="card">
                <div class="card-block clearfix">
                  <h4>Chart Sales</h4>

                  </br>
                  <div class="card-block">
                      <!-- Bars Chart Container -->
                      <div class="js-flot-bars" style="height: 330px;"></div>
                  </div>
                </div>
              </div>
            </div>
        </div>
        @endif

      </div>
  </div>

  @endsection
