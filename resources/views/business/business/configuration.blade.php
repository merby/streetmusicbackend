@extends('layouts.business')

@section('content')


 <div class="container-fluid p-y-md">
      <div class="row">

        <div class=" col-sm-6 col-lg-3">
          <div class="card">
            <div class="card-block text-center bg-img">
              <img class="img-thumbnail" src="{{ asset($business->image) }}" alt="" />
            </div>

            @if ($business->businessCategories->id == 2)
            <div class="card-block text-center" style="background-color:#00BFFF;">
            @else
            <div class="card-block text-center" style="background-color:coral;">
            @endif
              <p class="h4 profile-title m-b-xs">{{ucwords($business->name)}}</p>
              <b><i>{{$business->businessCategories->name}}</i></b>
            </div>

            <div class="card-block text-center">
              <a href="/business/dashboard-business/{{$business->id}}">Back To Dashboard</a>
            </div>
          </div>
        </div>

        <div class="col-md-8">
          <div class="card">
            <ul class="nav nav-tabs nav-tabs-icons-block nav-justified" data-toggle="tabs">
              <li @if ((int)$tag === 1 || $tag === null) class="active" @else @endif>
                <a href="#package">Konfigurasi Paket</a>
              </li>

              <li @if ((int)$tag === 2) class="active" @else @endif>
                <a href="#email">Konfigurasi Notification</a>
              </li>
            </ul>

            <div class="card-block tab-content">
              <div @if ((int)$tag === 1 || $tag === null) class="tab-pane active" @else class="tab-pane" @endif id="package">
                <div class="card-block clearfix">
                  <div class="text-right pr-4"><a href="/business/create-package/{{$business->id}}" class="btn btn-primary">Tambah Paket Baru</a></div><br>
                  <table class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th class="text-center" style="width: 50px;">Id</th>
                        <th>Nama Paket</th>
                        <th>Harga</th>
                        <th class="text-center" style="width: 100px;">Pilihan</th>
                      </tr>
                    </thead>

                    <tbody>
                        @if (!count($package))
                        <tr>
                          <td colspan="4" class="text-center">Data Tidak Ada</td>
                        </tr>
                        @else
                        @foreach($package as $bu)
                        <tr>
                          <td class="text-center">{{$bu->id}}</td>
                          <td>{{ucwords($bu->package_name)}}</td>
                          <td>{{$bu->price}}</td>
                          <td class="text-center">
                            <div class="btn-group">
                              <button onclick="location.href='/business/edit-package/{{$bu->id}}'" class="btn btn-primary" type="button">Edit Paket</a>
                            </div>
                          </td>
                        </tr>
                        @endforeach
                        @endif
                    </tbody>
                   </table>
                </div>
              </div>

              <div @if ((int)$tag === 2) class="tab-pane active" @else class="tab-pane" @endif id="email">
                <form method="POST" action="{{ route('business.updateNotification') }}" enctype="multipart/form-data" id="formBisnis">
                  @csrf
                    <input value="{{$business->id}}" type="hidden" name="bId" />

                    <div class="card-block clearfix">
                        <div class="form-group">
                          <div class="col-xs-12">
                            <label>
                              <input type="hidden" name="email[0]" value="0" />
                              <input @if ((int)$notif->email_notification === 1) checked @else @endif type="checkbox" name="email[0]" class="form-check-input" value="1" /> <h6>Email Notification</h6>
      				              </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <label>
                              <input type="hidden" name="web[0]" value="0" />
                              <input @if ((int)$notif->web_notification === 1) checked @else @endif type="checkbox" name="web[0]" class="form-check-input" value="1" /> <h6>Web Notification</h6>
      				              </label>
                          </div>
                        </div>
                        </br>
                        </br>
                        </br>
                        </br>
                    </div>
                    <div class="form-group m-b-0 text-left">
                        <button class="btn btn-primary" type="submit">Ubah Notification</button>
                    </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>

  @endsection
