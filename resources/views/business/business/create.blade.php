@extends('layouts.business')
@section('content')
<style>
/* HIDE RADIO */
.hiddenradio [type=radio] {
  position: absolute;
  opacity: 0;
  width: 0;
  height: 0;
}

/* IMAGE STYLES */
.hiddenradio [type=radio] + img {
  cursor: pointer;
}

/* CHECKED STYLES */
.hiddenradio [type=radio]:checked + img {
  outline: 1px solid #00FFC4;
  background:#64FCD9;
}
.img-avatar{
  border-radius:0;
}
</style>
<div class="container-fluid p-y-md">
  <div class="row">
    <div class="col-sm-6 col-lg-12">
        <div class="card-block clearfix card bg-blue bg-invers">

            <div class="pull-left m-r">
                <span class="img-avatar img-avatar-48 bg-blue bg-inverse"><i class="fa fa-building-o fa-4x"></i> </span>  <h4 class="text-light" style="color:white;">Create Your Business</h4>

            </div>
        </div>
    </div>
  </div>

  <form method="POST" action="/business/store" enctype="multipart/form-data" id="formBisnis">
    @csrf
    <div class="card hiddenradio" style="padding:10px;">

      @foreach($category as $cat)
        <label>
          <input required type="radio" id="example-radio{{$cat->id}}" name="business_category_id" value="{{$cat->id}}" /><img class="img-avatar img-avatar-128 " src="{{ asset($cat->image)}}" alt=""> <br>
        </label>
      @endforeach

    </div>

    <div class="card">
      <div class="card-header">
        <h4>Business Information</h4>
      </div>

      <div class="card grid-margin">
        <div class="card-body">
          <div class="row">
            <div class="col-md-7 card-block">

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-email">Name</label>
                    <input required class="form-control" type="text" name="name" value="{{ old('names') }}"/>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-email">Description</label>
                    <textarea required class="form-control" id="example-textarea-input" name="description" rows="6"></textarea>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-password" />Location</label>
                    <input class="form-control" name="address" type="text"/>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-password" />Contact</label>
                    <input class="form-control" name="contact" type="text"/>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <input type="file" name="image" class="file" accept="image/*" style="visibility: hidden; position: absolute;">

              <div class="text-center">
                <img src="{{ URL::to('/') }}/images/insert.png" id="preview" class="img-thumbnail img-fluid" style="margin: auto;width:200px;max-width: 100%;height: auto;vertical-align: middle;align: middle;">
              </div>

              </br></br></br>

              <div  class="input-group my-3">
                <input type="text" name="image" disabled class="form-control" placeholder="Upload File" id="file">
                  <span class="input-group-btn">
                    <button type="button" class="browse btn btn-primary">Browse</button>
                  </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group m-b-0 text-center">
        <button class="btn btn-primary" type="submit">Simpan</button>
    </div>
    </br></br></br></br></br>
  </form>
</div>

@endsection

@section('script')
<script>

  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });

  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });

</script>
@endsection
