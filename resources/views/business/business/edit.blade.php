@extends('layouts.business')
@section('content')

<div class="container-fluid p-y-md">
  <div class="row">
    <div class="col-sm-6 col-lg-12">
        <div class="card-block clearfix card bg-blue bg-invers">

            <div class="pull-left m-r">
                <span class="img-avatar img-avatar-48 bg-blue bg-inverse"><i class="fa fa-building-o fa-4x"></i> </span>  <h4 class="text-light" style="color:white;">Edit Business</h4>

            </div>
        </div>
    </div>
  </div>

  <form method="POST" action="/business/edit" enctype="multipart/form-data">
    @csrf

    <div class="card">
      <div class="card-header">
        <h4>Edit {{ucwords($business->name)}}</h4>
      </div>

      <div class="card grid-margin">
        <div class="card-body">
          <div class="row">
            <div class="col-md-7 card-block">

              <input value="{{$business->image}}" type="hidden" name="oldImage" />
              <input value="{{$business->id}}" type="hidden" name="id" />

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-email">Name</label>
                    <input required class="form-control" value="{{$business->name}}" type="text" name="name" value="{{ old('names') }}"/>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-email">Description</label>
                    <textarea required class="form-control" id="example-textarea-input" name="description" rows="6"> {{$business->description}} </textarea>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-password" />Location</label>
                    <input class="form-control" value="{{$business->address}}" name="address" type="text"/>
                  <br>
                </div>
              </div>

              <div class="form-group">
                <div class="col-xs-10">
                  <label for="example-nf-password" />Contact</label>
                    <input class="form-control" value="{{$business->contact}}" name="contact" type="text"/>
                </div>
              </div>
            </div>

            <div class="col-md-4">
              <input type="file" name="image" class="file" accept="image/*" style="visibility: hidden; position: absolute;">

              <div class="text-center">
                <img src="{{ URL::to('/') }}/images/insert.png" id="preview" class="img-thumbnail img-fluid" style="margin: auto;width:200px;max-width: 100%;height: auto;vertical-align: middle;align: middle;">
              </div>

              </br></br></br>

              <div  class="input-group my-3">
                <input type="text" name="image" disabled class="form-control" placeholder="Upload File" id="file">
                  <span class="input-group-btn">
                    <button type="button" class="browse btn btn-primary">Browse</button>
                  </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="form-group m-b-0 text-center">
        <button class="btn btn-app" type="submit">Save</button>
    </div>
    </br></br></br></br></br>
  </form>
</div>

@endsection

@section('script')
<script>

  $(document).on("click", ".browse", function() {
    var file = $(this).parents().find(".file");
    file.trigger("click");
  });

  $('input[type="file"]').change(function(e) {
    var fileName = e.target.files[0].name;
    $("#file").val(fileName);

    var reader = new FileReader();
    reader.onload = function(e) {
      // get loaded data and render thumbnail.
      document.getElementById("preview").src = e.target.result;
    };
    // read the image file as a data URL.
    reader.readAsDataURL(this.files[0]);
  });

</script>
@endsection
