@extends('layouts.business')

@section('content')

<!-- Page Content -->
<div class="container-fluid p-y-md">
<!-- Stats -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-block clearfix text-center">
          <h2><b>Welcome To StreetMusic Business</b></h2>


          <img class="img-responsive" style="width:50%;height:auto;  display: block; margin: 0 auto;margin-top:100px;" src="{{ asset('img/sm-dark.png') }}" title="WEW" alt="AppUI" />

          </br></br></br></br></br>
        </div>
      </div>
    </div>
  </div>
<!-- .row -->
</div>
<!-- .container-fluid -->
<!-- End Page Content -->
@endsection
