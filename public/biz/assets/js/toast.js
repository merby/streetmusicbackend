(function($) {
  showSuccessToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Success',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'success',
      loaderBg: '#f96868',
      position: 'top-right'
    })
  };
  showInfoToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Info',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'info',
      loaderBg: '#46c35f',
      position: 'top-right'
    })
  };
  showWarningToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Warning',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'warning',
      loaderBg: '#57c7d4',
      position: 'top-right'
    })
  };
  showDangerToast = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Danger',
      text: 'And these were just the basic demos! Scroll down to check further details on how to customize the output.',
      showHideTransition: 'slide',
      icon: 'error',
      loaderBg: '#f2a654',
      position: 'top-right'
    })
  };
  showToastPosition = function(position) {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Keterangan Opsi',
      text: '<br> <b>NONE</b> : Tidak Diaktifkan. <br><br> <b>Opsi 1</b> : Nilai Peserta lebih besar dari standar mandatori, tetap lolos dan masuk perhitungan prosentase normal. <br><br> <b>Opsi 2</b> : Nilai Peserta sama dengan standar mandatori masuk perhitungan prosentase normal, apabila tidak sama dengan mandatori otomatis hasil tidak disarankan.                                 <br> <br>           <b>Opsi 3</b> : Nilai Peserta lebih besar plus 1 dari standar mandatori, tetap lolos dan masuk perhitungan prosentase normal, apabila tidak demikian otomatis hasil tidak disarankan.',
      position: String(position),
      icon: 'info',
      stack: false,
      hideAfter : false,
      loaderBg: '#f96868'
    })
  }
  showToastInCustomPosition = function() {
    'use strict';
    resetToastPosition();
    $.toast({
      heading: 'Custom positioning',
      text: 'NONE : Tidak Diaktifkan. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Opsi 1 : Nilai Peserta lebih besar dari standar mandatori, tetap lolos dan masuk perhitungan prosentase normal. Opsi 2 : Nilai Peserta sama dengan standar mandatori masuk perhitungan prosentase normal, apabila tidak sama dengan mandatori otomatis hasil tidak disarankan. &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Opsi 3 : Nilai Peserta lebih besar plus 1 dari standar mandatori, tetap lolos dan masuk perhitungan prosentase normal, apabila tidak demikian otomatis hasil tidak disarankan.',
      icon: 'info',
      position: {
        left: 120,
        top: 120
      },
      stack: false,
      hideAfter : false,
      loaderBg: '#f96868'
    })
  }
  resetToastPosition = function() {
    $('.jq-toast-wrap').removeClass('bottom-left bottom-right top-left top-right mid-center'); // to remove previous position class
    $(".jq-toast-wrap").css({
      "top": "",
      "left": "",
      "bottom": "",
      "right": ""
    }); //to remove previous position style
  }
})(jQuery);
