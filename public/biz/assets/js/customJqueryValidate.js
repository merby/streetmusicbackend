// add method indonesian date validation dd/mm/yyyy
$.validator.addMethod(
    "indonesianDate",
    function(value, element) {
        return value.match(/^\d\d?\/\d\d?\/\d\d\d\d$/);
    },
    "Please enter a Date of Birth in the format dd/mm/yyyy."
);

// add method letter only
jQuery.validator.addMethod("lettersonly", function(value, element) {
  return this.optional(element) || /^[a-zA-Z\s]*$/i.test(value);
  }, "Letters only please"
);

// add method username
jQuery.validator.addMethod("usernameFormat", function(value, element) {
  return this.optional(element) || /^[a-z0-9_]{3,100}$/i.test(value);
}, "Special character not allowed. Just letter, digit and underscore"
);

// add method no space
jQuery.validator.addMethod("noSpace", function(value, element) {
return (value.trim() == value) && (value.indexOf(" ") < 0);
}, "No space please and don't leave it empty");

jQuery.validator.addMethod("nowhitespace", function(value, element) {
    return value.trim() == value;
}, "No white space please");

// add method formatPermission
jQuery.validator.addMethod("permissionFormat", function(value, element) {
  return this.optional(element) || /^([a-z]+)-([a-z]+)$/i.test(value);
}, "Format (abcd-abcd) only please"
);

// add method Domain
jQuery.validator.addMethod("domain", function(value, element) {
  return this.optional(element) || /^(?!:\/\/)([a-zA-Z0-9-_]+\.)*[a-zA-Z0-9][a-zA-Z0-9-_]+\.[a-zA-Z]{2,11}?$/i.test(value);
}, "Invalid domain name"
);

// show feedback error
jQuery.validator.setDefaults({
    highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
    }
});
